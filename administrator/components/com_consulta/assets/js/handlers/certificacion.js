/**
*
* Handler for { Tecnico }
*
*
**/

( function( $, window, document, controller, Utilities ){
	
	var CertificacionHandler = function( a ){
		
	};
	
	CertificacionHandler.prototype ={
			
				initialize: function(){

					this.parseExcel();
				}

				/**
				 * Function header when calls the model method and gives the view the response
				 *
				 * @param {}
				 * @return {}
				 */
			,	parseExcel: function(){



					$( '#importar-datos' ).click( function( e ){
						
						e.preventDefault();

						var data = {}

						if( controller.excels.length <= 0 ){
							alert( 'Aún no ha cargado ningún archivo excel.' );
							return;
						}

						controller.truncate( data );
					});

				
				}
		
			
	};
	

	// Use this way when script is loaded at the end of the page
	// Expose to global scope
	$( document ).ready( function( e ){

		window.CertificacionHandler = new CertificacionHandler();
		window.CertificacionHandler.initialize();
	});
	
})( jQuery, this, this.document, this.CertificacionController, this.Misc, undefined );