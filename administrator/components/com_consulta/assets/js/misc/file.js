/**
*
* File uploading
*
*/

( function( $, window, document, certificacion ){

	var Uploader = function( a ){

		this.Excelparams = {

				element: document.getElementById( 'excel-uploader' )
			,   action: 'index.php'
			,   params: {

			    	option: 'com_consulta',
			    	task: 'carga.uploadExcel'
			    }

			,   allowedExtensions: [ 'xlsx', 'xls' ]
			,   debug: false
			,	onSubmit: this.onSubmitExcel
			,	onProgress: this.onProgressExcel
			,	onComplete: this.onCompleteExcel
			,   onError: this.onErrorExcel
		    
		};


	};

	Uploader.prototype = {

			initialize: function(){

				this.createUploaderExcel();
			}

		,	createUploaderExcel: function(){

				var _this = this;

				var uploader = new qq.FileUploader( _this.Excelparams );

			}

		,	onSubmitExcel: function( id, fileName ){

				var li = $( '<li>' )
				,	i = $( '<i>' )
				,	spanName = $( '<span>' )
				,	spanBar = $( '<span>' )
				,	spanPerc = $( '<span>' );

				var htmlName = fileName.replace( /.xls/g, '' );
				htmlName = htmlName.replace( / /g, '-' );

				console.log(htmlName);

				i.addClass( 'icon-excel' );
				spanName.text( fileName );
				spanName.addClass('filename');
				spanBar.addClass( 'progress-bar' );
				spanPerc.addClass( 'percentage' );

				i.appendTo( li );
				spanName.appendTo( li );
				spanBar.appendTo( li );
				spanPerc.appendTo( li );

				li.attr( 'id', 'item-' + htmlName );

				li.appendTo( $( '.excel-list' ) );
			}

		,	onProgressExcel: function( id, fileName, loaded, total ){

				var perc = Math.floor( loaded / total * 100 );

				var htmlName = fileName.replace( /.xls/g, '' );
				htmlName = htmlName.replace( / /g, '-' );

				$( '#item-' + htmlName + ' .percentage' ).text( perc + '%' );
				$( '#item-' + htmlName + ' .progress-bar' ).width( perc + 'px' );

			}

		,	onCompleteExcel: function(id, fileName, responseJSON){

				console.log(responseJSON);

				if( ! responseJSON.success ){
					$( '.qq-upload-list' ).remove();
					return;
				}

				// Save the file name in array to be processed at the end
				certificacion.excels.push( fileName );

			}

		,   onErrorExcel: function(id, fileName, xhr){
				console.log(xhr);
				var span = $('<span>');
				span.addClass('error');
				span.text( 'Falló subida de este archivo' );
				$('#item-' + fileName.replace( /.xls/g, '') ).append( span );
				return;
			}

		
	};

	$( document ).ready( function(){
		window.Uploader = new Uploader();
		window.Uploader.initialize();
	});

})( jQuery, this, this.document, this.CertificacionController, undefined );







