/**
*
* CertificacionModel for { Consulta }
*
*/

( function( $, window, document, Utilities ){

	var CertificacionModel = function( a ){

	};

	CertificacionModel.prototype = {


			/**
			* Triggers a method in backend, sending data and returning the response
			*
			*/
			parseExcel: function( success, error, data ){

				var aOptions = {
						dataType: "json"
					,	async: true
					,   success: success
					,	error: error
				}
				
				,   aData = {
						option: "com_consulta"
					,	task: "carga.parseExcel"
					,   data: data
				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return Utilities.ajaxHandler( aOptions, aData );
				
			}

			/**
			* Truncates the data in db
			*
			*/
		,	truncate: function( success, data ){

				var aOptions = {
						dataType: "json"
					,	async: true
					,   success: success
				}
				
				,   aData = {
						option: "com_consulta"
					,	task: "carga.truncate"
					,   data: data
				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return Utilities.ajaxHandler( aOptions, aData );
			}

			/**
			* Delete all temporary files after all
			*
			*/
		,	deleteTemp: function( success, data ){

				var aOptions = {
						dataType: "json"
					,	async: true
					,   success: success
				}
				
				,   aData = {
						option: "com_consulta"
					,	task: "carga.deleteTemp"
					,   data: data
				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return Utilities.ajaxHandler( aOptions, aData );

			}


	};

	// Expose to global scope
	window.CertificacionModel = new CertificacionModel();

})( jQuery, this, this.document, this.Misc, undefined );