/**
*
* View for { Consulta }
*
*
**/

( function( $, window, document, Utilities ){
	
	var CertificacionView = function( a ){
		
		// atributes, selector or global vars
		this.attribute = 'my-atribute';
		
	};
	
	CertificacionView.prototype ={
			

			/**
			* Function header when calls the model method and gives the view the response
			*
			* @param {}
			* @return {}
			*/
			onCompleteParse: function( data ){

				var excel = data.excel.replace( /.xls/g, '' ).replace( / /g, '-' );

				console.log(excel);

				var _class = ( data.status == 500 ) ? 'error' : 'success';
				$( '#' + excel ).removeClass();
				$( '#' + excel ).text( excel + ': '+ data.message ).addClass( _class );

				if( data.status == 500 )
					return false;


				$( '.excel-reporte' ).slideDown();
				$( '.excel-reporte' ).find( '.excel-errors' ).text( data.errors );
				$( '.excel-log' ).html( data.log );


				return true;
			}


			/**
			* On error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){
				console.log( "error :" + XMLHttpRequest.responseText );
				$( '.truncate-label' ).removeClass('error progress');
				$( '.truncate-label' ).text( 'Hubo un error en el sistema. Recargue la página e intente de nuevo.' ).addClass( 'error' );

				return;
			}

			/**
			* Render the view before parse is uploaded
			*
			*/
		,	onBeforeParse: function( files ){

				// for each file create an li
				for (var i = 0; i < files.length; i++) {
					
					var li = $( '<li>' );

					li.attr( 'id', files[i].replace( /.xls/g, '' ).replace( / /g, '-' ) );
					li.text( files[i] + ': Convirtiendo...' );
					li.addClass( 'progress' );
					
					li.appendTo('.excel-parse-list');
				};
			}

			/**
			* Shows a message on before truncate
			*
			*/
		,	onBeforeTruncate: function(){

				$( '.truncate-label' ).removeClass('error progress');
				$( '.truncate-label' ).text( 'Borrando datos antes de guardar los nuevos. Espere...' ).addClass( 'progress' );
			}

			/**
			* Shows a message when delete temporary files operation starts
			*
			*/
		,	onBeforeDeleteTemp:function(){

				$( '.truncate-label' ).removeClass('error progress');
				$( '.truncate-label' ).text( 'Borrando archivos y carpetas temporales...' ).addClass( 'progress' );
			}

			/**
			* Shows a message when delete temporary files ends
			*
			*/
		,	onCompleteDeleteTemp: function( data ){

				var _class = ( data.status == 500 ) ? 'error' : 'success';
				$( '.truncate-label' ).removeClass('error progress');
				$( '.truncate-label' ).text( data.message ).addClass( _class );
			}

			/**
			* Shows a message on complete truncate
			*
			*/
		,	onCompleteTruncate: function( data ){

				var _class = ( data.status == 500 ) ? 'error' : 'success';
				$( '.truncate-label' ).removeClass('error progress');
				$( '.truncate-label' ).text( data.message ).addClass( _class );
			}

		
		
			
	};
	

	// Use this way when script is loaded at the end of the page
	// Expose to global scope
	window.CertificacionView = new CertificacionView();
	
})( jQuery, this, this.document, this.Misc, undefined );