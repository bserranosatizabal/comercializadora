<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
require_once( JPATH_COMPONENT . DS . 'helpers/uploader.php' );
require_once( JPATH_COMPONENT . DS . 'helpers/PHPExcel.php' );
require_once( JPATH_COMPONENT . DS . 'controller.php' );

$controller = JControllerLegacy::getInstance('Consulta');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
?>