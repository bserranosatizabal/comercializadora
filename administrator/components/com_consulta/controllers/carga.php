
<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );

// Begining of the controller
class ConsultaControllerCarga extends JControllerLegacy{

	protected $dataCertificados;
	protected $dataSeriales;
	protected $certi;
	/**
	*
	* Uploads and the excels files
	*
	*/
	public function uploadExcel(){

		// list of valid extensions, ex. array("jpeg", "xml", "bmp")
		$allowedExtensions = array( 'xlsx', 'xls' );
		// max file size in bytes
		$sizeLimit = 32 * 1024 * 1024;

		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);

		// Call handleUpload() with the name of the folder, relative to PHP's getcwd()
		$result = $uploader->handleUpload('../excels/');

		// to pass data through iframe you will need to encode all html tags
		echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);

		die();
	}


	/**
	* Parse and excel file and save it into database
	*
	*/
	public function parseExcel(){



		$data = JRequest::getVar( 'data' );




		$response = ( object )array();
		$response->log = '';
		$response->errors = 0;

		$response->status = 200;
		$response->message = 'Datos guardados.';
		$response->sent = $data;


		// // get the file uploaded
		if( ! file_exists( '../excels/' . $data['excel'] ) ){

			$response->status = 500;
			$response->log .= 'Archivo no encontrado, controller tecnico linea 53.<br>';
			$response->message = 'No se puede acceder al archivo que has subido.';

			echo json_encode( $response );
			die();
		}


		
		ini_set('max_execution_time', 500); //300 seconds = 5 minutes

		// 1. Parsear excel hoja 1
		$this->dataCertificados = $this->parseSheet( '../excels/' . $data['excel'], 0 );

		// 1.1 Guardar certificacion, producto y serial
		$response = $this->saveSheetOne( $response );

		// 3. Parsear excel hoja 2
		$this->dataSeriales = $this->parseSheet( '../excels/' . $data['excel'], 1 );


		// 3.1 Guardar certificaciones por serial
		$response = $this->saveSheetThree( $response );

		$response->excel = $data[ 'excel' ];

		$response->message = 'Datos convertidos y guardados.';

		echo json_encode( $response );
		die();

	}

	/**
	* Parse sheet from excel file
	*
	*/
	protected function parseSheet( $excelPath = NULL, $sheet = 1 ){

		ini_set('max_execution_time', 500);
		if( ! is_string($excelPath ) )
			return array();

		$objPHPExcel = PHPExcel_IOFactory::load( $excelPath );
		$objWorksheet = $objPHPExcel->setActiveSheetIndex( $sheet );
		$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);


		return $sheetData;
	}

	/**
	* Guarda la hoja 1 del excel
	*
	*/
	protected function saveSheetOne( $response ){

		if( ! is_array( $this->dataCertificados) )
			return false;

		ini_set('max_execution_time', 500); //300 seconds = 5 minutes


		// save the model for each row
		$productos = $this->dataCertificados[1];

		// 1. Guardar productos
		$response = $this->saveProductos( $productos, $response );
		
		// 2. Guardar empresa y certificacion

		$response = $this->saveEmpresa( $response );

		
		return $response;
	}

	/**
	* Guarda la hoja 3 del excel
	*
	*/
	protected function saveSheetThree(  $response ){

		if( ! is_array( $this->dataSeriales ) )
			return false;

		ini_set('max_execution_time', 500); //300 seconds = 5 minutes

		// save the model for each row
		for ($i = 2; $i <= count( $this->dataSeriales ); $i++) {

			$row = $this->dataSeriales[$i];

			// Obtener objeto matricula
			$seriales = $this->getModel( 'seriales' );

			list( $dia, $mes, $año ) = explode('/', $row['E']);
			$fecha_inicio = date( 'Y-m-d', mktime(0,0,0, $mes, $dia, $año) );

			list( $dia, $mes, $año ) = explode('/', $row['F']);
			$fecha_fin = date( 'Y-m-d', mktime(0,0,0, $mes, $dia, $año) );
			
			$args = array(

					'serial' => $row['A']
				,	'producto' => $row['B']
				,	'capacidad' => $row['C']
				,	'certificado' => $row['D']
				,	'fecha_inicio' => $fecha_inicio
				,	'fecha_fin' => $fecha_fin
					
			);

			$seriales->instance( $args );

			if( ! $seriales->save( 'bool' ) ){
				$response->log .= "No se pudo guardar el producto/serial de la fila $i<br>";
				continue;
			}

		}

		return $response;
	}

	/**
	* Guardar productos
	*
	*/
	protected function saveProductos( $data, $response ){

		if( ! is_array( $data ) )
			return false;

		ini_set('max_execution_time', 500); //300 seconds = 5 minutes

		$response->productos = array();

		foreach ($data as $key => $producto) {


			if( $key != 'A' ){

				$productos = explode(',',$producto);

				// Instance the model
				$model = $this->getModel( 'productos' );

				// save the model for each row
				$args = array(
						'nombre' => $productos[0]
					,	'codigo_certificado' => $productos[1]
				);	

				// Instance new producto and save
				$model->instance( $args );

				if( ! $model->save( 'bool' ) ){
					$response->errors++;
					$response->log .= "Producto en columna $key no pudo ser guardado.<br>";
					return $response;
				}

				
			}

			array_push($response->productos, $model->id);

		}

		return $response;

	}

	/**
	* Guardar empresa
	*
	*/
	protected function saveEmpresa( $response ){

		if( ! is_array( $this->dataCertificados ) )
			return false;

		if( ! is_array( $response->productos ) ){
			$response->status = 500;
			$response->log .= 'Archivo no encontrado, productos no encontrados.<br>';
			$response->message = 'No se han encontrado los productos.';

			echo json_encode( $response );
			die();
		}

		$response->empresa = array();

		ini_set('max_execution_time', 500); //300 seconds = 5 minutes


		for ($i=2; $i <= count($this->dataCertificados); $i++) {

			$keyProducto = 0;

			foreach ( $this->dataCertificados[$i] as $key => $value) {

				if( $key == 'A' ){
					// get empresa
					$empresa =  $this->dataCertificados[$i][$key];
					$empresa = explode(',', $empresa );

					// save empresa
					$argsEmpresa = array(

						'nit' => $empresa[1],
						'nombre' => $empresa[0]
					);

					$empresa = $this->getModel( 'empresa' );
					$empresa->instance( $argsEmpresa );

					// If save is not successful, add error to log and try to the next item in the loop
					if( ! $empresa->save( 'bool' ) ){
						$response->log .= 'La empresa en la fila $i no puso ser guardada <br/>';				
						continue;
					}
				}



				// Get id proucto from $response->productos array
				$idproducto = $response->productos[$keyProducto];
				$keyProducto++;


				// Save certifications
			
				if( $key != 'A' ){
					
					$certificado = $this->dataCertificados[$i][$key];
					$certificado = explode( ',', $certificado );

					list( $dia, $mes, $año ) = explode('.', $certificado[1]);
					$fecha_inicio = date( 'Y-m-d', mktime(0,0,0, $mes, $dia, $año) );

					list( $dia, $mes, $año ) = explode('.', $certificado[2]);
					$fecha_fin = date( 'Y-m-d', mktime(0,0,0, $mes, $dia, $año) );

					$argsCertificado = array(
							'codigo' => $certificado[0]
						,	'tipo_certificacion' => $certificado[3]
						,	'fecha_inicio' => $fecha_inicio
						,	'fecha_fin' => $fecha_fin
						,	'id_producto' => $idproducto
						,	'id_empresa' => $empresa->id
					);

					// Save certificado
					$certificado = $this->getModel( 'certificacion' );
					$certificado->instance( $argsCertificado );


					if( ! $certificado->save( 'bool' ) ){
						$response->log .= 'El certificado en columna $key de la empresa en fila $i no pudo ser guardado</br>';
						continue;
					}
				}
			}

		}

		return $response;

	}

	/**
	* Guardar certificacion
	*
	*/
	protected function saveCertificacion( $response ){


		if( ! is_array( $this->dataCertificados ) )
			return false;


		ini_set('max_execution_time', 500); //300 seconds = 5 minutes

		$i;
		foreach ($this->dataCertificados as $key => $certificados) {

			if( $key == 1  )
				continue;

			foreach ($certificados as $col => $certificacion) {

				
				if( $col == 'A')
					continue;

				$certificaciones = explode(',',$certificacion);


				// Instance the model
				$model = $this->getModel( 'certificacion' );

				// save the model for each row
				$args = array(
						'codigo' => $certificaciones[0]
					,	'tipo_certificacion' => $certificaciones[3]
					,	'fecha_inicio' => $certificaciones[1]
					,	'fecha_fin' => $certificaciones[2]
					,	'id_producto' => $response->productos
					,	'id_empresa' => $response->empresa
				);

				// Instance new empresa and save
				$model->instance( $args );

				if( ! $model->save( 'bool' ) ){
					$response->errors++;
					$response->log .= "Empresa en fila $i no pudo ser guardado.<br>";
					return $response;
				}

			}
			
			$i++;
		}

		return $response;

	}

	/**
	* Truncate the table
	*
	*/
	public function truncate(){

		$response = ( object )array();

		$type = JRequest::getVar( 'data' );

		$certificacion = $this->getModel( 'certificacion' );
		$empresa = $this->getModel( 'empresa' );
		$producto = $this->getModel( 'productos' );
		$seriales = $this->getModel( 'seriales' );

		$certificacion->truncate();
		$empresa->truncate();
		$producto->truncate();
		$seriales->truncate();

		$response->status = 200;
		$response->message = "Datos borrados correctamente.";

		echo json_encode( $response );
		die();

	}

	/**
	* Deletes all temporary excel files
	*
	*/
	public function deleteTemp(){

		$response = (object)array();

		// delete excels folder
		system('/bin/rm -rf ' . escapeshellarg( '../excels' ) );

		// re create excels folder
		mkdir( '../excels' );

		$response->status = 200;
		$response->message = "Archivos temporales y antiguos borrados correctamente.";

		echo json_encode( $response );
		die();	
	}


}
?>