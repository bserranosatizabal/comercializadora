<?php

/**
 * Model for Certificacion 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.model' );

// Initializes the Class
class ConsultaModelCertificacion extends JModel {
	
	/**
	 * Object Id
	 * @var int
	 */
	var $id;


	/**
	 * codigo de la certificacion
	 * @var string
	 */
	var $codigo;


	/**
	 * tipo de la certificacion
	 * @var string
	 */
	var $tipo_certificacion;

	/**
	 * fecha de inicio de la certificacion
	 * @var string
	 */
	var $fecha_inicio;

	/**
	 * fecha de fin de la certificacion
	 * @var string
	 */
	var $fecha_fin;

	/**
	 * id del producto asociado a la certificación
	 * @var string
	 */
	var $id_producto;

	/**
	 * id de la empresa asociado a la certificación
	 * @var string
	 */
	var $id_empresa;

	/**
	 * Attribute state
	 * @var bool
	 */
	var $state;
	
	/**
	 * Get if the object exists
	 * @var bool
	 */
	var $exists = false;
	
	/**
	 * Cache of results, data, queries
	 * @var unknown
	 */
	var $data;

	/**
	 * Constant for table
	 * @var string
	 */
	const TABLE = '#__certificacion';

	/**
	 * Constant for filters states
	 * @var string
	 */
	const FILTER_STATE = 'filter.component.';

	
	/**
	 * Attributes Map
	 * @var array
	 */
	var $attrs_map = array(
			'id'
		,	'codigo'
		,	'tipo_certificacion'
		,	'fecha_inicio'
		,	'fecha_fin'
		,	'id_producto'
		,	'id_empresa'
	);
	
	
	/**
	 * Methods
	 * 
	 */
	
	/**
	 * Constructor
	 * 
	 * @param { array || int } the args to instance the model or the single id
	 * 
	 */
	public function instance( $config = NULL ){
		
		if( is_numeric( $config ) )
			$config = array( 'id' => $config );
		
		if( ! is_array( $config ) )
			return;
		
		// Get existing object if the id was passed through
		return $this->fill( $config );
	}
	
	/**
	 * Fill the model attributes with the passed arguments.
	 *
	 * @param { arr } Object arguments
	 */
	protected function fill( $args = null ){


		if ( ! is_array( $args ) )
			return false;
		
	
		// Get object in DB			
		if ( is_numeric( $args[ 'id' ] ) ){

			$object = $this->getObject( $args[ 'id' ] );

			foreach ( $this->attrs_map as $attr ) {
				
				if ( isset( $object->$attr ) )
					$this->$attr = $object->$attr;
			}

		}

	
		// Merge attributes	when id is not passed through
		foreach ( $this->attrs_map as $attr ) {
			if ( isset( $args[ $attr ] ) )
				$this->$attr = $args[ $attr ];
		}

		// Set exists to true.
		$this->exists = true;
	
	}
	
	
	/**
	 * Get a single Object
	 * 
	 * @param { int } the id or attributes of the Object.
	 * @return { bool/object } the object returned or false otherwise
	 */
	protected function getObject( $id = 1 ){
		
		if( ! is_numeric( $id ) )
			return false;
		
		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );

		$query->select( '*' );
		$query->from( self::TABLE );
		$query->where( 'id = ' . $id );
		$db->setQuery( $query );
		
		return $db->loadObject();
		
	}
	
	/**
	 * Get Objects collection
	 *
	 * @param { int } the id or attributes of the Object.
	 * @return { bool/object } the object returned or false otherwise
	 **/
	public function getObjects( $wheres = NULL ){
		
		$result = array();
		
		if( ! is_array( $wheres ) )
			$wheres = array();
		
		// Verifies if data already contains a collection
		if( empty( $this->data ) ){
			
			$query = $this->buildQuery( $wheres );
			$this->data = $this->_getList( $query );
		}
		
		foreach ( $this->data as $obj ){

			$args = array();
			
			foreach ( $obj as $key => $attr ) {
				
				$args[ $key ] = $obj->$key;
			}
			
			$object = new ComponentModelObject();
			$object->fill( $args );
			array_push( $result, $object );
			
		}
	
		return $result;
	
	}

	/**
	 * Save a new object or update an exist.
	 *
	 * @param { string } the type of the response
	 * ( string returns the error string or "" )
	 * ( bool return false or true )
	 * ( object returns the response with error_message, status true or false )
	 * @return { string  } the query string calling the insert query
	 *
	 */
	public function save( $return = 'string' ){

		// Initialize
		$db = JFactory::getDbo();
		$response = ( object ) array();
		$model = ( object ) array();

		if( ! is_string( $return ) )
			$return = 'string';

		// Fetch the model attributes with the $model's var
		foreach ( $this->attrs_map as $attribute ) {
			$model->$attribute = $this->$attribute;
		}

		// If id exists, update the model
		// If id doesn't exist, insert a new row in database
		if( $model->id == NULL || $model->id == "" ){

			if (! $db->insertObject( self::TABLE, $model ) ) {

				if( $return == 'string' ){
					return "No se pudo guardar el object. " . $db->stderr();
				}

				if( $return == 'bool' ){
					return false;
				}

				if( $return == 'object' ){
					$response->status = false;
					$response->error = "No se pudo guardar el object. " . $db->stderr();
					return $response;
				}
			}

			if( $return == 'string' ){
				return "";
			}

			if( $return == 'bool' ){
				return true;
			}

			if( $return == 'object' ){
				$response->status = true;
				$response->error = "";
				return $response;
			}
		}

		// Update
		if ( ! $db->updateObject( self::TABLE, $model, 'id', false ) ) {
			
			if( $return == 'string' ){
				return "No se pudo actualizar el object. " . $db->stderr();
			}

			if( $return == 'bool' ){
				return false;
			}

			if( $return == 'object' ){
				$response->status = false;
				$response->error = "No se pudo guardar el object. " . $db->stderr();
				return $response;
			}
		}

		if( $return == 'string' ){
			return "";
		}

		if( $return == 'bool' ){
			return true;
		}

		if( $return == 'object' ){
			$response->status = true;
			$response->error = "";
			return $response;
		}

	}

	/**
	 * Truncates table from database
	 *
	 * @return { bool/object } the object returned or false otherwise
	 */
	public function truncate(){
	
		// Restart the table, clean the rows and restart id = 0

		$query = "TRUNCATE TABLE ". self::TABLE;
		$db = JFactory::getDbo();
		$db->setQuery( $query );
	
		return $db->query();
	
	}
	
	/**
	 * Delete object from database
	 *
	 * @param { array || int } the id of the object or array that contains the id
	 * @return { bool/object } the object returned or false otherwise
	 */
	public function delete(){
	
		if( ! is_numeric( $this->id ) )
			return false;
	
	
		// Delete existing object if the id was passed through

		$query = "DELETE FROM ". self::TABLE ." WHERE id = $this->id";
		$db = JFactory::getDbo();
		$db->setQuery( $query );
	
		return $db->query();
	
	}

	/**
	 * Publish or unpublish the object
	 * @param { string } the type of the response
	 * ( string returns the error string or "" )
	 * ( bool return false or true )
	 * ( object returns the response with error_message, status true or false )
	 * @return { string  } the state after change
	 *
	 */
	public function publish( $return = 'string' ){

		$db = JFactory::getDbo();
		$this->state = ($this->state == 1 ) ? 0 : 1;

		// Update
		$std = new stdClass();
		$std->id = $this->id;
		$std->state = $this->state;

		if (! $db->updateObject( self::TABLE, $std, 'id', false ) ) {
			
			if( $return == 'string' ){
				return "No se pudo actualizar el object. " . $db->stderr();
			}

			if( $return == 'bool' ){
				return false;
			}

			if( $return == 'object' ){
				$response->status = false;
				$response->error = "No se pudo actualizar el object. " . $db->stderr();
				return $response;
			}
		}

		if( $return == 'string' ){
			return "";
		}

		if( $return == 'bool' ){
			return true;
		}

		if( $return == 'object' ){
			$response->status = true;
			$response->error = "";
			return $response;
		}

	}
	
	// Helpers for the same Model
	// Next methods are utilities no core methods, therefore they all are protected
	
	public function getParams(){

		$mainframe =& JFactory::getApplication();

		foreach ( $this->attrs_map as $key => $param ) {

			$_param = ( object ) array();
			$_param->key = $param;
			$_param->value = $mainframe->getUserState( self::FILTER_STATE . $param );
			$_param->type = gettype( $_param->value );
			array_push( $this->filters, $_param );
		}
	}


	public function setParams(){

		$mainframe =& JFactory::getApplication();

		foreach ( $this->attrs_map as $key => $param ) {

			$mainframe->setUserState( self::FILTER_STATE . $param, JRequest::getVar( $param ) );
		}

		$this->getParams();
	}

	public function cleanParams(){

		$mainframe =& JFactory::getApplication();

		foreach ( $this->attrs_map as $key => $param ) {

			$mainframe->setUserState( self::FILTER_STATE . $param, " " );
		}

		$this->getParams();
	}
	
	/**
	 * Build a query for collection. Filters query are included.
	 *
	 * @param { array } wheres clausule. Clausule must be { key: 'value', value: 'value', condition:'=', glue: 'AND || OR' }
	 * @return { string } the query string calling the collection
	 *
	 */
	protected function buildQuery( $wheres = NULL ){

		// Validation
		if( ! is_array( $wheres ) )
			$wheres = array();

		// Initialize
		$db = JFactory::getDbo();
		$query  = $db->getQuery(true);

		// Get the filters
		$this->getParams();
		
		// Query base		
		$query->select( "*" );
		$query->from( self::TABLE);

		
		// Wheres appending
		foreach ( $wheres as $key => $clausule ) {
			
			if( ! is_object( $clausule ) )
				break;

			$query->where( $clausule->key . $clausule->condition . $clausule->value, $clausule->glue );

			var_dump($query);

		}

		// Filters appending
		foreach ( $this->filters as $key => $filter ) {

			if( $filter->type == 'string' ){
				$query->where( $filter->key . "like '%{ $filter->value }%'" );
				break;
			}
			
			$query->where( $filter->key . " = " . $filter->value );	

		}
		
		return $query;
	}

	/**
	 * API of the class
	 * 
	 * @return { void }
	 */
	protected function API(){
		
	
	}
}
?>