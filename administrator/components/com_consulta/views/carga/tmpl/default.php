<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();

//add the links to the external files into the head of the webpage (note the 'administrator' in the path, which is not nescessary if you are in the frontend)
$document =& JFactory::getDocument();
$document->addScript( '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js');
$document->addScript( $host.'administrator/components/com_consulta/assets/js/libs/fileuploader/fileuploader.js');
$document->addStyleSheet($host.'administrator/components/com_consulta/assets/js/libs/fileuploader/fileuploader.css');
$document->addStyleSheet($host.'administrator/components/com_consulta/assets/css/style.css');
$document->addScript( $host.'administrator/components/com_consulta/assets/js/misc/misc.js');
$document->addScript( $host.'administrator/components/com_consulta/assets/js/views/certificacion.js');
$document->addScript( $host.'administrator/components/com_consulta/assets/js/models/certificacion.js');
$document->addScript( $host.'administrator/components/com_consulta/assets/js/controllers/certificacion.js');
$document->addScript( $host.'administrator/components/com_consulta/assets/js/handlers/certificacion.js');
$document->addScript( $host.'administrator/components/com_consulta/assets/js/misc/file.js');
?>
<form action="index.php" method="post" name="adminForm" id="adminForm" class="form-validate">

	<fieldset class="adminform">
		<legend>Archivo Excel</legend>
		<p>Asegúrese que su archivo de excel conserva el mismo formato, puesto que los campos adicionales que no coincidan con la base de datos no se guardarán.</p>
		<ul class="excel-list">
		</ul>
		<div id="excel-uploader">       
			<noscript>          
			    <p>habilita javascript en tu navegador para poder subir archivos.</p>
			    <!-- or put a simple form for upload here -->
			</noscript>         
		</div>

		<p class="import-label">Cuando todos los archivos indiquen 100%, haga click en "Importar" para comenzar la importación de datos.</p>
		<a href="#" id="importar-datos">Importar datos</a>
		<p class="truncate-label"></p>
		<ul class="excel-parse-list"></ul>

		<div class="excel-reporte">
			<h3>Reporte</h3>
			<ul>
				<li>Errores en la conversión: <span class="excel-errors">0</span></li>
			</ul>
			<div class="excel-log">
			</div>
		</div>
	</fieldset>

	<input type="hidden" name="option" value="com_certificados" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="id" value="" />
	
</form>