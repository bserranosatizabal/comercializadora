<?php

/**
 * General View for "informes seguimiento" "lista" layout
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.view' );

class ConsultaViewCarga extends JViewLegacy {

	
	// Function that initializes the view
	function display( $tpl = null ){
	
		// Add the toolbar with the actions
		$this->addToolbar();
	
		parent::display( $tpl );
	
	}
	
	// Show the toolbar with CRUD modules
	protected function addToolbar(){
	
		JToolBarHelper::title( "Carga de Certificaciones", 'logo' );
		JToolBarHelper::cancel( "cancel" );
	}

}
?>