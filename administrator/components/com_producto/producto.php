<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
require_once( JPATH_COMPONENT . DS . 'controller.php' );

$controller = JControllerLegacy::getInstance('producto');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
?>