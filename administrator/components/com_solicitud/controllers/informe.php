<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );


// Begining of the controller
class SiControllerinforme extends JController{
	
	/**
	* Save or update a register
	* Call the model passing args
	*
	*/
	public function save(){


		$args = array(
				'id' 		=> JRequest::getVar( 'id' )
			,	'titulo' 	=> JRequest::getVar( 'titulo' )
			,	'ano'		=> JRequest::getVar( 'ano' )
			,	'tema'		=> JRequest::getVar( 'tema' )
			,	'tipo'		=> JRequest::getVar( 'tipo' )
			,	'state'		=> JRequest::getVar( 'state' )
			,	'ruta'		=> $_SESSION['si_file'] 
		);


		// Get the model
		$model = $this->getModel('informe');

		// Fill the model attributes with args
		$model->instance( $args );

		
		if( (! isset( $_SESSION['si_file']  ) || $_SESSION['si_file']  == "" )  && empty($model->ruta)){
			$this->setRedirect( "index.php?option=com_si&render=Edit", "Elija un archivo PDF para el informe.","error"  );
			return;
		}

		//save the model into DB
		$respuesta = $model->save();


		unset( $_SESSION['si_file'] );
		
	
		if( empty( $respuesta ) ){
				
			$this->setRedirect("index.php?option=com_si");
			return;
				
		}else{
				
			$this->setRedirect( "index.php?option=com_si&render=Edit",$respuesta,"error" );
			return;
				
		}
	
	}

	/**
	*
	* Uploads a file usinfg file uploader helper
	*
	*
	*/
	public function uploadFile(){

		// list of valid extensions, ex. array("jpeg", "xml", "bmp")
		$allowedExtensions = array( 'pdf' );
		// max file size in bytes
		$sizeLimit = 32 * 1024 * 1024;

		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);

		// Call handleUpload() with the name of the folder, relative to PHP's getcwd()
		$result = $uploader->handleUpload('../documentos/si/');


		$_SESSION['si_file'] = $result[ 'filename' ];

		// to pass data through iframe you will need to encode all html tags
		echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);

		die();

	}

	/**
	* Make a model request to publish or unpublish an informe
	*
	*
	*/

	public function publicar(){


		$id = JRequest::getVar("id");

		$model = $this->getModel( 'informe' );

		$model->instance($id);


		echo $model->publish();

		
		die();


	}

	
}
?>