
<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );

// Begining of the controller
class SiControllerlista extends JController{
	
	
	/**
	 *  Task that executes the list of Informes Seguimiento
	 */
	public function show(){
		
		// Call the view
		$view =& $this->getView( 'lista', 'html' );
		
		// Call the model
		$model =& $this->getModel( 'informe' );
		
		// Get all the Informes Seguimiento
		$informes = $model->getInformes();
		
		// Assign informes as reference to the view
		$view->assignRef( 'informes', $informes );
		
		// Display the view
		$view->display();
	}

	/**
	 *  Task that deletes the list of Informes Seguimiento
	 */
	public function delete(){
		
		//Het the items picked
		$pks = JRequest::getVar( 'cid' );

		//Instance a model for each item and delete
		foreach ( $pks as $id ) {
			
			// Call the model
			$model = $this->getModel( 'informe' );
			
			$model->instance( $id );

			$model->delete();
			
		}

		$this->setRedirect( "index.php?option=com_si" );

		return;
	}

	public function buscar(){


		$modelo = $this->getModel('informe');

		$modelo->setParams();

		$this->setRedirect( "index.php?option=com_si" );

		return;

	}

	public function limpiar(){


		$modelo = $this->getModel('informe');

		$modelo->setParams();

		$this->setRedirect( "index.php?option=com_si" );

		return;

	}

	/**
	 *  Task that publishes the list of Informes Seguimiento
	 */
	public function publish(){
		
		//Het the items picked
		$pks = JRequest::getVar( 'cid' );

		//Instance a model for each item and delete
		foreach ( $pks as $id ) {
			
			// Call the model
			$model = $this->getModel( 'informe' );

			$args = array(
				'id' => $id,
				'state' => 0
			);
			
			$model->instance( $args );

			$model->publish();
			
		}

		$this->setRedirect( "index.php?option=com_si" );

		return;
	}

	/**
	 *  Task that unpublishes the list of Informes Seguimiento
	 */
	public function unpublish(){
		
		//Het the items picked
		$pks = JRequest::getVar( 'cid' );

		//Instance a model for each item and delete
		foreach ( $pks as $id ) {
			
			// Call the model
			$model = $this->getModel( 'informe' );

			$args = array(
				'id' => $id,
				'state' => 1
			);
			
			$model->instance( $args );

			$model->publish();
			
		}

		$this->setRedirect( "index.php?option=com_si" );

		return;
	}
	
	
	
}
?>