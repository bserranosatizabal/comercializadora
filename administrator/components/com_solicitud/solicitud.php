<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
require_once( JPATH_COMPONENT . DS . 'controller.php' );
require_once( JPATH_COMPONENT . DS . 'helpers'.DS.'file.php' );

$controller = JControllerLegacy::getInstance('Component');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
?>