<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();
$document =& JFactory::getDocument();

$document->addStyleSheet($host.'administrator/components/com_si/assets/css/style.css');
?>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>

function cambiarEstado( estado, id, elemento){


	
	var url='index.php';

	$.ajax({			
		async:true,   
		cache:false,                 
		type: 'POST',  
		url: url,
		data: { task: "informe.publicar", option: "com_si", id: id },
		success:  function(respuesta){

			if(respuesta.length==0){
				location.reload();
			}else{
				alert(respuesta);
			}
		
		}
	});
}


</script>


<form action="index.php" method="post" name="adminForm">

	 <!-- Published Filter -->
    <div>
        <h3>Filtros</h3>
        <table>
	        <tr>
	            <td><label>Titulo</label></td>
	            <td><input type="text" name="texto" id="texto" value=""></td>
	        	<td><input type="button" onclick="Joomla.submitbutton('lista.buscar');" value="Buscar" ></td>
	            <td><label>Tema</label></td>
	            <td><?php 
	            	$this->SelectTipos( 0, true );
	            ?></td>
	            <td><?php 
	            	$this->SelectYears( 0, true );
	            ?></td>
	            <td><a href="#" onclick="Joomla.submitbutton('lista.limpiar');">Limpiar</a></td>
	        </tr>
        </table>
    </div>
	
	<div class="editcell">
		<table class="adminlist">
			<!-- Encabezado -->
			<thead>
				<tr>
					<th width="20">
						<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $this->informes ); ?>);" />
					</th>
					<th width="5">ID</th>
					<th>Nombre</th>
					<th>Tipo</th>
					<th>A&ntilde;o</th>
					<th>Publicado</th>
				</tr>
			</thead>
			
			<!-- Elementos -->
			<tbody>
				<?php
					
					$n = 0;
					
					//loop for the elements
					foreach ( $this->informes as $informe ):
						
						// Create the checkboxes
						$checked = JHtml::_( 'grid.id', $n, $informe->id );
				?>
				<tr>
					<td><?php echo $checked; ?></td>
					<td><?php echo $informe->id; ?></td>
					<td><a href="index.php?option=com_si&render=Edit&id=<?php echo $informe->id; ?>"><?php echo $informe->titulo; ?></a></td>
					<td><?php echo $informe->tipo->nombre; ?></td>
					<td><?php echo $informe->ano; ?></td>
					<td>
						<a class="jgrid" href="javascript:void(0);" onclick="cambiarEstado(<? echo $informe->state; ?>,<? echo $informe->id ?>,this)" title="Publicar Elemento"><span  class="state <?php echo ( $informe->state == '1' ) ? "publish" : "unpublish"; ?>"><span class="text">Despublicado</span></span></a>
					</td>
				</tr>
				<?php
						$n++;
					endforeach;
				?>
			</tbody>
		</table>
	</div>
	<input type="hidden" name="option" value="com_si" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="controller" value="lista" />
</form>