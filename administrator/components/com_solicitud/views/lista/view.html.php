<?php

/**
 * General View for "informes seguimiento" "lista" layout
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.view' );

class ComponentViewlista extends JView {
	// Function that initializes the view
	function display( $tpl = null ){
	
		// Add the toolbar with the CRUD modules defined
		$this->addToolbar();
	
		parent::display( $tpl );
	}
	
	// Show the toolbar with CRUD modules
	protected function addToolbar(){
		
		JToolBarHelper::title( "Lista de informes de sistemas de informaci&oacute;n", "codhes-logo" );
		JToolBarHelper::deleteList( "Desea eliminar estos informes?", "lista.delete" );
		JToolBarHelper::publishList( "lista.publish" );
		JToolBarHelper::unpublishList( "lista.unpublish" );
		JToolBarHelper::addNew( "renderEdit" );
	}

}
?>