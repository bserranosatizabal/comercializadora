<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();

// Get the args when updating an informe
$id = JRequest::getVar( 'id' );

if( ! empty( $id ) ){

	$model = new SiModelinforme();
	$model->instance( $id );
}	
 
//add the links to the external files into the head of the webpage (note the 'administrator' in the path, which is not nescessary if you are in the frontend)
$document =& JFactory::getDocument();
$document->addScript( '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js');
$document->addScript( $host.'administrator/components/com_si/assets/js/libs/fileuploader/fileuploader.js');
$document->addStyleSheet($host.'administrator/components/com_si/assets/js/libs/fileuploader/fileuploader.css');
$document->addStyleSheet($host.'administrator/components/com_si/assets/css/style.css');
$document->addScript( $host.'administrator/components/com_si/assets/js/misc/file.js');

?>

<script type="text/javascript">

Joomla.submitbutton = function(task){
	
	
			   if(task== 'informe.save'){

			   		titulo = document.getElementById("titulo");

			   		
			   		if(titulo.value.length==0){
			   			alert("El nombre es obligatorio");
			   			return;
			   		}


			   }

	  			Joomla.submitform(task, document.getElementById('adminForm'));
	  			
		
}

</script>


<form action="index.php" method="post" name="adminForm" id="adminForm" class="form-validate" enctype="multipart/form-data">
	<fieldset class="adminform">
		<legend>Edici&oacute;n de informe</legend>
		
		<ul>
			<li>
				<label class="required">Titulo</label>
				<input type="text" name="titulo" id="titulo"
				 value="<?php echo $model->titulo; ?>"
				 class="inputbox required" required="required" />
			</li>
			
			<li>
				<label class="required">A&ntilde;o</label>
				<?php $this->SelectYears( $model->ano ); ?>
			</li>

			<li>
				<label class="required">Tipo</label>
				<?php $this->SelectTipos( $model->tipo->id ); ?>
			</li>
		</ul>
	</fieldset>
	
	<fieldset class="adminform">
		<legend>Archivo PDF</legend>
		<ul class="pdf-list">
		<?php if( ! empty( $model->ruta ) ): ?>
			
			<li><i class="icon-pdf"></i><span><?php echo $model->ruta; ?></span></li>
		<?php
		endif; ?>
		</ul>
		<div id="file-uploader">       
			<noscript>          
			    <p>habilita javascript en tu navegador para poder subir archivos.</p>
			    <!-- or put a simple form for upload here -->
			</noscript>         
		</div>
	</fieldset>
	
	<fieldset class="adminform">
		<legend>Opciones de Publicaci&oacute;n</legend>
		
		<label>Publicar:</label>
		<ul>
			<li>
				<label class="required">Si</label>
				<input type="radio" name="state" id="state_1" value="1" <?php if( $model->state == "1" ): echo "checked='checked'"; endif; ?> />
			</li>
			<li>
				<label class="required">No</label>
				<input type="radio" name="state" id="state_0" value="0" <?php if( $model->state == "0" ): echo "checked='checked'"; endif; ?> />
			</li>
		</ul>
	</fieldset>

	<input type="hidden" name="option" value="com_si" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="id" value="<?php echo $model->id; ?>" />
	
</form>