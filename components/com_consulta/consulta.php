<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
require_once( JPATH_COMPONENT . DS . 'helpers/dompdf-master/dompdf_config.inc.php' );
require_once( JPATH_COMPONENT . DS . 'controller.php' );
require_once( JPATH_COMPONENT . DS . 'helpers/cert-generator.php' );

$controller = JControllerLegacy::getInstance('consulta');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
?>