<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );


// Begining of the controller
class ConsultaControllerConsulta extends JController{
	
	/**
	* Consult the certification by code 
	* Call the model passing args
	*
	*/
	public function searchCertificationBy(){

		// value and key 
		$value = JRequest::getVar('value');
		$key = JRequest::getVar('key');

		$response = ( object )array();

		// get model the certification

		$certificacion = $this->getModel('certificacion');


		// args to get the certification
		$args = array(
			0 => ( object )array(
				'key' => $key,
				'value' => $value,
				'condition' => ' = ',
				'glue' => 'AND'
			)
		);

		// instance the function that list the certification

		$certificaciones = $certificacion->getObjects( $args );

		if( count($certificaciones) <= 0 ){
			$response->status = 404;
			$response->message = "Certificación no encontrada.";
			echo json_encode( $response );
			die;
		}

		// loop through certifications to get empresa and producto

		foreach ($certificaciones as $key => $certificacion) {

			// get model empresa & producto
			$empresa = $this->getModel( 'empresa' );
			$producto = $this->getModel( 'productos' );

			// instance the models with Id of empresa & id of producto

			$empresa->instance( $certificacion->id_empresa );
			$producto->instance( $certificacion->id_producto );

			$response->empresa = $empresa;
			$response->producto = $producto;

		}

		$response->status = 200;
		$response->certificacion = $certificaciones;

		echo json_encode( $response );
		die;
		
	}

	/**
	* Consult the certification by serial 
	* Call the model passing args
	*
	*/
	public function searchCertificationBySerial(){
		// value and key 
		$value = JRequest::getVar('value');
		$key = JRequest::getVar('key');

		$response = ( object )array();

		// get model the certification

		$serial = $this->getModel('seriales');



		// args to get the certification
		$args = array(
			0 => ( object )array(
				'key' => $key,
				'value' => $value,
				'condition' => ' = ',
				'glue' => ''
			)
		);

		
		// instance the function that list the certification

		$seriales = $serial->getObjects( $args );

		if( count($seriales) <= 0 ){
			$response->status = 404;
			$response->message = "Certificación no encontrada.";
			echo json_encode( $response );
			die;
		}

		$response->status = 200;
		$response->serial = $seriales;

		echo json_encode( $response );
		die;
	}


	/**
	* Generate the certification by code 
	* 
	*
	*/

	public function generatePdfCertificacion(){

		//get html of certification
		$html = JRequest::getVar('html');

		// get code of certification
		$codigo = JRequest::getVar('codigo');

		$data = ( object ) array();
		$response = ( object )array();

		$data->header->certificado_number = $codigo;
		$data->content = $html;

		$certificado = certGen::getCertificadoByCode( $data );

		$response->path = 'certificados/codigo/' . $data->header->certificado_number . '.pdf';

		$response->status = 200;

		echo json_encode( $response );
		die;

	}

	/**
	* Generate the certification by serial 
	* 
	*
	*/

	public function generatePdfSerial(){

		//get html of certification
		$html = JRequest::getVar('html');

		// get code of certification
		$codigo = JRequest::getVar('codigo');

		// get serial of certification
		$serial = JRequest::getVar('serial');

		$data = ( object ) array();
		$response = ( object )array();

		$data->header->certificado_number = $serial;
		$data->header->codigo_certificado = $codigo;
		$data->content = $html;

		$serial = certGen::getCertificadoBySerial( $data );

		$response->path = 'certificados/seriales/' . $data->header->certificado_number . '.pdf';

		$response->status = 200;

		echo json_encode( $response );
		die;
	}


	
}
?>