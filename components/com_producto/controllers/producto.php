<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );


// Begining of the controller
class ProductoControllerProducto extends JController{
	
	public function saveVehiculo(){
		
		$object = new stdClass();

		$data = JRequest::getVar('data');

		$vehiculo = $data['vehiculo'];

		$products = $data['productos'];

		$vehiculos = $this->getModel( 'vehiculos' );

		$exist = $vehiculos->getVehiculo( strtoupper($vehiculo['placa']) );

		if ( empty($exist) ) {
			$id = NULL;
			$fecha_instalacion = $vehiculo['fecha_instalacion'];
		}else{
			$id = $exist->id;
			$fecha_instalacion = $exist->fecha_instalacion;
		}

		// get user logged
		$user = JFactory::getUser();
		$idUser = $user->get('id');

		$dataUser = ( object )array();

		$dataUser->mailUser = $user->get('email');
		$dataUser->nameUser = $user->get('name');

		$vehiculos = $this->getModel( 'vehiculos' );

		// args to model
		$args = array(
				'id' => $id
			,	'placa' => $vehiculo['placa']
			,	'fecha_instalacion' => $fecha_instalacion
			,	'id_user' => $idUser
		);


		$vehiculos->instance( $args );

		if ( ! $vehiculos->save('bool')) {
			$response->status = 500;
			$response->message = 'No se pudo guardar el vehiculo';
			echo json_encode( $response );
	        die;
		}

		if ( empty($exist) ) {

			$id_vehiculo = $vehiculos->insertId;
		}else{
			
			$id_vehiculo = $exist->id;
		}

		$orden = $this->saveOrdenProducto( $id_vehiculo, $vehiculo['kilometraje'] );
		
		if ( $orden->status == 500 ){
			$response->status = 500;
			$response->message = 'No se pudo guardar la orden';
			echo json_encode( $response );
	        die;
		}

		$productos = $this->saveProductos( $orden->radicado, $data['productos'] );

		if ( $productos->productStatus == 404 ) {

			$response->productStatus = $productos->productStatus;
			$response->productMessage = $productos->productMessage;
			echo json_encode( $response );
        	die;
		}

		$this->sendMailUser( $dataUser, $orden->radicado, $vehiculo, $products );

		$this->sendMailGroup( $idUser, $orden->radicado, $vehiculo, $products );

		$response->radicado = $orden->radicado;
		$response->status = 200;
		$response->message = 'Orden guardada correctamente.';
		echo json_encode( $response );
        die;
	}

	public function saveOrdenProducto( $idVehiculo, $kilometraje ){

		$response = new stdClass();

		$date = date( 'Y-m-d' );

		// args to model
		$args = array(
				'id_vehiculo' => $idVehiculo
			,	'fecha' => $date
			,	'kilometraje' => $kilometraje
		);

		$ordenes = $this->getModel( 'ordenes' );

		$ordenes->instance( $args );

		if ( ! $ordenes->save('bool')) {
			$response->status = 500;
			return $response;
		}

		$response->radicado = $ordenes->insertId;

		return $response;

	}

	/**
	* Send mail user when create a warranty order
	*/
	public function sendMailUser( $dataUser, $id, $data, $productos ){

		$curDate = Misc::spanishDate( false );
		$fecha_instalacion = Misc::formatDateSpanish( $data['fecha_instalacion'] );

		$mail = JFactory::getMailer();

        $contenido = "<div><img src='".JURI::root()."images/header-pdf.jpg'><br><br><br><br>";
        $contenido .= "Usted ha creado una nueva solicitud de producto con los siguientes datos:<br>";

        $contenido .= "No radicado: 0000". $id ."<br>";
        $contenido .= "Fecha y hora de la radicación de la orden de producto:". $curDate ."<br>";
        $contenido .= "Placa del vehiculo:". $data['placa'] ."<br>";
        $contenido .= "Kilometraje:". $data['kilometraje'] ."<br>";
        $contenido .= "Fecha de instalación:". $fecha_instalacion ."<br>";

        $contenido .= "<br>PRODUCTOS ASOCIADOS A LA ORDEN DE PRODUCTO GENERADA.<br>";

        $contenido .= "
      	<table>
		<tr>
			<td>Serial</td>
			<td>Referencia</td>
			<td>Tipo Producto</td>
		</tr>";

		foreach ($productos as $key => $item) {
			
			$contenido .= '<tr>
				<td>'.$item['serial'].'</td>
				<td>'.$item['referencia'].'</td>
				<td>'.$item['tipo'].'</td>
			</tr>';
				
		}

        $contenido .= "</table><br>";


        $contenido .= "<br>Muchas gracias por utilizar nuestros servicios.<br>";
        $contenido .x= "<img src='".JURI::root()."images/pie-pdf.jpg'></div>";

        $mail->setSender( 'info@comercializadoragm.com' );
        $mail->addRecipient( $dataUser->mailUser );
        $mail->setSubject( "Usted ha creado una nueva orden de producto." );
        $mail->Encoding = 'base64';
        $mail->isHtml( true );
        $mail->setBody( $contenido );

    	//enviar email
       	if ( ! $mail->Send()) {
       		$response->status = 500;
       	}
	}

	/**
	* Send mail user when create a warranty order
	*/
	public function sendMailGroup( $idUser, $id, $data, $productos ){

		$curDate = Misc::spanishDate( false );
		$fecha_instalacion = Misc::formatDateSpanish( $data['fecha_instalacion'] );

		var_dump($idUser);

		$model = $this->getModel( 'producto' );

		$user = $model->getUserGroup( $idUser );

		$mail = JFactory::getMailer();

        $contenido = "<div><img src='".JURI::root()."images/header-pdf.jpg'><br><br><br><br>";
        $contenido .= "Usted ha creado una nueva solicitud de producto con los siguientes datos:<br>";

        $contenido .= "No radicado: 0000". $id ."<br>";
        $contenido .= "Fecha y hora de la radicación de la orden de producto:". $curDate ."<br>";
        $contenido .= "Placa del vehiculo:". $data['placa'] ."<br>";
        $contenido .= "Kilometraje:". $data['kilometraje'] ."<br>";
        $contenido .= "Fecha de instalación:". $fecha_instalacion ."<br>";

        $contenido .= "<br>PRODUCTOS ASOCIADOS A LA ORDEN DE PRODUCTO GENERADA.<br>";

        $contenido .= "
      	<table>
		<tr>
			<td>Serial</td>
			<td>Referencia</td>
			<td>Tipo Producto</td>
		</tr>";

		foreach ($productos as $key => $item) {
			
			$contenido .= '<tr>
				<td>'.$item['serial'].'</td>
				<td>'.$item['referencia'].'</td>
				<td>'.$item['tipo'].'</td>
			</tr>';
				
		}

        $contenido .= "</table><br>";


        $contenido .= "<br>Muchas gracias por utilizar nuestros servicios.<br>";
        $contenido .= "<img src='".JURI::root()."images/pie-pdf.jpg'></div>";

        $mail->setSender( 'info@comercializadoragm.com' );
        $mail->addRecipient( $user->correo );
        $mail->setSubject( "Usted ha creado una nueva orden de producto." );
        $mail->Encoding = 'base64';
        $mail->isHtml( true );
        $mail->setBody( $contenido );

    	//enviar email
       	if ( ! $mail->Send()) {
       		$response->status = 500;
       	}
	}

	public function saveProductos( $idOrden, $productos ){

		$object = new stdClass();


		foreach ($productos as $key => $item) {

			$producto = $this->getModel( 'producto' );

			$serial = $producto->getProductoBySerial( $item['serial'] );

			$count = count($serial);

			if( $count >= 1 ){

				$object->productStatus = 404;
				$object->productMessage = 'El producto con serial '. $item['serial'].' ya existe';

				return $object;

			}

			$args = array(
					'referencia' => $item['referencia']
				,	'serial' => $item['serial']
				,	'kilometraje' => $kilometraje
				,	'foto' => $item['foto']
				,	'id_orden' => $idOrden
				,	'id_tipo' => $item['id_tipo']
			);

			$producto->instance( $args );

			if ( ! $producto->save('bool') ) {

				$object->productStatus = 500;
				$object->productMessage = 'El producto con serial '. $item['serial'].' ya esta registrado en la base de datos.';

				return $object;
			}

		}

	}

	public function generatePdf(){

		$data = JRequest::getVar('data');

		$productos = $data['productos'];

		$vehiculo = $data['vehiculo'];

		$codigo = JRequest::getVar('codigo');

		$data = ( object ) array();
		$response = ( object )array();

		$data->header->certificado_number = $codigo;
		$data->vehiculo = $vehiculo;
		$data->productos = $productos;

		$certificado = certGen::getCertificado( $data );

		$response->path = 'productos/' . $data->header->certificado_number . '.pdf';

		$response->status = 200;

		echo json_encode( $response );
		die;
		
	}

	/**
	*
	* Uploads and parse and Image file
	*
	*/
	public function uploadImages(){

		// list of valid extensions, ex. array("jpeg", "xml", "bmp")
		$allowedExtensions = array( 'jpg', 'png', 'jpeg' );
		// max file size in bytes
		$sizeLimit = 2 * 1024 * 1024;

		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);

		// Call handleUpload() with the name of the folder, relative to PHP's getcwd()
		$result = $uploader->handleUpload('imgproductos/tmp/');

		// to pass data through iframe you will need to encode all html tags
		echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);

		die();
	}
}
?>