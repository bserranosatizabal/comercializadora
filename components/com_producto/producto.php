<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
require_once( JPATH_COMPONENT . DS . 'helpers/misc.php' );
require_once( JPATH_COMPONENT . DS . 'helpers/dompdf-master/dompdf_config.inc.php' );
require_once( JPATH_COMPONENT . DS . 'controller.php' );
require_once( JPATH_COMPONENT . DS . 'helpers/cert-generator.php' );
require_once( JPATH_COMPONENT . DS . 'helpers' . DS . 'uploader.php' );

$controller = JControllerLegacy::getInstance('producto');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
?>