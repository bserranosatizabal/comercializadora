<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();
$document = JFactory::getDocument();
$app = JFactory::getApplication();

$document->addStyleSheet($host.'less/load-styles.php?load=productos');
jimport('joomla.application.module.helper');
$module = JModuleHelper::getModule('mod_vm_login');

if ($this->group != '17'  ) {
	$session = JFactory::getSession(); // starting the session
	$session->destroy(); //This will destroy the joomla session
	$app->redirect('index.php');	
}

?>

<div class="close-session">
	<?php echo JModuleHelper::renderModule($module); ?>
</div>

<?php echo $this->renderMenu(); ?>

<div class="content-productos">
	<h2>Registrar producto</h2>

	<form id="vehiculo-form">
		<ul class="two-columns">
			<li><input type="text" name="placa" placeholder="Placa" /></li>
			<li><input type="text" name="kilometraje" placeholder="Kilometraje" /></li>
			<li><label>Fecha instalación</label><input type="text" name="fecha_instalacion" placeholder="Fecha instalación" class="fecha" readonly id="instalacion"/><i class="calendar" data-calendar="#instalacion"></i></li>
		</ul>
	</form>

	<?php foreach ($this->tipos as $key => $tipo) {
	?>

		<fieldset>
			<form id="productos-form" class="<?php echo strtolower($tipo->nombre); ?>">
				<ul class="input-checkbox">
					<li><input type="checkbox" value="<?= $tipo->id ?>" name="<?php echo strtolower($tipo->nombre); ?>" id="<?php echo strtolower($tipo->nombre); ?>"/><label for="<?php echo strtolower($tipo->nombre); ?>"><?= $tipo->nombre ?></label></li>
				</ul>

				<div class="medium">
				
					<ul class="fields-productos">
						<li><input type="text" value="" name="referencia" placeholder="Referencia" id="referencia" /></li>
						<li><input type="text" value="" name="serial" placeholder="Serial" id="serial" /></li>
						<li>
							<div class="wrapper-images-uploader">
					           <div class="product-picture-uploader" id="image-uploader-<?php echo $tipo->nombre; ?>">       
					               <noscript>          
					                   <p>Please enable JavaScript to use file uploader.</p>
					                   <!-- or put a simple form for upload here -->
					               </noscript>         
					           </div>
					       </div>
					   	</li>
						<li><button type="button" class="button-app" value="Añadir" id="add-product-orden" data-form="<?php echo strtolower($tipo->nombre); ?>">Añadir</button></li>
					</ul>

					<table class="content-products">
						<thead>
							<th>Eliminar</th>
							<th>Referencia</th>
							<th>Serial</th>
							<th>Imagen</th>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>

				<input type="hidden" value="<?php echo strtolower($tipo->nombre); ?>" name="tipo" />
				<input type="hidden" value="<?php echo $tipo->id; ?>" name="id_tipo" />


			</form>
		</fieldset>

	<?php
	} ?>
	<input type="button" class="button-app" value="Enviar" id="button-add-orden" />
</div>