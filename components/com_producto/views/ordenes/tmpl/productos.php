<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();
$document = JFactory::getDocument();
$app = JFactory::getApplication();

$document->addStyleSheet($host.'less/load-styles.php?load=productos');
jimport('joomla.application.module.helper');
$module = JModuleHelper::getModule('mod_vm_login');

if ( $this->group == '13' || $this->group == '17' ) {
	$session = JFactory::getSession(); // starting the session
	$session->destroy(); //This will destroy the joomla session
	$app->redirect('index.php');	
}

// Load the tooltip behavior.
JHtml::_('behavior.multiselect');
JHtml::_('behavior.framework');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.modal');


$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));

$document->addScriptDeclaration( 
	'var inicio = "' . $this->state->get( 'filter.id_user' ) . '";'

	 );
?>

<div class="close-session">
	<?php echo JModuleHelper::renderModule($module); ?>
</div>

<?php echo $this->renderMenu(); ?>
<div class="content-productos">
	<div class="productos">
		<h2>A continuación encontrará las ordenes generadas por el cliente.</h2>
		<form action="<?php echo JRoute::_('?layout=productos');?>" method="post" name="adminForm" id="adminForm">
			<div class="form-productos-reportados">
				<div class="top">
					<ul class="filters">
						<li><input type="text" name="identificacion" value="<?php echo $this->state->get( 'filter.identificacion' );?>" id="identificacion" placeholder="Nit o razón social" ></li>
						<li><input type="text" name="placa" value="<?php echo $this->state->get( 'filter.placa' );?>" id="placa" placeholder="Placa" ></li>
						<li><label>Fecha de instalación</label><input readonly type="text" name="inicio" value="<?php echo $this->state->get( 'filter.inicio' );?>" id="inicio" class="datepicker-input" placeholder="AAAA/MM/DD" ><i class="calendar" data-calendar="#inicio"></i></li>
						<li><input type="text" readonly name="final" value="<?php echo $this->state->get( 'filter.final' );?>" id="final" class="datepicker-input" placeholder="AAAA/MM/DD" ><i class="calendar" data-calendar="#final"></i></li>
						
					</ul>
					<button type="button" class="limpiar-filtros" ><?php echo JText::_('Limpiar'); ?></button>
					
				</div>
				<div class="bottom"> 
					<div class="header">
						<table>
							<thead>
								<tr>
									<th><span>Serial</span></th>
									<th><span>Cliente</span></th>
									<th><span>Producto</span></th>
									<th style="width: 100px;"><span><?php echo JHtml::_('grid.sort', 'Placa', 'placa', $listhirn, $listOrder); ?></span></th>
									<th><span>Kilometraje</span></th>
									<th><span><?php echo JHtml::_('grid.sort', 'Fecha de registro', 'fecha', $listhirn, $listOrder); ?></span></th>
									<th><span><?php echo JHtml::_('grid.sort', 'Fecha de instalación', 'fecha_instalacion', $listhirn, $listOrder); ?></span></th>
									<th><span>Foto</span></th>
								</tr>
							</thead>
						<?php if (count( $this->productos) <= 0) {
						?>
						<tbody>
						<tr><td colspan="8"><h3>No hay resultados</h3></td></tr></tbody></table>
						<?php
						}else{ ?>
						<tbody>
							<?php foreach ($this->productos as $key => $producto) {
								
							?>
							<tr>
								<td><?php echo $producto->serial ?></td>
								<td><?php echo $producto->name ?></td>
								<td><?php echo $producto->nombre ?></td>
								<td><?php echo $producto->placa ?></td>
								<td><?php echo $producto->kilometraje ?></td>
								<td><?php echo $producto->fecha ?></td>
								<td><?php echo $producto->fecha_instalacion ?></td>
								<td><a class="button-foto" href="#" data-foto="<? echo $producto->foto ?>" >Ver foto</a></td>
							</tr>
							<?php
							} ?>
							
						</tbody></table>
						<?php } ?>
						 
					</div>	
				</div>
				<div class="wrapper-paginator">
					<?php echo $this->pagination->getListFooter(); ?>
				</div>
			</div>
			
			<div>
				<input type="hidden" name="task" value="" />
				<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
				<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
				<input type="hidden" name="boxchecked" value="0" />
				<?php echo JHtml::_('form.token'); ?>
			</div>	
		</form>
		
		</div>           
</div>
