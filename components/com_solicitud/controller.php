<?php
/**
 * @copyright	Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Helloworld Controller
 *
 * @package		Joomla.Administrator
 * @subpackage	com_helloworld
 */
class SolicitudController extends JControllerLegacy {


	/**
	 * Method to display a view.
	 *
	 * @param	boolean			If true, the view output will be cached
	 * @param	array			An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return	JController		This object to support chaining.
	 * @since	1.5
	 */
	public function display( $cachable = false, $urlparams = false) {
		
		// Call all the models here so the views can access them

		// Display the view passed as url params
		parent::display( $cachable, $urlparams );
	}
}