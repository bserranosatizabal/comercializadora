
<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );

// Begining of the controller
class SolicitudControllergarantia extends JController{

	/**
	* Save warranty
	*/
	public function saveGarantia(){
		
		$data = JRequest::getVar('data');

		$productos = JRequest::getVar( 'productos' );

		$response = ( object )array();

		// model of servicio
		$garantia = $this->getModel( 'garantia' );

		// get user logged
		$user = JFactory::getUser();
		$idUser = $user->get('id');

		$dataUser = ( object )array();

		$dataUser->mailUser = $user->get('email');
		$dataUser->nameUser = $user->get('name');

		$date = date( 'Y-m-d' );

		$lastRadicado = $garantia->getMaxObject();

		$radicado = intval($lastRadicado[0]);

		$last = 0;

		if( $radicado == 0 ){
			$last = 1;
		}else{
			$last = $radicado + 1;
		}

		// args to model
		$args = array(
				'placa' => $data['placa']
			,	'kilometraje' => $data['kilometraje']
			,	'sistema' => $data['sistema']
			,	'fecha_instalacion' => $data['fecha_instalacion']
			,	'fecha_radicacion' => $date
			,	'no_radicado' => $last
			,	'estado' => '5'
			,	'id_user' => $idUser
		);

		$garantia->instance( $args );

		if (! $garantia->save('bool')) {
			$response->status = 500;
			$response->messagen = 'La solicitud de garantía no ha sido guardada.';

			echo json_encode($response);
			die;
		}

		// Instance function save products by id garantia
		$this->saveProductos( $garantia->insertId, $productos );

		$this->sendMailUser( $dataUser, $last, $data, $productos );
		$this->sendMailRec( $dataUser, $last, $data, $productos );

		$response->radicado = $last;
		$response->status = 200;
		$response->message = 'Su solicitud ha sido generada correctamente';
		echo json_encode( $response );
        die;

	}

	/**
	* Send mail user when create a warranty order
	*/
	public function sendMailUser( $dataUser, $id, $data, $productos ){

		$curDate = Misc::spanishDate( false );
		$fecha_instalacion = Misc::formatDateSpanish( $data['fecha_instalacion'] );

		$mail = JFactory::getMailer();

        $contenido = "<div><img src='".JURI::root()."images/header-pdf.jpg'><br><br><br><br>";
        $contenido .= "Usted ha creado una nueva solicitud de garantía con los siguientes datos:<br>";

        $contenido .= "No radicado: 0000". $id ."<br>";
        $contenido .= "Fecha y hora de la radicación de la garantía:". $curDate ."<br>";
        $contenido .= "Placa del vehiculo:". $data['placa'] ."<br>";
        $contenido .= "Kilometraje:". $data['kilometraje'] ."<br>";
        $contenido .= "Fecha de instalación:". $fecha_instalacion ."<br>";
        $contenido .= "Sistema instalado:". $data['sistema'] ."<br>";

        $contenido .= "<br>PRODUCTOS ASOCIADOS A LA ORDEN DE GARANTÍA GENERADA.<br>";

        $contenido .= "
      	<table>
		<tr>
			<td>Id. Producto</td>
			<td>Nombre producto</td>
			<td>Serial</td>
			<td>Descripción Fallo</td>
		</tr>";

		foreach ($productos as $key => $item) {
			
			$contenido .= '<tr>
				<td>'.$item['id_producto'].'</td>
				<td>'.$item['nombre_producto'].'</td>
				<td>'.$item['serial'].'</td>
				<td>'.$item['descripcion_fallo'].'</td>
			</tr>';
				
		}

        $contenido .= "</table><br>";


        $contenido .= "<br>Muchas gracias por utilizar nuestros servicios.<br>";
        $contenido .= "<img src='".JURI::root()."images/pie-pdf.jpg'></div>";


        $mail->setSender( 'info@comercializadoragm.com' );
        $mail->addRecipient( $dataUser->mailUser );
        $mail->setSubject( "Usted ha creado una nueva solicitud de garantía." );
        $mail->Encoding = 'base64';
        $mail->isHtml( true );
        $mail->setBody( $contenido );

    	//enviar email
       	if ( ! $mail->Send()) {
       		$response->status = 500;
       	}
	}


	/**
	* Send mail of groups assigned when create a warranty order
	*/
	public function sendMailRec( $dataUser, $last, $data ){

		$solicitud = $this->getModel( 'garantia' );
		$users = $solicitud->getUserGroups();

		foreach ($users as $key => $user) {
			
			$curDate = Misc::spanishDate( false );
			$fecha_visita = Misc::formatDateSpanish( $data['fecha_visita'] );
			$fecha_instalacion = Misc::formatDateSpanish( $data['fecha_instalacion'] );

			$mail = JFactory::getMailer();

	        $contenido = "<div><img src='".JURI::root()."images/header-pdf.jpg'><br><br><br><br>";
	        $contenido .= "Han creado una solicitud de garantía con los siguientes datos:<br>";

	        $contenido .= "No radicado: 0000". $id ."<br>";
	        $contenido .= "Fecha y hora de la radicación del servicio:". $curDate ."<br>";
	        $contenido .= "Nombre solicitante:". $dataUser->nameUser ."<br>";
	        $contenido .= "Correo solicitante:". $dataUser->mailUser ."<br>";
	        $contenido .= "Placa del vehículo:". $data['placa'] ."<br>";
	        $contenido .= "Kilometraje:". $data['kilometraje'] ."<br>";
	        $contenido .= "Fecha de instalación:". $fecha_instalacion ."<br>";
	        $contenido .= "Sistema instalado:". $data['sistema'] ."<br>";

	        $contenido .= "<br>PRODUCTOS ASOCIADOS A LA ORDEN DE GARANTÍA GENERADA.<br>";

	        $contenido .= "
	      	<table>
			<tr>
				<td>Id. Producto</td>
				<td>Nombre producto</td>
				<td>Serial</td>
				<td>Descripción Fallo</td>
			</tr>";

			foreach ($productos as $key => $item) {
				
				$contenido .= '<tr>
					<td>'.$item['id_producto'].'</td>
					<td>'.$item['nombre_producto'].'</td>
					<td>'.$item['serial'].'</td>
					<td>'.$item['descripcion_fallo'].'</td>
				</tr>';
					
			}

	        $contenido .= "</table><br>";

	        $contenido .= "<br>Muchas gracias por utilizar nuestros servicios.<br>";
	        $contenido .= "<img src='".JURI::root()."images/pie-pdf.jpg'></div>";

	        $mail->setSender( 'info@comercializadoragm.com' );
	        $mail->addRecipient( $user->email );
	        $mail->setSubject( "Usted ha creado una nueva solicitud de garantía." );
	        $mail->Encoding = 'base64';
	        $mail->isHtml( true );
	        $mail->setBody( $contenido );

	    	//enviar email
	       	if ( ! $mail->Send()) {
	       		$response->status = 500;
	       	}

		}

		

	}

	/**
	* Save products of warranty
	*/

	public function saveProductos( $idGarantia, $productos){

		$response = ( object )array();

		foreach ($productos as $key => $item) {

			$producto = $this->getModel( 'producto' );


			// args to model
			$args = array(
					'id_garantia' => $idGarantia
				,	'id_producto' => $item['id_producto']
				,	'serial' => $item['serial']
				,	'descripcion_fallo' => $item['descripcion_fallo']
				,	'estado' => '1'
			);

			$producto->instance( $args );

			$producto->save( 'bool' );

		}
	}

	/**
	* Generate pdf of warranty detail
	*/
	public function generatePdfGarantia(){

		//get garantia of certification
		$garantia = JRequest::getVar('garantia');

		$productos = JRequest::getVar('productos');


		// get code of certification
		$codigo = JRequest::getVar('codigo');

		$data = ( object ) array();
		$response = ( object )array();

		$data->header->certificado_number = $codigo;
		$data->garantia = $garantia;
		$data->productos = $productos;

		$certificado = garantiaGen::getCertificado( $data );

		$response->cliente = 'garantias/' . $data->header->certificado_number . '-cliente.pdf';
		$response->path = 'garantias/' . $data->header->certificado_number . '.pdf';

		$response->status = 200;

		echo json_encode( $response );
		die;
	}


	/**
	* Save reply or comment of customer about product
	*/
	public function saveReplica(){
		
		$response = ( object )array();

		$data = JRequest::getVar( 'data' );

		$producto = $this->getModel( 'producto' );

		$producto->instance( $data );

		if (! $producto->save( 'bool' ) ) {

			$response->message = 'La Replica no pudo ser enviada.';
			$response->status = 500;
			echo json_encode( $response );
			die;

		}

		$response->message = 'Replica enviada correctamente.';
		$response->status = 200;
		echo json_encode( $response );
		die;
	
	}

	/**
	* Save gestion of each product
	*/
	public function saveGestion(){

		$response = ( object )array();

		$data = JRequest::getVar( 'data' );

		$producto = $this->getModel( 'producto' );

		if ( $data['estado'] == '7' ) {
			
			$data['fecha_recepcion'] = date( 'Y-m-d' );
		}

		if ( $data['estado'] == '8' ) {
			
			$data['fecha_radicado'] = date( 'Y-m-d' );
		}

		$producto->instance( $data );
		

		if ( ! $producto->save( 'bool' ) ) {
			$response->message = 'El producto no pudo ser guardado.';
			$response->status = 500;
			echo json_encode( $response );
			die;
		}

		$response->view = $data['curview'];
		$response->garantia = $data['id_garantia'];
		$response->message = 'Producto guardado correctamente.';
		$response->status = 200;
		echo json_encode( $response );
		die;
	}

	/**
	* Get product by id
	*/
	public function getProductoById(){
		// get id product
		$id = JRequest::getInt('id');

		$model = $this->getModel( 'producto' );
		$garantia = $this->getModel( 'garantia' );

		// get product by id of garantia
		$producto = $model->getProducto( $id );

		$estados = $garantia->getEstados();
		$ciudad = $garantia->getEstadosProductosCiudad();
		$tipos = $garantia->getTipos();
		$warranty = $garantia->getGarantiaById( $producto->id_garantia );


		$view = $this->getView( 'producto', 'html' );

		$view->assignRef( 'estados', $estados );
		$view->assignRef( 'producto', $producto );
		$view->assignRef( 'ciudad', $ciudad );
		$view->assignRef( 'tipo', $tipos );
		$view->assignRef( 'garantia', $warranty );

		$view->setLayout( 'productodetalle' );

		$view->display();

	}

	/**
	* Get warranty by id
	*/
	public function getGarantiaById(){

		// get id garantia
		$id = JRequest::getVar('id');



		// get model of garantia
		$model = $this->getModel( 'garantia' );
		// get garantia by id
		$garantia = $model->getGarantiaById( $id );


		// get product by id of garantia
		$producto = $this->getProductByIdGarantia( $garantia->id );

		$view = $this->getView( 'garantia', 'html' );

		$view->assignRef( 'garantia', $garantia );
		$view->assignRef( 'productOnly', $producto );

		$view->setLayout( 'gestiondetalle' );

		$view->display();


	}


	/**
	* Get products of warranty by id user
	*/
	public function getProductByIdUser(){

		// get id garantia
		$id = JRequest::getVar('id');

		// get model of garantia
		$model = $this->getModel( 'garantia' );
		// get garantia by id
		$garantia = $model->getGarantiaById( $id );

		// get product by id of garantia
		$producto = $this->getProductByIdGarantia( $garantia->id );

		$view = $this->getView( 'garantia', 'html' );

		$view->assignRef( 'garantia', $garantia );
		$view->assignRef( 'products', $producto );

		$view->setLayout( 'detallegarantia' );

		$view->display();


	}

	/**
	* Get product by id of warranty
	*/
	public function getProductByIdGarantia( $id ){
		
		// get model producto
		$model = $this->getModel( 'producto' );

		// get product by id garantia
		$producto = $model->getProductoById( $id );

		return $producto;
	}

	/**
	* Change state products 
	*/
	public function changeStateProducts(){

		$response = ( object )array();
		$data = JRequest::getVar( 'data' );

		foreach ($data['ids'] as $key => $id) {

			$producto = $this->getModel( 'producto' );

			if ( $data['estado'] == '8' || $data['estado'] == '7' ){
				$recepcion = date( 'Y-m-d' );
				$radicado = date( 'Y-m-d' );
			}
			$args = array( 
				'id' => $id,
				'estado' => $data['estado'],
				'recepcion' => $recepcion,
				'radicado' => $radicado 
			);

			$item = $producto->getObject( intval( $id ) );

			if ( $item->fecha_recepcion == '0000-00-00' ) {

				$producto->changeState( $args );

				$this->changeStateClose();
			}
			
			

		}

		$response->message = 'Productos actualizados correctamente.';
		$response->status = 200;
		echo json_encode( $response );
		die;

	}

	/**
	* Calculate warranty days of each product
	*/
	public function diasGarantia(){


		$model = $this->getModel( 'producto' );

		$productos = $model->getProductos();

		foreach ($productos as $key => $item) {

			if( $item->fecha_recepcion != '0000-00-00' && $item->estado == '8' ){

				$now = time(); // or your date as well
				$your_date = strtotime($item->fecha_recepcion);
				$datediff = $now - $your_date;
				$diasGarantia = floor($datediff/(60*60*24));

				$producto = $this->getModel( 'producto' );
				
				$producto->setDays( $item->id, $diasGarantia );
			}
		}


	}

	/**
	* Change state close to warranty order when all products have state 'Reparado'
	*/
	public function changeStateClose(){
		
		$garantiaModel = $this->getModel( 'garantia' );

		$garantias = $garantiaModel->getGarantias();


		foreach ($garantias as $key => $garantia) {

			$productos = $this->getProductByIdGarantia( $garantia->id );

			$productoModel = $this->getModel( 'producto' );
			$reparados = $productoModel->getReparados( $garantia->id );

			$total = count( $productos );
			$totalReparados = count( $reparados );

			if ( $total == $totalReparados ) {
				
				$garantiaModel->changeReparado( $garantia->id );
			}

		}

	}

	/**
	* Change state close to warranty order and of each product when products have state 'Generado' 
	*/
	public function changeStateMore(){
		
		$garantiaModel = $this->getModel( 'garantia' );

		$garantias = $garantiaModel->getGarantias();


		foreach ($garantias as $key => $garantia) {

			$productos = $this->getProductByIdGarantia( $garantia->id );

			$productoModel = $this->getModel( 'producto' );
			$gestionados = $productoModel->getGestionados( $garantia->id );

			$total = count( $productos );
			$totalGestionados = count( $gestionados );


			if ( $total == $totalGestionados ) {
				

				foreach ($productos as $key => $producto) {
					
					$now = time(); // or your date as well
					$your_date = strtotime($garantia->fecha_radicacion);
					$datediff = $now - $your_date;
					$numberDays = floor($datediff/(60*60*24));

					if ($numberDays > 2) {

						$garantiaModel->changeEfectuado( $producto->id );
						$garantiaModel->changeReparado( $garantia->id );

					}

				}
			}

		}

	}



	/**
	* Exporta el layout exportacsv de la vista gestion a archivo de hoja de calculo .csv
	*/
	public function exportacsv(){

		$model =& $this->getModel( 'garantia' ); 
		$view =& $this->getView( 'garantia' , 'html');

		header('Content-type: application/vnd.ms-excel; name="excel"');
		header("Content-Disposition: attachment; filename=servicios_".time().".xls");
		header("Pragma: no-cache");
		header("Expires: 0");

		$view->setModel( $model, true );
		$view->setLayout( 'exportacsv' );

		$view->display();

		return;
	}

	

	
}
?>