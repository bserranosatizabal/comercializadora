
<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );

// Begining of the controller
class SolicitudControllersolicitud extends JController{
	
	public function saveSolicitud(){

		$response = ( object )array();


		// AJAX with Data
		$data = JRequest::getVar('data');

		// model of servicio
		$servicio = $this->getModel( 'servicio' );

		// get user logged
		$user = JFactory::getUser();
		$idUser = $user->get('id');

		$dataUser = ( object )array();

		$dataUser->mailUser = $user->get('email');
		$dataUser->nameUser = $user->get('name');

		$date = date( 'Y-m-d H:i:s' );

		$lastRadicado = $servicio->getMaxObject();

		$radicado = intval($lastRadicado[0]);

		$last = 0;

		if( $radicado == 0 ){
			$last = 1;
		}else{
			$last = $radicado + 1;
		}

		// args to model
		$args = array(
				'placa' => $data['placa']
			,	'fecha_visita' => $data['fecha_visita']
			,	'kilometraje' => $data['kilometraje']
			,	'fecha_instalacion' => $data['fecha_instalacion']
			,	'sistema' => $data['sistema']
			,	'descripcion_fallo' => $data['descripcion_fallo']
			,	'no_radicado' => $last
			,	'fecha_radicado' => $date
			,	'estado' => '1'
			,	'id_user' => $idUser
		);

		//instance model with args
		$servicio->instance( $args );

		if( ! $servicio->save('bool') ){
			$response->status = 500;
        	$response->message = "Su solicitud no ha podido ser guardada";
		} 

		$this->sendMailUser( $dataUser, $last, $data );
		$this->sendMailRec( $dataUser, $last, $data );

		$response->radicado = $last;
		$response->status = 200;
        $response->message = "Su solicitud ha sido guardada correctamente.";

        echo json_encode( $response );
        die;
	}

	public function sendMailUser( $dataUser, $id, $data ){

		$curDate = Misc::spanishDate( false );
		$fecha_visita = Misc::formatDateSpanish( $data['fecha_visita'] );
		$fecha_instalacion = Misc::formatDateSpanish( $data['fecha_instalacion'] );

		
		$mail = JFactory::getMailer();

        $contenido = "<div><img src='".JURI::root()."images/header-pdf.jpg'><br><br><br><br>";
        $contenido .= "Usted ha creado una nueva solicitud de servicio con los siguientes datos:<br>";

        $contenido .= "No radicado: 0000". $id ."<br>";
        $contenido .= "Fecha y hora de la radicación del servicio:". $curDate ."<br>";
        $contenido .= "Placa del vehiculo:". $data['placa'] ."<br>";
        $contenido .= "Fecha y hora de visita:". $fecha_visita ."<br>";
        $contenido .= "Kilometraje:". $data['kilometraje'] ."<br>";
        $contenido .= "Fecha de instalación:". $fecha_instalacion ."<br>";
        $contenido .= "Sistema instalado:". $data['sistema'] ."<br>";
        $contenido .= "Fecha y hora de visita:". $data['descripcion_fallo'] ."<br>";

        $contenido .= "<br>Muchas gracias por utilizar nuestros servicios.<br>";
        $contenido .= "<img src='".JURI::root()."images/pie-pdf.jpg'></div>";

        $mail->setSender( 'info@comercializadoragm.com' );
        $mail->addRecipient( $dataUser->mailUser );
        $mail->setSubject( "Usted ha creado una nueva solicitud de servicio." );
        $mail->Encoding = 'base64';
        $mail->isHtml( true );
        $mail->setBody( $contenido );

    	//enviar email
       	if ( ! $mail->Send()) {
       		$response->status = 500;
       	}
	}

	public function sendMailRec( $dataUser, $last, $data ){

		$solicitud = $this->getModel( 'servicio' );
		$users = $solicitud->getUserGroups();

		foreach ($users as $key => $user) {
			
			$curDate = Misc::spanishDate( false );
			$fecha_visita = Misc::formatDateSpanish( $data['fecha_visita'] );
			$fecha_instalacion = Misc::formatDateSpanish( $data['fecha_instalacion'] );

			$mail = JFactory::getMailer();

	        $contenido = "<div><img src='".JURI::root()."images/header-pdf.jpg'><br><br><br><br>";
	        $contenido .= "Han creado una solicitud de servicio con los siguientes datos:<br>";

	        $contenido .= "No radicado: 0000". $id ."<br>";
	        $contenido .= "Fecha y hora de la radicación del servicio:". $curDate ."<br>";
	        $contenido .= "Nombre solicitante:". $dataUser->nameUser ."<br>";
	        $contenido .= "Correo solicitante:". $dataUser->mailUser ."<br>";
	        $contenido .= "Placa del vehículo:". $data['placa'] ."<br>";
	        $contenido .= "Fecha y hora de visita:". $fecha_visita ."<br>";
	        $contenido .= "Kilometraje:". $data['kilometraje'] ."<br>";
	        $contenido .= "Fecha de instalación:". $fecha_instalacion ."<br>";
	        $contenido .= "Sistema instalado:". $data['sistema'] ."<br>";
	        $contenido .= "Fecha y hora de visita:". $data['descripcion_fallo'] ."<br>";

	        $contenido .= "<br>Muchas gracias por utilizar nuestros servicios.<br>";
	        $contenido .= "<img src='".JURI::root()."images/pie-pdf.jpg'></div>";

	        $mail->setSender( 'info@comercializadoragm.com' );
	        $mail->addRecipient( $user->email );
	        $mail->setSubject( "Usted ha creado una nueva solicitud de servicio." );
	        $mail->Encoding = 'base64';
	        $mail->isHtml( true );
	        $mail->setBody( $contenido );

	    	//enviar email
	       	if ( ! $mail->Send()) {
	       		$response->status = 500;
	       	}

		}

		

	}

	public function getServicioById(){

		$id = JRequest::getVar( 'id' );

		$model = $this->getModel( 'servicio' );

		$servicio = $model->getServicioById( $id );
		$estados = $model->getEstados();
		$tecnico = $model->getUsers();

		$view = $this->getView( 'servicio', 'html' );

		$view->assignRef( 'servicio', $servicio );
		$view->assignRef( 'estados', $estados );
		$view->assignRef( 'tecnico', $tecnico );

		$view->setLayout( 'detalleservicio' );

		$view->display();
	}

	public function saveGestion(){
		
		$data = JRequest::getVar( 'data' );

		$servicio  = $this->getModel( 'servicio' );

		$servicio->instance( $data );

		if ( ! $servicio->save('bool') ) {
			$response->status = 500;
    		$response->message = "Su solicitud ha sido guardada correctamente.";
		}	

		$response->status = 200;
        $response->message = "Gestión guardada correctamente.";

        echo json_encode( $response );
        die;
	}

	public function generatePdfServicio(){
		
		//get html of certification
		$datos = JRequest::getVar('data');

		// get code of certification
		$codigo = JRequest::getVar('codigo');

		$data = ( object ) array();
		$response = ( object )array();

		$data->header->certificado_number = $codigo;
		$data->content = $datos;

		$certificado = certGen::getCertificado( $data );

		$response->path = 'solicitudes/' . $data->header->certificado_number . '.pdf';

		$response->status = 200;

		echo json_encode( $response );
		die;

	}

	public function rangeDate(){

		$response = ( object )array();

		$data = JRequest::getVar( 'data' );

		$servicio = $this->getModel( 'servicio' );

		$servicios = $servicio->rangeDate( $data['initialDate'], $data['finalDate'] );

		$response->status = 200;

		echo json_encode( $response );
		die;

	}


	/**
	 * Get a list of filter options for the blocked state of a user.
	 *
	 * @return  array  An array of JHtmlOption elements.
	 */
	public function getNamesUser(){

		$Utilities = 'Misc';
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );
		$dataName = JRequest::getVar( 'data' );
		$response = array();

		$query->select( '*' );
		$query->from( $db->quoteName( 'jos_users' ) );
		$query->where( $db->quoteName( 'identificacion' ) .' LIKE '. $db->quote('%'. $db->escape( $dataName ) .'%') );
		$query->group( 'identificacion' );

		$db->setQuery( $query );
		$results = $db->loadAssocList();

		foreach ( $results as $key => $value) {
			
			$response[] = $value[ 'identificacion' ];
		}

		if( count($response) <= 0 )$response[] = '';

		if( $Utilities::isAjax() ){

			echo json_encode( $response );
			die;
		}

		return $response;
	}

	/**
	* Exporta el layout exportacsv de la vista gestion a archivo de hoja de calculo .csv
	*/
	public function exportacsv(){

		$model =& $this->getModel( 'servicio' ); 
		$view =& $this->getView( 'servicio' , 'html');

		header('Content-type: application/vnd.ms-excel; name="excel"');
		header("Content-Disposition: attachment; filename=servicios_".time().".xls");
		header("Pragma: no-cache");
		header("Expires: 0");

		$view->setModel( $model, true );
		$view->setLayout( 'exportacsv' );

		$view->display();

		return;
	}

	

	
}
?>