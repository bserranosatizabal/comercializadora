<?php
/**
* This class generates the certifies dinamically
*
*/
class certGen {

	public static function getCertificado( $data ){

		if( ! is_object( $data ) )
			return false;

		

		$dompdf = new DOMPDF();

		$font = Font_Metrics::get_font("helvetica", "normal");

		$html = self::getHeader( $data->header );
		$html .= self::getContent( $data->content );
		$html .= self::getFooter();	

		$dompdf->set_paper('letter');

		$dompdf->load_html( $html );
		$dompdf->render();


		$_output = $dompdf->output();

		file_put_contents("solicitudes/" . $data->header->certificado_number . ".pdf", $_output );


	}

	/**
	* Returns the pdf header
	* 
	* @param { string } the type of document
	* @return { string } the html compiled
	*/
	protected static function getHeader( $data ){

		$html = '<html>
		<head>
			<title>Title of the document</title>
			<meta http-equiv="content-type" content="text/html; charset=utf-8">'
			. self::getStyles( $type ) . 
		'</head>

		<body>
			<div class="document-wrapper">

				<div class="header">

	 				<div class="float-left">
	 					<img src="images/header-pdf.png"/>
	 				</div>

	 			</div>

				<div class="subheader">

					<strong><p class="center">Certificado No. 0000' . $data->certificado_number . '</p></strong>

				</div>';

		return $html;

	}

	protected static function getContent( $data ){

		$curDate = Misc::spanishDate( false );

		$html = '<div class="content">
					<p>
						Solicitud generada el '.$curDate.'
					</p><br>	
					<p>
						Datos de la solicitud generada:<br><br>
						<strong>Placa: </strong>'. $data['placa'].'<br>
						<strong>Fecha visita: </strong>'. $data['fecha_visita'].'<br>
						<strong>Kilometraje: </strong>'. Misc::numberDots($data['kilometraje']).'<br>
						<strong>Fecha instalación: </strong>'. $data['fecha_instalacion'].'<br>
						<strong>Sistema: </strong>'. $data['sistema'].'<br>
						<strong>Descripción fallo: </strong>'. $data['descripcion_fallo'].'<br>
					</p>

				</div>';

		return $html;
	}

	protected static function getFooter( $type ){
		$html = '<div class="footer">

			<div class="float-left">
				<img src="images/pie-pdf.png"/>
			</div>

		</div>';

		return $html;
	}

	protected static function getStyles( $type ){

		$html .= '
		<style type="text/css">

			body {
				background-color: #FFF;
				text-align: center;
				font-size: 14px;
			}

			@page document-wrapper {
				size: letter portrait; 
				margin: 1cm;
				background-color: #FFF;
				padding: 40px 0;
			}

			.subheader{
				margin-top: 200px;
			}

			.content {
				text-align: left;
				height: 510px;
			}

			.content p { margin: 25px 0; text-align: justify; }

			.center { text-align: center; }
 			.justify { text-align: justify; }
 			.align-right { text-align: right; }
 			.bold { font-weight: bold; }
 			.float-left { display:inline-block; text-align:left; width:50%; }
 			.float-right { display:inline-block; width:50%; text-align:right; }
 			.uppercase { text-transform: uppercase; }

			ul{
				list-style: none;
				list-style-type: none;
				padding: 0;
			}

			.subheader { margin-bottom: 0px; }
			.content { padding: 0 35px; margin-bottom: 30px; }

		</style>';

		return $html;
	}
}
?>