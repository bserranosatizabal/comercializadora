<?php
/**
* This class generates the certifies dinamically
*
*/
class garantiaGen {

	public static function getCertificado( $data ){

		if( ! is_object( $data ) )
			return false;
		
		// copia cliente

		$dompdf = new DOMPDF();

		$font = Font_Metrics::get_font("helvetica", "normal");

		$html = self::getHeader( $data->header );
		$html .= self::getContentCliente( $data->garantia, $data->productos );
		$html .= self::getFooter();	

		$dompdf->set_paper('letter');

		$dompdf->load_html( $html );
		$dompdf->render();


		$_output = $dompdf->output();

		file_put_contents("garantias/" . $data->header->certificado_number . "-cliente.pdf", $_output );

		// copia normal

		$dompdfnormal = new DOMPDF();

		$font = Font_Metrics::get_font("helvetica", "normal");

		$html = self::getHeader( $data->header );
		$html .= self::getContent( $data->garantia, $data->productos );
		$html .= self::getFooter();	

		$dompdfnormal->set_paper('letter');

		$dompdfnormal->load_html( $html );
		$dompdfnormal->render();


		$_outputnormal = $dompdfnormal->output();

		file_put_contents("garantias/" . $data->header->certificado_number . ".pdf", $_outputnormal );


	}

	/**
	* Returns the pdf header
	* 
	* @param { string } the type of document
	* @return { string } the html compiled
	*/
	protected static function getHeader( $data ){

		$html = '<html>
		<head>
			<title>Title of the document</title>
			<meta http-equiv="content-type" content="text/html; charset=utf-8">'
			. self::getStyles( $type ) . 
		'</head>

		<body>
			<div class="document-wrapper">

				<div class="header">

	 				<div class="float-left">
	 					<img src="images/header-pdf.png"/>
	 				</div>

	 			</div>

				<div class="subheader">

					<strong><p class="center">Certificado No. 0000' . $data->certificado_number . '</p></strong>

				</div>';

		return $html;

	}

	protected static function getContentCliente( $data, $productos ){

		$curDate = Misc::spanishDate( false );

		$html = '<div class="content">
					<p>
						Solicitud de garantía generada el '.$curDate.'
					</p><br>	
					<p>
						Datos de la solicitud generada:<br><br>
						<strong>Placa: </strong>'. $data['placa'].'<br>
						<strong>Kilometraje: </strong>'. Misc::numberDots($data['kilometraje']).'<br>
						<strong>Fecha instalación: </strong>'. $data['fecha_instalacion'].'<br>
						<strong>Sistema: </strong>'. $data['sistema'].'<br>
					</p>
					<br>
					<table>
						<tr>
							<td>Id. Producto</td>
							<td>Nombre producto</td>
							<td>Serial</td>
							<td>Descripción Fallo</td>
						</tr>';
					foreach ($productos as $key => $item) {
						
						$html .= '<tr>
							<td>'.$item['id_producto'].'</td>
							<td>'.$item['nombre_producto'].'</td>
							<td>'.$item['serial'].'</td>
							<td>'.$item['descripcion_fallo'].'</td>
						</tr>';
							
					}

		$html .='</table>

		<p style="text-align:center; margin-top: 40px;">--- Copia Cliente ---</p></div>';

		return $html;
	}

	protected static function getContent( $data, $productos ){

		$curDate = Misc::spanishDate( false );

		$html = '<div class="content">
					<p>
						Solicitud de garantía generada el '.$curDate.'
					</p><br>	
					<p>
						Datos de la solicitud generada:<br><br>
						<strong>Placa: </strong>'. $data['placa'].'<br>
						<strong>Kilometraje: </strong>'. Misc::numberDots($data['kilometraje']).'<br>
						<strong>Fecha instalación: </strong>'. $data['fecha_instalacion'].'<br>
						<strong>Sistema: </strong>'. $data['sistema'].'<br>
					</p>
					<br>
					<table>
						<tr>
							<td>Id. Producto</td>
							<td>Nombre producto</td>
							<td>Serial</td>
							<td>Descripción Fallo</td>
						</tr>';
					foreach ($productos as $key => $item) {
						
						$html .= '<tr>
							<td>'.$item['id_producto'].'</td>
							<td>'.$item['nombre_producto'].'</td>
							<td>'.$item['serial'].'</td>
							<td>'.$item['descripcion_fallo'].'</td>
						</tr>';
							
					}

		$html .='</table><p style="text-align:center; margin-top: 40px;">--- Copia Comercializadora G&M ---</p></div>';

		return $html;
	}

	protected static function getFooter( $type ){
		$html = '<div class="footer">

			<div class="float-left">
				<img src="images/pie-pdf.png"/>
			</div>

		</div>';

		return $html;
	}

	protected static function getStyles( $type ){

		$html .= '
		<style type="text/css">

			body {
				background-color: #FFF;
				text-align: center;
				font-size: 14px;
			}

			@page document-wrapper {
				size: letter portrait; 
				margin: 1cm;
				background-color: #FFF;
				padding: 40px 0;
			}

			.subheader{
				margin-top: 100px;
			}

			.content {
				text-align: left;
				height: 510px;
			}

			.content table{
				width: 100%;
				text-align: center;
				border-spacing: 0px;
			}

			.content table tr:first-child{
				color: #FFF;
				background-color: #1075A5;
			}

			.content table tr, .content table tr td{
				border: 1px solid #000;
			}

			.content p { margin: 25px 0; text-align: justify; }

			.center { text-align: center; }
 			.justify { text-align: justify; }
 			.align-right { text-align: right; }
 			.bold { font-weight: bold; }
 			.float-left { display:inline-block; text-align:left; width:50%; }
 			.float-right { display:inline-block; width:50%; text-align:right; }
 			.uppercase { text-transform: uppercase; }

			ul{
				list-style: none;
				list-style-type: none;
				padding: 0;
			}

			.subheader { margin-bottom: 0px; }
			.content { padding: 0 35px; margin-bottom: 140px; }

		</style>';

		return $html;
	}
}
?>