<?php
/**
 * @author          Empresa : Sainet Ingenieria Ltda. | Desarrollador : Robinson Perdomo
 * @link            http://www.creandopaginasweb.com
 * @copyright       Copyright © 2014 Sainet Ingenieria Ltda. All Rights Reserved
 */

// No direct access
defined('_JEXEC') or die;

/**
 * LogTrazabilidad helper.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_logtrazabilidad
 * @since		1.6
 */

jimport( 'joomla.environment.request' );

class GarantiaHelper
{
	
	/**
	 * Get a list of the user groups for filtering.
	 *
	 * @return  array  An array of JHtmlOption elements.
	 *
	 * @since   1.6
	 */
	public static function getEstados()
	{
		$db = JFactory::getDbo();

		$db->setQuery(
			"SELECT a.id AS value, a.estado AS text" .
			" FROM jos_estados_garantia AS a WHERE a.id = 5 OR a.id = 6 GROUP BY a.estado ORDER BY a.estado ASC"
		);
		
		$options = $db->loadObjectList();

		// Check for a database error.
		if ($db->getErrorNum())
		{
			JError::raiseNotice(500, $db->getErrorMsg());
			return null;
		}

		foreach ($options as &$option)
		{
			$option->text = ucfirst($option->text);
		}

		return $options;
	}

	

	/**
	 * Get a list of the user groups for filtering.
	 *
	 * @return  array  An array of JHtmlOption elements.
	 *
	 * @since   1.6
	 */
	public static function getTipos()
	{
		$db = JFactory::getDbo();

		$db->setQuery(
			"SELECT a.id AS value, a.tipo AS text" .
			" FROM jos_tipo_producto AS a GROUP BY a.tipo ORDER BY a.tipo ASC"
		);
		
		$options = $db->loadObjectList();

		// Check for a database error.
		if ($db->getErrorNum())
		{
			JError::raiseNotice(500, $db->getErrorMsg());
			return null;
		}

		foreach ($options as &$option)
		{
			$option->text = ucfirst($option->text);
		}

		return $options;
	}

	/**
	 * Get a list of the user groups for filtering.
	 *
	 * @return  array  An array of JHtmlOption elements.
	 *
	 * @since   1.6
	 */
	public static function getEstadosProductos()
	{
		$db = JFactory::getDbo();

		$db->setQuery(
			"SELECT a.id AS value, a.estado AS text" .
			" FROM jos_estados_garantia AS a WHERE a.id != 5 AND a.id != 6"
		);
		
		$options = $db->loadObjectList();

		// Check for a database error.
		if ($db->getErrorNum())
		{
			JError::raiseNotice(500, $db->getErrorMsg());
			return null;
		}

		foreach ($options as &$option)
		{
			$option->text = ucfirst($option->text);
		}

		return $options;
	}

	/**
	 * Get a list of the user groups for filtering.
	 *
	 * @return  array  An array of JHtmlOption elements.
	 *
	 * @since   1.6
	 */
	public static function getEstadosProductosCiudad()
	{
		$db = JFactory::getDbo();

		$db->setQuery(
			"SELECT a.id AS value, a.estado AS text" .
			" FROM jos_estados_garantia AS a WHERE a.id = 12 OR a.id = 9  GROUP BY a.estado ORDER BY a.estado "
		);
		
		$options = $db->loadObjectList();

		// Check for a database error.
		if ($db->getErrorNum())
		{
			JError::raiseNotice(500, $db->getErrorMsg());
			return null;
		}

		foreach ($options as &$option)
		{
			$option->text = ucfirst($option->text);
		}

		return $options;
	}

	public function getProductos(){

		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );

		$query->select( 'a.virtuemart_product_id AS value, b.*, c.product_name AS text' );
		$query->from( '#__virtuemart_products AS a' );
		$query->innerJoin( '#__virtuemart_product_categories AS b ON a.virtuemart_product_id = b.virtuemart_product_id' );
		$query->where( 'b.virtuemart_category_id = 29' );
		$query->innerJoin( '#__virtuemart_products_es_es AS c ON c.virtuemart_product_id = c.virtuemart_product_id' );
		$query->where( 'c.virtuemart_product_id = a.virtuemart_product_id' );
		$query->order( 'c.product_name ASC' );
		
		$db->setQuery( $query );

		$products = $db->loadObjectList();

		if ($db->getErrorNum())
		{
			JError::raiseNotice(500, $db->getErrorMsg());
			return null;
		}

		foreach ($products as &$option)
		{
			$option->text = ucfirst($option->text);
		}

		return $products;
	}

	/**
	 * Get a list of the user groups for filtering.
	 *
	 * @return  array  An array of JHtmlOption elements.
	 *
	 * @since   1.6
	 */
	public static function getUsers()
	{
		$db = JFactory::getDbo();

		$db->setQuery(
			"SELECT a.id AS value, a.name AS text, b.*" .
			" FROM jos_users AS a ".
			" INNER JOIN jos_user_usergroup_map AS b ON a.id = b.user_id".
			" WHERE b.group_id = 16".
			" GROUP BY a.name ORDER BY a.name ASC"
		);

		$users = $db->loadObjectList();

		// Check for a database error.
		if ($db->getErrorNum())
		{
			JError::raiseNotice(500, $db->getErrorMsg());
			return null;
		}

		foreach ($users as &$option)
		{
			$option->text = ucfirst($option->text);
		}

		return $users;
	}


}
?>