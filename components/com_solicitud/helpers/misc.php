<?php

/**
* This classs contains all resources of php
* Note: add the new functions at the end of the class, and write the explanation in API function
*
*/
class Misc {
	
	/**
	* Global attributes here
	*
	*/
	
	/**
	* llave para enciptación
	* @type string
	* @access private
	*/
	private static $Key = "key";


	/**
	* Convert date into spanish format.
	*
	* @param { date } object date with format, if date is not passed the function returns now
	* @param { string } string date converted
	*
	*/
	public static function spanishDate( $echo = true ){

		$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
		$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
		
		if( $echo ){
			echo $dias[ date( 'w' ) ] . ", " . date( 'd' ) . " de " . $meses[ date( 'n' ) - 1 ]. " de ". date( 'Y' );
		}else{
			
			return $dias[ date( 'w' ) ] . ", " . date( 'd' ) . " de " . $meses[ date( 'n' ) - 1 ]. " de ". date( 'Y' );
		}
			

	}

	public static function formatDateSpanish( $FechaStamp = NULL ){

		//2014-08-12 45:00:00
		// if( ! preg_match('/^\d{4}\-\d{2}\-\d{2}\s\d{2}\:\d{2}:\d{2}$/', $FechaStamp) )
		// 	return false;

		$FechaStamp = strtotime( $FechaStamp );

	    $ano = date('Y',$FechaStamp);
		$mes = date('n',$FechaStamp);
		$dia = date('d',$FechaStamp);
		$diasemana = date('w',$FechaStamp);

		$diassemanaN = array( "Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado" ); 
		$mesesN=array(1=>"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio", "Agosto","Septiembre","Octubre","Noviembre","Diciembre");

		return $diassemanaN[$diasemana].", $dia de ". $mesesN[$mes] ." de $ano";
	}

	/**
	* Remove spanish accents in string
	*
	* @param { string } the string to be cleared
	* @return { string } the string without accents
	*
	*/
	public static function removeAccents( $string ){

		$no_permitidas= array( "á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
		$permitidas= array( "a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E" );
		

		$texto = str_replace( $no_permitidas, $permitidas ,$string );

		return $texto;
	}

	/**
	* Sets dots in numbers
	*
	* @param { numeric } the number to be changed
	* @return { string } the number with dots
	*
	*/
	public static function numberDots( $n ) {
        // first strip any formatting;
        $n = ( 0 + str_replace( "," ,"", $n ) );
        
        // is this a number?
        if( ! is_numeric( $n ) ) return false;
        
        // now filter it;
        if( $n > 1000000000000 ) return round( ( $n / 1000000000000 ) , 1 ) . ' trillones';
        else if( $n > 1000000000 ) return round( ( $n / 1000000000 ) , 1 ).' billones';
        
        return number_format( $n );
    }
	
	/**
	* @params { string } string a encriptar
	* @return { string } cadena
	*/
	public static function Encrypt( $input = '' ) {
        $output = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5(self::$Key), $input, MCRYPT_MODE_CBC, md5(md5(self::$Key))));
        return $output;
    }
 	
	/**
	* @params { string } string a desencriptar, con el que metodo contrario en que
	* fue encriptado
	* @return { string } cadena
	*/
    public static function Decrypt( $input = '' ) {
        $output = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5(self::$Key), base64_decode($input), MCRYPT_MODE_CBC, md5(md5(self::$Key))), "\0");
        return $output;
    }
	
	/**
	* obtener los numeros de la semana, con el año y el mes
	* @param { numeric } mes
	* @param { numeric } año
	* @return { array } array de las semanas
	*/
	public static function getWeeksMonth( $year , $month ){
		
		$beg = (int) date('W', strtotime("first day of $year-$month"));
		$end = (int) date('W', strtotime("last day of $year-$month"));
		return range($beg, $end);
	}
	
	/**
	* validar Email
	* @param { string }
	* return { bool | int }
	*/
	public static function validateEmail( $str = '' ){
		
		$expr = '/^([a-zA-Z0-9_\.\-])+@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/';
		return preg_match( $expr , (string) $str );
	}

	/**
	* validar Fecha y hora, formato Y-m-d H:i
	* @param { string }
	* return { bool | int }
	*/
	public static function validateDateTime( $str = '' ){
		
		$patternDate = '/\d{1,2}\-\d{1,2}\-\d{2,4}(\s)\d{1,2}\:\d{1,2}/';
		return preg_match( $patternDate , (string) $str );
	}
	
	/**
	* Given an array of required fields, this function
	* checks whether the second argument have them
	*/
    public static function validateEmptyFields( $required = array(), $objectData = array() ) {
		
		$errors = array();

		foreach( $required as $value ){
			
			if( empty( $objectData[ $value ] ) ){
				
				$errors[] = $value;
			}
			
		}
		
		return $errors;
	}
	
	/**
	* conocer si una peticion es de tipo ajax
	* @param {}
	* return { bool }
	*/	
	public static function isAjax(){
		
		if( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
		{
			return true;
		}
		
		return false;
	}


	/**
	* convetir un texto para un nombre de campo aceptable
	* @param { String }
	* return { bool }
	*/	
	public static function parseNomField( $str ){
		
		$patternLetter = array('/\á/i', '/\é/i', '/\í/i', '/\ó/i', '/\ú/i'); 
		$replaceLetter = array('a', 'e', 'i', 'o', 'u'); 

		$str = preg_replace($patternLetter, $replaceLetter, $str);
		$str = preg_replace('/\t+|\s\s+/i', ' ', $str);
		$str = preg_replace('/\s/i', '-', $str);
		$str = strtolower($str);

		return $str;
	}

	protected function API(){

		// To get the date in spanish format, just do:
		Misc::spanishDate();

		// To clear a string from spanish accents:
		Misc::removeAccents( 'acción' ); // it returns 'accion'

		// To set number dots
		Misc::numberDots( 25000 ); // returns 25.000
		
		//encriptar y desencriptar texto
		$text = 'texto a encriptar';
		$encryptext = Misc::Encrypt( $text );
		Misc::Decrypt($encryptext);

	}
}
?>