<?php
/**
 * @author          Empresa : Sainet Ingenieria Ltda. | Desarrollador : Robinson Perdomo
 * @link            http://www.creandopaginasweb.com
 * @copyright       Copyright © 2014 Sainet Ingenieria Ltda. All Rights Reserved
 */

// No direct access
defined('_JEXEC') or die;

/**
 * LogTrazabilidad helper.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_logtrazabilidad
 * @since		1.6
 */

jimport( 'joomla.environment.request' );

class ServicioHelper
{
	
	/**
	 * Get a list of the user groups for filtering.
	 *
	 * @return  array  An array of JHtmlOption elements.
	 *
	 * @since   1.6
	 */
	public static function getEstados()
	{
		$db = JFactory::getDbo();

		$db->setQuery(
			"SELECT a.id AS value, a.estado AS text" .
			" FROM jos_estados AS a GROUP BY a.estado ORDER BY a.estado ASC"
		);
		$options = $db->loadObjectList();

		// Check for a database error.
		if ($db->getErrorNum())
		{
			JError::raiseNotice(500, $db->getErrorMsg());
			return null;
		}

		foreach ($options as &$option)
		{
			$option->text = ucfirst($option->text);
		}

		return $options;
	}

	/**
	 * Get a list of the user groups for filtering.
	 *
	 * @return  array  An array of JHtmlOption elements.
	 *
	 * @since   1.6
	 */
	public static function getUsers()
	{
		$db = JFactory::getDbo();

		$db->setQuery(
			"SELECT a.id AS value, a.name AS text, b.*" .
			" FROM jos_users AS a ".
			" INNER JOIN jos_user_usergroup_map AS b ON a.id = b.user_id".
			" WHERE b.group_id = 16".
			" GROUP BY a.name ORDER BY a.name ASC"
		);

		$users = $db->loadObjectList();

		// Check for a database error.
		if ($db->getErrorNum())
		{
			JError::raiseNotice(500, $db->getErrorMsg());
			return null;
		}

		foreach ($users as &$option)
		{
			$option->text = ucfirst($option->text);
		}

		return $users;
	}
}
?>