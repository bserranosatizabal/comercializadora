<?php

/**
 * Model for "Servicio"
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.modellist' );

// Initializes the Class
class SolicitudModelServicio extends JModelList {
	
	/**
	 * Object Id
	 * @var int
	 */
	var $id;

	/**
	 * Object Id
	 * @var int
	 */
	var $id_servicio;

	/**
	 * String Placa
	 * @var string
	 */
	var $placa;

	/**
	 * Date fecha_visita
	 * @var string
	 */
	var $fecha_visita;

	/**
	 * String Kilometraje
	 * @var string
	 */
	var $kilometraje;

	/**
	 * date fecha_instalación
	 * @var string
	 */
	var $fecha_instalacion;

	/**
	 * String Sistema
	 * @var string
	 */
	var $sistema;

	/**
	 * String descripcion_fallo
	 * @var string
	 */
	var $descripcion_fallo;

	/**
	 * String no_radicado
	 * @var string
	 */
	var $no_radicado;

	/**
	 * String fecha_radicado
	 * @var string
	 */
	var $fecha_radicado;

	/**
	 * String estado
	 * @var string
	 */
	var $estado;

	/**
	 * int id_user
	 * @var int
	 */
	var $id_user;

	/**
	 * int id_tecnico
	 * @var int
	 */
	var $id_tecnico;

	/**
	 * String fecha_servicio
	 * @var string
	 */
	var $fecha_servicio;

	/**
	 * string numero_asistencia
	 * @var string
	 */
	var $numero_asistencia;

	/**
	 * String acciones_realizadas
	 * @var string
	 */
	var $acciones_realizadas;

	protected $counts = 0;

	protected $count = 0;

	/**
	 * Constant for table
	 * @var string
	 */
	const TABLE = '#__servicio';

	/**
	 * Constant for filters states
	 * @var string
	 */
	const FILTER_STATE = 'servicio.state.';

	
	/**
	 * Attributes Map
	 * @var array
	 */
	var $attrs_map = array(
			'id'
		,	'id_servicio'
		,	'placa'
		,	'fecha_visita'
		,	'kilometraje'
		,	'fecha_instalacion'
		,	'sistema'
		,	'descripcion_fallo'
		,	'no_radicado'
		,	'fecha_radicado'
		,	'estado'
		,	'id_user'
		,	'id_tecnico'
		,	'fecha_servicio'
		,	'numero_asistencia'
		,	'acciones_realizadas'
	);

	/**
	 * Methods
	 * 
	 */
	

	/**
	 * Constructor
	 * 
	 * @param { array || int } the args to instance the model or the single id
	 * 
	 */

	public function __construct()
	{

	    if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'fecha_radicado',
				'no_radicado',
				'placa',
				'estado'
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication();

		// Adjust the context to support modal layouts.
		if ($layout = JRequest::getVar('layout', 'gestionservicio'))
		{
			$this->context .= '.'.$layout;
		}

		// $limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'), 'uint');
		// $this->setState('list.limit', $limit);


		// $limitstart = JRequest::getUInt('limitstart', 0);
		// $this->setState('list.start', $limitstart);

		$search = $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$search = $this->getUserStateFromRequest($this->context.'.filter.inicio', 'inicio');
		$this->setState('filter.inicio', $search);


		$search = $this->getUserStateFromRequest($this->context.'.filter.final', 'final');
		$this->setState('filter.final', $search);

		$search = $this->getUserStateFromRequest($this->context.'.filter.identificacion', 'identificacion');
		$this->setState('filter.identificacion', $search);

		$search = $this->getUserStateFromRequest($this->context.'.filter.estado', 'estado');
		$this->setState('filter.estado', $search);

		// Load the parameters.
		// $params = JComponentHelper::getParams('com_solicitud');

		// $this->setState('params', $params);

		// List state information.
		parent::populateState('no_radicado', 'ASC');

		
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return  string  A store id.
	 *
	 * @since   1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id	.= ':'.$this->getState('filter.inicio');
		$id	.= ':'.$this->getState('filter.final');
		$id	.= ':'.$this->getState('filter.identificacion');
		$id	.= ':'.$this->getState('filter.estado');



		return parent::getStoreId($id);
	}

	public function instance( $config = NULL ){
		
		if( is_numeric( $config ) )
			$config = array( $this->attrs_map[ 0 ] => $config );
		
		if( ! is_array( $config ) )
			return;
		
		// Get existing object if the id was passed through
		return $this->fill( $config );
	}
	
	/**
	 * Fill the model attributes with the passed arguments.
	 *
	 * @param { arr } Object arguments
	 */
	protected function fill( $args = NULL ){


		if ( ! is_array( $args ) )
			return false;
		
	
		// Get object in DB			
		if ( is_numeric( $args[ $this->attrs_map[ 0 ] ] ) ){

			$object = $this->getObject( $args[ $this->attrs_map[ 0 ] ] );
			
			if( is_object( $object ) ){
				foreach ( $this->attrs_map as $attr ) {
					
					if ( isset( $object->$attr ) )
						$this->$attr = $object->$attr;
				}
			}

		}

	
		// Merge attributes	when id is not passed through
		foreach ( $this->attrs_map as $attr ) {
			if ( isset( $args[ $attr ] ) )
				$this->$attr = $args[ $attr ];
		}

		// Set exists to true.
		$this->exists = true;
	
	}

	public function getServicios( $idUser, $initial, $final){


		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );

		$query->select( '*' );
		$query->from( self::TABLE );
		$query->where( 'id_user ='.  $idUser );
		$query->where( 'fecha_radicado BETWEEN ' . $db->quote($initial) .' AND '. $db->quote($final) );

		$db->setQuery( $query );
		

		
		return $db->loadObjectList();


	}
	
	
	/**
	 * Get a single Object
	 * 
	 * @param { int } the id or attributes of the Object.
	 * @return { bool/object } the object returned or false otherwise
	 */
	protected function getObject( $id = 1 ){
		
		if( ! is_numeric( $id ) )
			return false;
		
		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );

		$query->select( '*' );
		$query->from( self::TABLE );
		$query->where( 'id = ' . $id );
		$db->setQuery( $query );
		
		return $db->loadObject();
		
	}


	/**
	 * Get a single Object
	 * 
	 * @param { int } the id or attributes of the Object.
	 * @return { bool/object } the object returned or false otherwise
	 */
	public function getMaxObject( ){
		
		
		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );

		$query->select( 'MAX( no_radicado )' );
		$query->from( self::TABLE );
		$db->setQuery( $query );

		
		return $db->loadColumn();
		
	}

	/**
	 * Get a user for groups
	 * 
	 * @param { int } the id or attributes of the Object.
	 * @return { bool/object } the object returned or false otherwise
	 */
	public function getUserGroups( ){
		
		
		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );

		$query->select( 'a.*, b.*' );
		$query->from( '#__users AS a' );
		$query->innerJoin( '#__user_usergroup_map AS b ON a.id = b.user_id' );
		$query->where( 'b.group_id = 11 OR b.group_id = 10' );
		$db->setQuery( $query );
		
		return $db->loadObjectList();
		
	}

	/**
	 * Get a user for groups
	 * 
	 * @param { int } the id or attributes of the Object.
	 * @return { bool/object } the object returned or false otherwise
	 */
	public function getServicioById( $id ){
		
		
		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );

		$query->select( 'a.*' );
		$query->from( '#__servicio AS a' );
		$query->where( 'a.id ='. $id );
		$db->setQuery( $query );
		
		return $db->loadObject();
		
	}
	
	/**
	 * Get Objects collection
	 *
	 * @param { int } the id or attributes of the Object.
	 * @return { bool/object } the object returned or false otherwise
	 **/
	public function getObjects( $wheres = NULL , $prlimits = array() , $params ){

		$start = JRequest::getVar( 'start' );
		
		$this->setState('list.start', $start);
		
		if( !isset( $prlimits[ 'limitstart' ] ) )
			$prlimits[ 'limitstart' ] = $this->getState('list.start');
			
		if( !isset( $prlimits[ 'limit' ] ) )	
			$prlimits[ 'limit' ] = $this->getState('list.limit');
		
		$result = array();
		
		if( ! is_array( $wheres ) )
			$wheres = array();
			
		if( ! is_array( $params ) )	
			$params = array();

		$user = JFactory::getUser();

		$idUser = $user->get('id');

		$wheres = array(

			0 => (object)array(
				'key' => 'id_user',
				'value' => $idUser,
				'condition' => '='
			)
		);

		
		$query = $this->buildQuery( $wheres , $params );

		$this->data = $this->_getList( $query , $this->getState('list.start') , $this->getState('list.limit') );
		$this->count = $this->_getListCount($query);

		
		foreach ( $this->data as $obj ){

			$args = array();
			
			foreach ( $obj as $key => $attr ) {
				
				$args[ $key ] = $obj->$key;
			}
			
			$estaClase = get_class( $this );
	
			$object = new $estaClase();
			$object->fill( $args );
			array_push( $result, $object );
			
		}
	
		return $result;
	
	}

	/**
	 * Get Objects collection
	 *
	 * @param { int } the id or attributes of the Object.
	 * @return { bool/object } the object returned or false otherwise
	 **/
	public function getGestion( $wheres = NULL , $prlimits = array() , $params ){

		$start = JRequest::getVar( 'start' );
		
		$this->setState('list.start', $start);

		

		if( ! isset( $prlimits[ 'limitstart' ] ) )
			$prlimits[ 'limitstart' ] = $this->getState('list.start');
			
		if( ! isset( $prlimits[ 'limit' ] ) )	
			$prlimits[ 'limit' ] = $this->getState('list.limit');
		
		$result = array();
		
		if( ! is_array( $wheres ) )
			$wheres = array();
			
		if( ! is_array( $params ) )	
			$params = array();

		$gestion = true;
		
		$query = $this->buildQuery( $wheres , $params, $gestion );

		$this->data = $this->_getList( $query , $this->getState('list.start') , $this->getState('list.limit') );
		$this->counts = $this->_getListCount($query);
		
		foreach ( $this->data as $obj ){

			$args = array();
			
			foreach ( $obj as $key => $attr ) {
				
				$args[ $key ] = $obj->$key;
			}
			
			$estaClase = get_class( $this );
	
			$object = new $estaClase();
			$object->fill( $args );
			array_push( $result, $object );
			
		}

		// var_dump($result);

	
		return $result;
	
	}

	public function rangeDate( $initial, $final ){


		// Instance databse
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );

		$query->select( '*'  );
		$query->from( '#__servicio' );
		
		
		$db->setQuery( $query );
		
		return $db->loadObjectList();
	}

	/**
	 * Save a new object or update an exist.
	 *
	 * @param { string } the type of the response
	 * ( string returns the error string or "" )
	 * ( bool return false or true )
	 * ( object returns the response with error_message, status true or false )
	 * @return { string  } the query string calling the insert query
	 *
	 */
	public function save( $return = 'string' ){

		// Initialize
		$db = JFactory::getDbo();
		$response = ( object ) array();
		$model = ( object ) array();

		if( ! is_string( $return ) )
			$return = 'string';

		// Fetch the model attributes with the $model's var
		foreach ( $this->attrs_map as $attribute ) {
			$model->$attribute = $this->$attribute;
		}

		// If id exists, update the model
		// If id doesn't exist, insert a new row in database
		if( $model->id == NULL || $model->id == "" ){

			if (! $db->insertObject( self::TABLE, $model ) ) {

				if( $return == 'string' ){
					return "No se pudo guardar el object. " . $db->stderr();
				}

				if( $return == 'bool' ){
					return false;
				}

				if( $return == 'object' ){
					$response->status = false;
					$response->error = "No se pudo guardar el object. " . $db->stderr();
					return $response;
				}
			}

			if( $return == 'string' ){
				return "";
			}

			if( $return == 'bool' ){
				return true;
			}

			if( $return == 'object' ){
				$response->status = true;
				$response->error = "";
				return $response;
			}
		}

		// Update
		if ( ! $db->updateObject( self::TABLE, $model, 'id', false ) ) {
			
			if( $return == 'string' ){
				return "No se pudo actualizar el object. " . $db->stderr();
			}

			if( $return == 'bool' ){
				return false;
			}

			if( $return == 'object' ){
				$response->status = false;
				$response->error = "No se pudo guardar el object. " . $db->stderr();
				return $response;
			}
		}

		if( $return == 'string' ){
			return "";
		}

		if( $return == 'bool' ){
			return true;
		}

		if( $return == 'object' ){
			$response->status = true;
			$response->error = "";
			return $response;
		}

	}
	
	/**
	 * Delete object from database
	 *
	 * @param { array || int } the id of the object or array that contains the id
	 * @return { bool/object } the object returned or false otherwise
	 */
	public function delete(){
	
		if( ! is_numeric( $this->id ) )
			return false;
	
	
		// Delete existing object if the id was passed through

		$query = "DELETE FROM ". self::TABLE ." WHERE id = $this->id";
		$db = JFactory::getDbo();
		$db->setQuery( $query );
	
		return $db->query();
	
	}

	/**
	 * Publish or unpublish the object
	 * @param { string } the type of the response
	 * ( string returns the error string or "" )
	 * ( bool return false or true )
	 * ( object returns the response with error_message, status true or false )
	 * @return { string  } the state after change
	 *
	 */
	public function publish( $return = 'string' ){

		$db = JFactory::getDbo();
		$this->estado = ($this->estado == 1 ) ? 0 : 1;

		// Update
		$std = new stdClass();
		$std->id = $this->id;
		$std->estado = $this->estado;

		if (! $db->updateObject( self::TABLE, $std, 'id', false ) ) {
			
			if( $return == 'string' ){
				return "No se pudo actualizar el object. " . $db->stderr();
			}

			if( $return == 'bool' ){
				return false;
			}

			if( $return == 'object' ){
				$response->status = false;
				$response->error = "No se pudo actualizar el object. " . $db->stderr();
				return $response;
			}
		}

		if( $return == 'string' ){
			return "";
		}

		if( $return == 'bool' ){
			return true;
		}

		if( $return == 'object' ){
			$response->status = true;
			$response->error = "";
			return $response;
		}

	}

	/**
	 * Get a list of the user groups for filtering.
	 *
	 * @return  array  An array of JHtmlOption elements.
	 *
	 * @since   1.6
	 */
	public static function getEstados()
	{
		$db = JFactory::getDbo();

		$db->setQuery(
			"SELECT * FROM jos_estados AS a GROUP BY a.estado ORDER BY a.estado ASC"
		);
		return $db->loadObjectList();

		// Check for a database error.
		if ($db->getErrorNum())
		{
			JError::raiseNotice(500, $db->getErrorMsg());
			return null;
		}

		foreach ($options as &$option)
		{
			$option->text = ucfirst($option->text);
		}

		return $options;
	}

	/**
	 * Get a list of the user groups for filtering.
	 *
	 * @return  array  An array of JHtmlOption elements.
	 *
	 * @since   1.6
	 */
	public static function getUsers()
	{
		$db = JFactory::getDbo();

		$db->setQuery(
			"SELECT a.id AS value, a.name AS text, b.*" .
			" FROM jos_users AS a ".
			" INNER JOIN jos_user_usergroup_map AS b ON a.id = b.user_id".
			" WHERE b.group_id = 16".
			" GROUP BY a.name ORDER BY a.name ASC"
		);

		$users = $db->loadObjectList();

		// Check for a database error.
		if ($db->getErrorNum())
		{
			JError::raiseNotice(500, $db->getErrorMsg());
			return null;
		}

		foreach ($users as &$option)
		{
			$option->text = ucfirst($option->text);
		}

		return $users;
	}
	
	// Helpers for the same Model
	
	
	/**
	 * Build a query for collection. Filters query are included.
	 *
	 * @param { array } wheres clausule. Clausule must be { key: 'value', value: 'value', condition:'=', glue: 'AND || OR' }
	 * @return { string } the query string calling the collection
	 *
	 */
	protected function buildQuery( $wheres = NULL, $params = array(), $gestion){
		
		//Validation
		if( empty( $wheres ) )
			$wheres = array();

		// Get a storage key.
		$store = $this->getStoreId();

		// Initialize
		$db = JFactory::getDbo();
		$query  = $db->getQuery(true);
		
		// Query base
		
		$query->select( "a.*, a.id AS id_servicio" );	
		$query->from( '#__servicio as a');
		
		
		// Wheres appending
		foreach ( $wheres as $key => $clausule ) {
			
			if( ! is_object( $clausule ) )
				continue;

			$query->where( $db->quoteName($clausule->key) . $clausule->condition . $db->quote($clausule->value) . ' '.trim($clausule->glue).' ' );

		}

		
		//Filters appending

		//query order
		if( isset($params[ 'order' ]) ){
			$query->order( $params[ 'order' ] );
		}
		else{
			// Add the list ordering clause.
			$query->order($db->escape($this->getState('list.ordering', 'no_radicado')).' '.$db->escape($this->getState('list.direction', 'ASC')));
		}

		//query group	
		if( isset($params['group']) )
			$query->group( $params[ 'group' ] );

		//filter for range date

		$inicio = $this->getState('filter.inicio');
		$final = $this->getState('filter.final');


		if( empty($inicio) == false &&  empty($final) == false)
			$query->where( 'a.fecha_radicado BETWEEN ' . $db->quote($inicio) .' AND '. $db->quote($final) );
		

		if( empty($inicio) == false &&  empty($final) == true)
			$query->where( 'a.fecha_radicado BETWEEN ' . $db->quote($inicio) .' AND NOW()' );


		// filter to razon social
		$identificacion = $this->getState('filter.identificacion');

		if( ! empty($identificacion) ){
			$query->select( "b.*" );
			$query->innerJoin( '#__users AS b ON b.id = a.id_user');
			$query->where( ' b.identificacion = '. $db->quote($identificacion) );
		}

		// filter to estado
		$estado = $this->getState('filter.estado');

		if( ! empty($estado) ){
			$query->select( "c.*" );
			$query->innerJoin( '#__estados AS c ON c.id = a.estado');
			$query->where( ' a.estado = '. $db->quote($estado) );
		}

		//var_dump($query->__toString());

		return $query;
	}

	/**
	* generar Paginador
	*/
	public function getPaginations(){
		// Load the content if it doesn't already exist
		
        jimport('joomla.html.pagination');
        $_pagination = new JPagination( $this->count , $this->getState('list.start'), $this->getState('list.limit') );

        return $_pagination;
	}

	/**
	* generar Paginador
	*/
	public function getPagination(){
		// Load the content if it doesn't already exist
		
        jimport('joomla.html.pagination');
        $_pagination = new JPagination( $this->counts , $this->getState('list.start'), $this->getState('list.limit') );

        return $_pagination;
	}

	

	/**
	 * API of the class
	 * 
	 * @return { void }
	 */
	protected function API(){
		
		// Instance an object with defaults
		$Object = new ComponentModelObject();
		
		// Instance an object with args not ID
		$args = array(
				'attribue'	=> 'any'
		);
		
		$Object = new ComponentModelObject();
		$Object->instance( $args );
		$Object->save(); // saves a new item
		
		//---------------
		// Instance an object with args with the ID
		// Result will be the model from DB merge the fields passed
		$args = array(
					'id' 		=> 1
				,	'tema' 		=> 1
				,	'titulo'		=> 'example'
				,	'ruta' 		=> 'pdf_test.pdf'
				,	'ano'		=> '2013'
				,	'state'		=>	1 // state pusblished
		);
		
		$Object = new ComponentModelObject();
		$Object->instance( $args );
		$Object->save(); // It will update the object with the id
		$Object->delete(); // It will delete the object with the id

		// Instance an object with args with the ID		
		$Object = new ComponentModelObject( 1 );

		// To get the objects with the table
		$Object = new ComponentModelObject();
		$object->getObjects();


		// To get the objects with conditions
		$wheres = array(
			0 => ( object ) array(
					'key' => 'id'
				,	'value' => '3'
				,	'condition' => '='
				,	'glue' => 'AND'
			)
		);

		$Object = new ComponentModelObject();
		$Object->getObjects( $wheres );
		
		
	}
}
?>