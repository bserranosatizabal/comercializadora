<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
require_once( JPATH_COMPONENT . DS . 'helpers/misc.php' );
require_once( JPATH_COMPONENT . DS . 'helpers/dompdf-master/dompdf_config.inc.php' );
require_once( JPATH_COMPONENT . DS . 'controller.php' );
require_once( JPATH_COMPONENT . DS . 'helpers/cert-generator.php' );
require_once( JPATH_COMPONENT . DS . 'helpers/garantia-generator.php' );
require_once( JPATH_COMPONENT . DS . 'helpers/servicio.php' );
require_once( JPATH_COMPONENT . DS . 'helpers/garantia.php' );
require_once( JPATH_COMPONENT . DS . 'solicitud.php' );

$controller = JControllerLegacy::getInstance('Solicitud');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
?>