<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();
$document = JFactory::getDocument();

$document->addStyleSheet($host.'less/load-styles.php?load=solicitud');
jimport('joomla.application.module.helper');
$module = JModuleHelper::getModule('mod_vm_login');

if ($this->group != '11' && $this->group != '16' ) {
	$app->redirect('index.php');	
}

?>

<div class="close-session">
	<?php echo JModuleHelper::renderModule($module); ?>
</div>


<?php echo $this->renderMenu(); ?>
<div class="content-solicitud gestion">
	
	<div class="solicitud">
		<h2>Productos</h2>
		<a href="javascript:history.back()" class="close-button"><i>x</i></a>
		<form id="gestion-garantia-form">
			<div class="garantia-detalle bottom">
				<table>
					<thead>
						<tr>
							<th width="1%">
								<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
							</th>
							<th>Producto</th>
							<th>Días en garantía</th>
							<th>Serial</th>
							<th>Estado</th>
							<th>Detalle</th>
						</tr>
					</thead>
					<tbody>

					<?php foreach ($this->productOnly as $key => $producto) {
					?>
						<tr>
							<td><?php echo JHtml::_('grid.id', $key, $producto->id) ?></td>
							<td><?php echo $producto->product_name ?></td>
							<td><?php echo $producto->dias_garantia ?></td>
							<td><?php echo $producto->serial ?></td>
							<td><?php echo $producto->estado ?></td>
							<td><a href="index.php/solicitud?task=garantia.getProductoById&id=<?= $producto->id ?>">Ver</a></td>

						</tr>
					<?php
					} ?>
					</tbody>

				</table>
			</div>	
		</form>
		
	</div>
		

</div>
