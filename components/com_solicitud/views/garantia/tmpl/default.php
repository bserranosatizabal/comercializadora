<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();
$document = JFactory::getDocument();

$document->addStyleSheet($host.'less/load-styles.php?load=solicitud');
jimport('joomla.application.module.helper');
$module = JModuleHelper::getModule('mod_vm_login');

if ( $this->group != '17' ) {
	$session = JFactory::getSession(); // starting the session
	$session->destroy(); //This will destroy the joomla session
	$app->redirect('index.php');	
}

?>

<div class="close-session">
	<?php echo JModuleHelper::renderModule($module); ?>
</div>


<?php echo $this->renderMenu(); ?>
<div class="content-solicitud">
	
	<div class="solicitud">
		<h2>Solicitud de garantía</h2>
		<form id="garantia-form">
			<div class="garantia-solicitud">
				<div class="top">
					<ul>
						<li><input type="text" name="placa" placeholder="Placa de vehículo"></li>
						<li><input type="text" name="sistema" placeholder="Sistema instalado"></li>
						<li><input type="text" name="kilometraje" placeholder="Kilometraje en gas"></li>
						<li><label>Fecha de instalación</label><input readonly type="text" name="fecha_instalacion" id="instalacion" class="fecha" placeholder="AAAA/MM/DD"><i class="calendar" data-calendar="#instalacion"></i></li>
					</ul>
					
				</div>
		</form>
				<div class="action">
					<a href="#" id="producto"><i class="fa fa-plus-circle"></i> Agregar producto</a>
				</div>
				<div class="medium">
					<form id="producto-form">
						<ul>
							<li>
								<select name="id_producto" id="productos">
									<option value="">Producto:</option>
									<?php echo JHtml::_('select.options', GarantiaHelper::getProductos(), 'value', 'text');?>
								
								</select>
							</li>
							<li>
								<input type="text" name="serial" placeholder="Serial">
								<div class="tooltip serial">
									<h3>Este es el serial del producto</h3>
								</div>

							</li>
							<li>
								<textarea name="descripcion_fallo" placeholder="Descripción del fallo"></textarea>
								<div class="tooltip descripcion">
									<h3>Este es la descripción del fallo del producto</h3>
								</div>
							</li>
						</ul>
						<input type="button" value="Agregar" class="button-app" id="add-product"/>
					</form> 
				</div>
				<div class="bottom">
					<div class="header">
						<table>
							<tr>
								<td></td>
								<td>Producto</td>
								<td>Serial</td>
								<td>Fallo</td>
							</tr>
						</table>
					</div>
					<div class="content content-products">
					</div>	
				</div>
				<div class="accion">
					<input type="submit" value="Enviar" class="button-app"/>
				</div>
			</div>	
		
		
		</div>
		<div class="em-over-screen"></div>
	    <div class="em-modal-box">
            <div class="header">
                <i class="close-modal-button">x</i>
            </div>
            <div class="body">
                Hello modal box
            </div>
        </div>

</div>
