<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();
$document = JFactory::getDocument();

$document->addStyleSheet($host.'less/load-styles.php?load=solicitud');
jimport('joomla.application.module.helper');
$module = JModuleHelper::getModule('mod_vm_login');

if ( $this->group != '17' ) {
	$session = JFactory::getSession(); // starting the session
	$session->destroy(); //This will destroy the joomla session
	$app->redirect('index.php');	
}

?>

<div class="close-session">
	<?php echo JModuleHelper::renderModule($module); ?>
</div>


<?php echo $this->renderMenu(); ?>
<div class="content-solicitud">
	
	<div class="solicitud gestion">
		<h2>Mis solicitudes de garantía</h2>
		<a href="index.php/garantia?layout=garantiasolicitudes" class="close-button">><i>x</i></a>
		<form id="detalle-garantia-form">
			<div class="garantia-solicitudes">
				<div class="top">
					<ul>
						<li><label>Placa de vehículo</label><span><?php echo $this->garantia->placa ?></span></li>
						<li><label>Sistema instalado</label><span><?php echo $this->garantia->sistema ?></span></li>
						<li><label>Kilometraje en gas</label><span><?php echo $this->garantia->kilometraje ?></span></li>
						<li><label>Fecha de Instalación</label><span><?php echo $this->garantia->fecha_instalacion ?></span></li>
					</ul>
					
				</div>
				<div class="bottom"> 
					<div class="header">
						<table>
							<tr>
								<td>Producto</td>
								<td>Serial</td>
								<td>Fallo</td>
								<td>Estado</td>
							</tr>
						</table>
					</div>
					<div class="content">
						<table>
							<?php foreach ($this->products as $key => $producto) {
							?>
								<tr>
									<td><?php echo $producto->product_name ?></td>
									<td><?php echo $producto->serial ?></td>
									<td><a href="#" class="vermas" data-id="<?php echo $producto->id ?>" data-description="<?php echo $producto->descripcion_fallo ?>">Ver más</a></td>
									<?php if( $producto->estado == 'Diagnostico' ){
									?>
									<td><a href="#" class="button-diagnostico" data-id="<?php echo $producto->id ?>" data-diagnostico="<?php echo $producto->diagnostico ?>" data-replica="<?php echo $producto->replica ?>"><?php echo $producto->estado ?></a></td>
									<?php	
									}else{ ?>

									<td><?php echo $producto->estado ?></td>

									<?php } ?>
								</tr>
							<?php
							} ?>
							
						</table>
					</div>	
				</div>
			</div>	
		</form>
		
		</div>

</div>
