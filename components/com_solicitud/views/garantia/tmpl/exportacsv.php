<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	com_solicitud
 */

// No direct access.
defined('_JEXEC') or die;
jimport( 'joomla.environment.uri' );

$user = JFactory::getUser();
$loggeduser = JFactory::getUser();

?>
<table class="adminlist">
	<thead>
		<tr>
			<th colspan="9">Ordenes de Garantía</th>
		</tr>
		<tr>
			<th class="nowrap">No. Garantía</th>
			<th class="nowrap">Placa</th>
			<th class="nowrap">Kilometraje</th>
			<th class="nowrap">Sistema</th>
			<th class="nowrap">Fecha instalación</th>
			<th class="nowrap">Fecha de Radicado</th>
			<th class="nowrap">No. Radicado</th>
			<th class="nowrap">Estado</th>
			<th class="nowrap">Usuario</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($this->gestion as $i => $item) :
	?>
		<tr class="row<?php echo $i % 2; ?>">
			<td class="center"><?php echo $item->id_garantia?></td>
			<td class="center"><?php echo $item->placa?></td>
			<td class="center"><?php echo $item->kilometraje?></td>
			<td class="center"><?php echo $item->sistema?></td>
			<td class="center"><?php echo $item->fecha_instalacion?></td>
			<td class="center"><?php echo $item->fecha_radicacion?></td>
			<td class="center"><?php echo $item->no_radicado?></td>
			<td class="center"><?php echo ( $item->estado == 6 ) ? 'Cerrado': 'Abierto'; ?></td>
			<td class="center"><?php echo $item->id_user?></td>
		</tr>
	<?php 
		
	endforeach; 
	?>
	</tbody>
</table>

<table class="adminlist">
	<thead>
		<tr>
			<th colspan="12">Productos por garantía</th>
		</tr>
		<tr>
			<th class="nowrap">Id</th>
			<th class="nowrap">No. Garantía</th>
			<th class="nowrap">Producto</th>
			<th class="nowrap">Serial</th>
			<th class="nowrap">Descripción fallo</th>
			<th class="nowrap">Estado</th>
			<th class="nowrap">Diagnostico</th>
			<th class="nowrap">Observaciones</th>
			<th class="nowrap">Tipo Producto</th>
			<th class="nowrap">Replica</th>
			<th class="nowrap">Días en garantía</th>
			<th class="nowrap">Fecha recepción</th>
		</tr>
	</thead>
	<tbody>

	<?php 

	foreach ($this->gestion as $key => $gestion) {
		
	
		foreach ($this->productos as $i => $producto) {

			if ($producto->id_garantia == $gestion->id_garantia) {
			
		?>
				<tr class="row<?php echo $i % 2; ?>">
					<td class="center"><?php echo $producto->id?></td>
					<td class="center"><?php echo $producto->id_garantia?></td>
					<td class="center"><?php echo $producto->product_name?></td>
					<td class="center"><?php echo $producto->serial?></td>
					<td class="center"><?php echo $producto->descripcion_fallo?></td>
					<td class="center"><?php echo $producto->estado?></td>
					<td class="center"><?php echo $producto->diagnostico?></td>
					<td class="center"><?php echo $producto->observaciones?></td>
					<td class="center"><?php echo $producto->tipo?></td>
					<td class="center"><?php echo $producto->replica?></td>
					<td class="center"><?php echo $producto->dias_garantia?></td>
					<td class="center"><?php echo $producto->fecha_recepcion?></td>

				</tr>
		<?php 
			}
		}
	}
	?>
	</tbody>
</table>