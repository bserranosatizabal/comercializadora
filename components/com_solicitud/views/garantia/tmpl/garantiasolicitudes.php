<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();
$document = JFactory::getDocument();
$app = JFactory::getApplication();

$document->addStyleSheet($host.'less/load-styles.php?load=solicitud');
jimport('joomla.application.module.helper');
$module = JModuleHelper::getModule('mod_vm_login');

if ( $this->group != '17' ) {
	$session = JFactory::getSession(); // starting the session
	$session->destroy(); //This will destroy the joomla session
	$app->redirect('index.php');	
}

// Load the tooltip behavior.
JHtml::_('behavior.multiselect');
JHtml::_('behavior.framework');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.modal');

$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));

$document->addScriptDeclaration( 
	'var inicio = "' . $this->state->get( 'filter.id_user' ) . '";'

	 );
?>

<div class="close-session">
	<?php echo JModuleHelper::renderModule($module); ?>
</div>

<?php echo $this->renderMenu(); ?>
<div class="content-solicitud">
	<div class="solicitud">
		<h2>Mis solicitudes de garantía</h2>
		<form action="<?php echo JRoute::_('?layout=garantiasolicitudes');?>" method="post" name="adminForm" id="adminForm">
			<div class="form-mis-garantias">
				<div class="top">
					<ul>
						<li><label>Fecha de radicado</label><input readonly type="text" name="inicio" value="<?php echo $this->state->get( 'filter.inicio' );?>" id="inicio" class="datepicker-input" placeholder="AAAA/MM/DD" ><i class="calendar" data-calendar="#inicio"></i></li>
						<li><input readonly type="text" name="final" value="<?php echo $this->state->get( 'filter.final' );?>" id="final" class="datepicker-input" placeholder="AAAA/MM/DD" ><i class="calendar" data-calendar="#final"></i></li>
						<li>
							<select name="estado" id="estado">
								<option value="">Estado:</option>
								<?php echo JHtml::_('select.options', GarantiaHelper::getEstados(), 'value', 'text', $this->state->get('filter.estado'));?>
							</select>
						</li>
					</ul>
					<button type="button" class="limpiar-filtros" ><?php echo JText::_('Limpiar'); ?></button>
					
				</div>
				<div class="bottom"> 
					<div class="header">
						<table>
							<tr>
								<td><span><?php echo JHtml::_('grid.sort', 'No de radicado', 'no_radicado', $listDirn, $listOrder); ?></span></td>
								<td><span><?php echo JHtml::_('grid.sort', 'Fecha de radicación', 'fecha_radicacion', $listDirn, $listOrder); ?></span></td>
								<td><span><?php echo JHtml::_('grid.sort', 'Días en garantía', 'dias_garantia', $listDirn, $listOrder); ?></span></td>
								<td>Detalle</td>
							</tr>
						</table>
					</div>
					<div class="content">

						<?php if (count( $this->garantias) <= 0) {
						?>
						<table>
						<tr><td><h3>No hay resultados</h3></td></tr></table>	
						<?php
						}else{ ?>
						<table>
							<?php foreach ($this->garantias as $key => $garantias) {
							?>
							<tr>
								<td><?php echo $garantias->no_radicado ?></td>
								<td><?php echo $garantias->fecha_radicacion ?></td>
								<td><?php echo $garantias->dias_garantia ?></td>
								<td><a href="index.php/solicitud?task=garantia.getProductByIdUser&id=<?php echo $garantias->id ?>">Ver más</a></td>
							</tr>
							<?php
							} ?>
							
						</table>
						<?php } ?>
						 <div class="wrapper-paginator">
							<?php echo $this->paginations->getListFooter(); ?>
						</div>
					</div>	
				</div>
			</div>
			<div>
				<input type="hidden" name="task" value="" />
				<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
				<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
				<input type="hidden" name="boxchecked" value="0" />
				<?php echo JHtml::_('form.token'); ?>
			</div>	
		</form>
		
		</div>
		

       

</div>
