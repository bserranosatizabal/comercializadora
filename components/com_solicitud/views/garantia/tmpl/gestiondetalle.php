<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();
$document = JFactory::getDocument();
$app = JFactory::getApplication();

$document->addStyleSheet($host.'less/load-styles.php?load=solicitud');
jimport('joomla.application.module.helper');
$module = JModuleHelper::getModule('mod_vm_login');

if ( $this->group != '10' && $this->group != '15' ) {
	$session = JFactory::getSession(); // starting the session
	$session->destroy(); //This will destroy the joomla session
	$app->redirect('index.php');	
}

?>

<div class="close-session">
	<?php echo JModuleHelper::renderModule($module); ?>
</div>


<?php echo $this->renderMenu(); ?>
<div class="content-solicitud gestion">
	
	<div class="solicitud">
		<h2>Solicitudes de garantía</h2>
		<a href="index.php/garantia?layout=gestiongarantia" class="close-button"><i>x</i></a>
		<form id="gestion-garantia-form">
			<div class="garantia-detalle">
				<div class="top">
					<ul>
						<li><label>Placa de vehículo</label><span><?php echo $this->garantia->placa ?></span></li>
						<li><label>Sistema instalado</label><span><?php echo $this->garantia->sistema ?></span></li>
						<li><label>Kilometraje en gas</label><span><?php echo $this->garantia->kilometraje ?></span></li>
						<li><label>Fecha de Instalación</label><span><?php echo $this->garantia->fecha_instalacion ?></span></li>
					</ul>
					
				</div>
				<div class="bottom"> 
					
						<table>
							<thead>
								<tr>
									<th width="1%">
										<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
									</th>
									<th>Producto</th>
									<th>Días en garantía</th>
									<th>Serial</th>
									<th>Estado</th>
									<th>Detalle</th>
								</tr>
							</thead>
							<tbody>

							<?php foreach ($this->productOnly as $key => $producto) {
							?>
								<tr>
									<td><?php echo JHtml::_('grid.id', $key, $producto->id) ?></td>
									<td><?php echo $producto->product_name ?></td>
									<td><?php echo $producto->dias_garantia ?></td>
									<td><?php echo $producto->serial ?></td>
									<td><?php echo $producto->estado ?></td>
									<td><a href="index.php/solicitud?task=garantia.getProductoById&id=<?= $producto->id ?>">Ver</a></td>

								</tr>
							<?php
							} ?>
							</tbody>

						</table>

						<ul>
							<li><label>Cambiar los productos seleccionados a estado</label></li>
							<li>
								<?php if( $this->group != '15' ){ ?>
									<select name="estado" id="estado">
										<option value="">Estado:</option>
										<?php echo JHtml::_('select.options', GarantiaHelper::getEstadosProductos(), 'value', 'text');?>
									</select>
								<?php }else{ ?>
									<select name="estado" id="estado">
										<option value="">Estado:</option>
										<?php echo JHtml::_('select.options', GarantiaHelper::getEstadosProductosCiudad(), 'value', 'text');?>
									</select>
								<?php } ?>
							</li>
						</ul>

						<input type="submit" value="Guardar" class="button-normal" />

					</div>	
				</div>
			</div>	
		</form>
		
	</div>

</div>
