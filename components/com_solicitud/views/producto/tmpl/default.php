<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();
$document = JFactory::getDocument();
$app = JFactory::getApplication();

$document->addStyleSheet($host.'less/load-styles.php?load=solicitud');
jimport('joomla.application.module.helper');
$module = JModuleHelper::getModule('mod_vm_login');

if ($this->group != '11' && $this->group != '16' ) {
		$session = JFactory::getSession(); // starting the session
	$session->destroy(); //This will destroy the joomla session
	$app->redirect('index.php');
}

// Load the tooltip behavior.
JHtml::_('behavior.multiselect');
JHtml::_('behavior.framework');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.modal');


$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));

$document->addScriptDeclaration( 
	'var inicio = "' . $this->state->get( 'filter.id_user' ) . '";'
);

?>

<div class="close-session">
	<?php echo JModuleHelper::renderModule($module); ?>
</div>


<?php echo $this->renderMenu(); ?>

<div class="content-solicitud gestion">
	
	<div class="solicitud">
		<h2>Productos</h2>
		<form action="<?php echo JRoute::_('?layout=default');?>" method="post" name="adminForm" id="adminForm"  class="producto-form">
			<div class="producto-list">
				<div class="top">
					<ul class="filters">
						<li><label>Fecha de radicado</label><input readonly type="text" name="inicio" value="<?php echo $this->state->get( 'filter.inicio' );?>" id="inicio" class="datepicker-input" placeholder="AAAA/MM/DD" ><i class="calendar" data-calendar="#inicio"></i></li>
						<li><input type="text" readonly name="final" value="<?php echo $this->state->get( 'filter.final' );?>" id="final" class="datepicker-input" placeholder="AAAA/MM/DD" ><i class="calendar" data-calendar="#final"></i></li>
						<li><input type="text" name="identificacion" value="<?php echo $this->state->get( 'filter.identificacion' );?>" id="identificacion" placeholder="Nit o razón social" ></li>
						<li>
							<select name="estado" id="estado">
								<option value="">Estado:</option>
								<?php echo JHtml::_('select.options', GarantiaHelper::getEstadosProductos(), 'value', 'text', $this->state->get('filter.estado'));?>
							
							</select>
						</li>
						
					</ul>
					<button type="button" class="limpiar-filtros" ><?php echo JText::_('Limpiar'); ?></button>
					
				</div>
				<div class="bottom">
					<table>
						<thead>
							<tr>
								<th width="30px">
									No. de Orden
								</th>
								<th><span><?php echo JHtml::_('grid.sort', 'Producto', 'product_name', $listDirn, $listOrder); ?></span></th>
								<th><span><?php echo JHtml::_('grid.sort', 'Días en garantía', 'dias_garantia', $listDirn, $listOrder); ?></span></th>
								<th>Serial</th>
								<th>Estado</th>
								<th>Detalle</th>
							</tr>
						</thead>
						<tbody>
						<?php foreach ($this->productos as $key => $producto) {
						?>
							<tr>
								<td><?php echo $producto->id_garantia ?></td>
								<td><?php echo $producto->product_name ?></td>
								<td><?php echo $producto->dias_garantia ?></td>
								<td><?php echo $producto->serial ?></td>
								<td><?php echo $producto->estado ?></td>
								<td><a href="index.php/solicitud?task=garantia.getProductoById&id=<?= $producto->id ?>&curview=admin">Ver</a></td>

							</tr>
						<?php
						} ?>
						</tbody>

					</table>
				</div>
				
			</div>
			<div>
				<input type="hidden" name="task" value="" />
				<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
				<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
				<input type="hidden" name="boxchecked" value="0" />
				<?php echo JHtml::_('form.token'); ?>
			</div>	
		</form>
		<div class="wrapper-paginator">
			<?php echo $this->pagination->getListFooter(); ?>
		</div>
		
	</div>
		

</div>
