<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();
$document = JFactory::getDocument();
$app = JFactory::getApplication();

$document->addStyleSheet($host.'less/load-styles.php?load=solicitud');
jimport('joomla.application.module.helper');
$module = JModuleHelper::getModule('mod_vm_login');

if ( $this->group != '10' && $this->group != '15' && $this->group != '16' && $this->group != '11' ) {
	$session = JFactory::getSession(); // starting the session
	$session->destroy(); //This will destroy the joomla session
	$app->redirect('index.php');	
}

$readonly = '';

if( $this->group == '10' || $this->group == '15' || $this->group == '11' ){
	$readonly = 'readonly';
}

?>

<div class="close-session">
	<?php echo JModuleHelper::renderModule($module); ?>
</div>


<?php echo $this->renderMenu(); ?>
<div class="content-solicitud gestion">
	
	<div class="solicitud">
		<h2>Producto: <?php echo $this->producto->product_name ?></h2>
		<a href="javascript:history.back()" class="close-button"><i>x</i></a>
		<form id="gestion-producto-form">
			<?php 

				$curview = JRequest::getVar('curview');
			?>
			<input type="hidden" name="id" value="<?= $this->producto->id ?>">
			<input type="hidden" name="dias_garantia" value="<?= $this->producto->dias_garantia ?>">
			<input type="hidden" name="curview" value="<?= $curview ?>">
			<input type="hidden" name="id_garantia" value="<?= $this->producto->id_garantia ?>">
			<input type="hidden" name="id_producto" value="<?= $this->producto->id_producto ?>">

			<div class="top">
				<ul>
					<li><label>Placa de vehículo: </label><span><?php echo $this->garantia->placa ?></span></li>
					<li><label>Sistema instalado: </label><span><?php echo $this->garantia->sistema ?></span></li>
					<li><label>Kilometraje en gas: </label><span><?php echo $this->garantia->kilometraje ?></span></li>
					<li><label>Fecha de Instalación: </label><span><?php echo $this->garantia->fecha_instalacion ?></span></li>
				</ul>
			</div>
			<div class="producto-detalle">	
				<ul class="two-columns">
					<li><label>Producto:</label><input <?= $readonly ?> type="text" name="nombre_producto" value="<?= $this->producto->product_name ?>" /></li>
					<li><label>Serial:</label><input <?= $readonly ?> type="text" name="serial" value="<?= $this->producto->serial ?>"/></li>
					<li><label>Descripción fallo:</label><textarea <?= $readonly ?> name="descripcion_fallo"><?= $this->producto->descripcion_fallo ?></textarea></li>
					<li>
						<?php if( $this->group != '15' ){ ?>
							<select name="estado" id="estado">
								<option value="">Estado:</option>
								<?php foreach ($this->estados as $key => $estado) {

									$selected = '';

									if( $this->producto->estado == $estado->id ){
										$selected = 'selected';
									}
								?>
									<option value="<?= $estado->id; ?>" <?= $selected ?>><?= $estado->estado; ?></option>
								<?
								} ?>
							</select>
						<?php }else{ ?>
							<select name="estado" id="estado">
								<option value="">Estado:</option>
								<?php foreach ($this->ciudad as $key => $estado) {

									$selected = '';

									if( $this->producto->estado == $estado->id ){
										$selected = 'selected';
									}
								?>
									<option value="<?= $estado->id; ?>" <?= $selected ?>><?= $estado->estado; ?></option>
								<?
								} ?>
							</select>	
						<?php } ?>


						<?php foreach ($this->tipo as $key => $tipo) {

								$checked = '';

								if( $this->producto->id_tipo == $tipo->id ){
									$checked = 'checked';
								}
						?>	
								<input type="radio" name="id_tipo" value="<?= $tipo->id ?>" <?= $checked ?>><label><?= $tipo->tipo ?></label>
						<?php
						} ?>
					</li>
					
				</ul>

				
				<ul class="two-columns">
					<li><label>Diagnostico:</label><textarea <?= $readonly ?> name="diagnostico"><?= $this->producto->diagnostico ?></textarea></li>
					<?php if ( $this->group == '16' || $this->group =! '11') {
					?>
						 <li><label>Observaciones:</label><textarea readonly name="observaciones"><?= $this->producto->observaciones ?></textarea></li>

					<?php }else{?>
					 <li><label>Observaciones:</label><textarea name="observaciones"><?= $this->producto->observaciones ?></textarea></li>
					 <?php } ?>
					<?php if ( $this->group == '16' || $this->group == '11' ): ?>
						<li><label>Replica:</label><textarea readonly name="replica"><?= $this->producto->replica ?></textarea></li>
					<?php endif; ?>

					<li><input type="submit" value="Guardar" class="button-normal" /></li>
				</ul>
				
				
			</div>	
		</form>
		
	</div>

</div>
