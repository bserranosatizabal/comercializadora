<?php

/**
 * General View for "informes seguimiento" "lista" layout
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.view' );

class SolicitudViewproducto extends JView {

	public $group;
	protected $productos;
	protected $productOnly;
	protected $state;
	protected $pagination;
	protected $estados;
	protected $producto;
	protected $ciudad;
	protected $tipo;
	protected $garantia;

	// Function that initializes the view
	function display( $tpl = null ){


		$this->productos = $this->get('Gestion');
		$this->pagination = $this->get('Pagination');

		$this->state = $this->get('State');

		$user = JFactory::getUser();
		$user_type = $user->get('id');

		$groups = JUserHelper::getUserGroups($user_type);

		foreach ($groups as $key => $group) {

			$this->group = $group;

		}

		if( strtolower($this->getLayout()) == 'exportacsv' ){

			parent::display($tpl);
			die;
		}


		parent::display( $tpl );
		
	}
	
	public function renderMenu(){


		$html = '
				<div class="menu">
					<ul>
				';

		

		// menu usuario

		if ($this->group == '17' ) {
			
			$html .= '
					<li><a href="#">Ordenes de servicio</a>
						<ul>
							<li><a href="index.php/solicitud?layout=solicitud">Solicitar servicio</a></li>
							<li><a href="index.php/solicitud?layout=missolicitudes">Historial de solicitudes</a></li>
						</ul>
					</li>
					<li><a href="#">Ordenes de garantía</a>
						<ul>
							<li><a href="index.php/garantia">Solicitar garantia</a></li>
							<li><a href="index.php/garantia?layout=garantiasolicitudes">Historial de solicitudes</a></li>
						</ul>
					</li>
					<li><a href="#">Ordenes de producto</a>
						<ul>
							<li><a href="index.php/ordenes-de-producto">Registrar producto</a></li>
							<li><a href="#">Productos reportados</a></li>
						</ul>
					</li>
					<li><a href="#">Ordenes de compra</a>
						<ul>
							<li><a href="#">Crear orden de compra </a></li>
							<li><a href="#">Consultar ordenes</a></li>
						</ul>
					</li>';
			
		}

		// menu recepcionista

		if ( $this->group == '10' ) {

			$html .= '<li><a href="#">Ordenes de servicio</a>
						<ul>
						<li><a href="index.php/solicitud?layout=solicitud">Solicitar servicio</a></li>
								<li><a href="index.php/solicitud?layout=gestionservicio">Gestión ordenes de servicio</a></li>
							</ul>
						</li>
						<li><a href="#">Ordenes de garantía</a>
							<ul>
								<li><a href="index.php/garantia?layout=gestiongarantia">Gestión ordenes de garantia</a></li>
							</ul>
						</li>
						<li><a href="#">Ordenes de producto</a>
							<ul>
								<li><a href="#">Productos reportados</a></li>
							</ul>
						</li>';
		}

		// menu jefe tecnico

		if ( $this->group == '11' ) {

			$html .= '<li><a href="#">Ordenes de servicio</a>
						<ul>
						<li><a href="index.php/solicitud?layout=solicitud">Solicitar servicio</a></li>
								<li><a href="index.php/solicitud?layout=gestionservicio">Gestión ordenes de servicio</a></li>
							</ul>
						</li>
						<li><a href="#">Ordenes de garantía</a>
							<ul>
								<li><a href="index.php/producto">Administrar ordenes de garantia</a></li>
							</ul>
						</li>
						<li><a href="#">Ordenes de producto</a>
							<ul>
								<li><a href="#">Productos reportados</a></li>
							</ul>
						</li>';
		}

		// menu ciudad

		if ( $this->group == '15' ) {

			$html .= '
						<li><a href="#">Ordenes de garantía</a>
							<ul>
								<li><a href="index.php/garantia?layout=gestiongarantia">Gestión ordenes de garantia</a></li>
							</ul>
						</li>
						<li><a href="#">Ordenes de producto</a>
							<ul>
								<li><a href="#">Productos reportados</a></li>
							</ul>
						</li>';
		}

		// menu despacho

		if ( $this->group == '13' ) {

			$html .= '
						<li><a href="#">Ordenes de garantía</a>
							<ul>
								<li><a href="index.php/garantia?layout=gestiongarantia">Gestión ordenes de garantia</a></li>
							</ul>
						</li>
						<li><a href="#">Ordenes de compra</a>
							<ul>
								<li><a href="#">Consultar ordenes</a></li>
							</ul>
						</li>
						';
		}

		// menu despacho

		if ( $this->group == '12' || $this->group == '14') {

			$html .= '
						<li><a href="#">Ordenes de producto</a>
							<ul>
								<li><a href="#">Productos reportados</a></li>
							</ul>
						</li>
						<li><a href="#">Ordenes de compra</a>
							<ul>
								<li><a href="#">Consultar ordenes</a></li>
							</ul>
						</li>
						';
		}

		// menu tecnico

		if ( $this->group == '16' ) {

			$html .= '<li><a href="#">Ordenes de garantía</a>
							<ul>
								<li><a href="index.php/producto">Administrar ordenes de garantia</a></li>
							</ul>
						</li>
						<li><a href="#">Ordenes de producto</a>
							<ul>
								<li><a href="#">Productos reportados</a></li>
							</ul>
						</li>
						';
		}



		$html .= '</ul>
				</div>';		

		return $html;
	}		

}
?>