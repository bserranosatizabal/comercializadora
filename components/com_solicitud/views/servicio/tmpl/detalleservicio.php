<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();
$document = JFactory::getDocument();

$app = JFactory::getApplication();

$document->addStyleSheet($host.'less/load-styles.php?load=solicitud');

jimport('joomla.application.module.helper');
$module = JModuleHelper::getModule('mod_vm_login');

if (  $this->group != '10' && $this->group != '11') {
	$session = JFactory::getSession(); // starting the session
	$session->destroy(); //This will destroy the joomla session
	$app->redirect('index.php');	
}

if ( $this->group == '11' ){
	$readonly = 'readonly';
}

?>

<div class="close-session">
	<?php echo JModuleHelper::renderModule($module); ?>
</div>

<?php echo $this->renderMenu(); ?>


<div class="content-solicitud">
	<div class="solicitud gestion">
		<h2>Solicitud de servicio</h2>
		<a href="index.php/solicitud?layout=gestionservicio" class="close-button">><i>x</i></a>
		<form id="gestion-servicio-form">
			<div class="form-solicitud form-gestion">
				<div class="left">
					<ul>
						<li><input <?php echo $readonly ?> type="text" name="placa" placeholder="Placa de vehículo" value="<?php echo $this->servicio->placa ?>"></li>
						<li><label>Fecha y hora de visita</label><input type="text" name="fecha_visita" id="visita" class="fecha" placeholder="AAAA/MM/DD" value="<?php echo $this->servicio->fecha_visita ?>"><i class="calendar" data-calendar="#visita"></i></li>
						<li><input <?php echo $readonly ?> type="text" name="kilometraje" placeholder="Kilometraje en gas" value="<?php echo $this->servicio->kilometraje ?>"></li>
						<li><label>Fecha de instalación</label><input type="text" name="fecha_instalacion" id="instalacion" class="fecha" placeholder="AAAA/MM/DD" value="<?php echo $this->servicio->fecha_instalacion ?>"><i class="calendar" data-calendar="#instalacion"></i></li>
					</ul>
				</div>
				<div class="right"> 
					<ul>
						<li><input <?php echo $readonly ?> type="text" name="sistema" placeholder="Sistema instalado" value="<?php echo $this->servicio->sistema ?>"></li>
						<li><textarea <?php echo $readonly ?> name="descripcion_fallo" placeholder="Descripción fallo"><?php echo $this->servicio->descripcion_fallo ?></textarea></li>
					</ul>
				</div>
				<div class="left">
					<ul>
						<li>

							<select name="id_tecnico" id="id_tecnico">
								<option value="">Técnico:</option>
								<?php foreach ($this->tecnico as $key => $tecnico) {

									$selected = '';

									if( $this->servicio->id_tecnico == $tecnico->value ){
										$selected = 'selected';
									}
								?>
									<option value="<?php echo $tecnico->value ?>" <?php echo $selected ?> ><?php echo $tecnico->text; ?></option>
								<?php
								} ?>
							
							</select>
						</li>
						<li>
							<select name="estado" id="estado">
								<option value="">Estados:</option>
								<?php foreach ($this->estados as $key => $estado) {

									$selected = '';

									if( $this->servicio->estado == $estado->id ){
										$selected = 'selected';
									}
								?>
									<option value="<?php echo $estado->id ?>" <?php echo $selected ?>  ><?php echo $estado->estado; ?></option>
								<?php
								} ?>
							
							</select>
						</li>
						<li><textarea <?php echo $readonly ?> type="text" name="acciones_realizadas" placeholder="Acciones realizadas:"><?php echo $this->servicio->acciones_realizadas ?></textarea></li>
						
					</ul>
				</div>
				<div class="right"> 
					<ul>
						<li><label>Fecha de radicación</label><input <?php echo $readonly ?> type="text" name="fecha_radicado" id="radicado" class="fecha" placeholder="AAAA/MM/DD" value="<?php echo $this->servicio->fecha_radicado ?>"><i class="calendar" data-calendar="#radicacion"></i></li>
						<li><label>Fecha de servicio</label><input <?php echo $readonly ?> type="text" name="fecha_servicio" id="servicio" class="fecha" placeholder="AAAA/MM/DD" value="<?php echo $this->servicio->fecha_servicio ?>"><i class="calendar" data-calendar="#servicio"></i></li>
						<li><label>Numero de informe de asistencia</label></li>
						<li><input type="text" name="numero_asistencia" <?php echo $readonly ?> placeholder="Numero de informe de asistencia" value="<?php echo $this->servicio->numero_asistencia ?>"></li>
					</ul>
					<input type="hidden" name="id" value="<?php echo $this->servicio->id ?>">
					<input type="hidden" name="no_radicado" value="<?php echo $this->servicio->no_radicado ?>">
				</div>

				<?php if ( $this->group != '11' ) {
				?>
				<div class="accion">
					<input type="submit" value="Guardar" class="button-register"/>
				</div>
				<?php } ?>
			</div>	
		</form>
		
	</div>
		<div class="em-over-screen"></div>
	    <div class="em-modal-box">
            <div class="header">
                <i class="close-modal-button">x</i>
            </div>
            <div class="body">
                Hello modal box
            </div>
        </div>

</div>
