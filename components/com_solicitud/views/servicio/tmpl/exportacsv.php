<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	com_solicitud
 */

// No direct access.
defined('_JEXEC') or die;
jimport( 'joomla.environment.uri' );

$user = JFactory::getUser();
$loggeduser = JFactory::getUser();

?>
<table class="adminlist">
	<thead>
		<tr>
			<th class="nowrap">Id</th>
			<th class="nowrap">Placa</th>
			<th class="nowrap">Fecha visita</th>
			<th class="nowrap">Kilometraje</th>
			<th class="nowrap">Fecha instalación</th>
			<th class="nowrap">Sistema</th>
			<th class="nowrap">Descripción fallo</th>
			<th class="nowrap">No. Radicado</th>
			<th class="nowrap">Fecha de Radicado</th>
			<th class="nowrap">Estado</th>
			<th class="nowrap">Usuario</th>
			<th class="nowrap">Técnico</th>
			<th class="nowrap">Fecha servicio</th>
			<th class="nowrap">Número de asistencia</th>
			<th class="nowrap">Acciones realizadas</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($this->gestion as $i => $item) :
	?>
		<tr class="row<?php echo $i % 2; ?>">
			<td class="center"><?php echo $item->id_servicio?></td>
			<td class="center"><?php echo $item->placa?></td>
			<td class="center"><?php echo $item->fecha_visita?></td>
			<td class="center"><?php echo $item->kilometraje?></td>
			<td class="center"><?php echo $item->fecha_instalacion?></td>
			<td class="center"><?php echo $item->sistema?></td>
			<td class="center"><?php echo $item->descripcion_fallo?></td>
			<td class="center"><?php echo $item->no_radicado?></td>
			<td class="center"><?php echo $item->fecha_radicado?></td>
			<td class="center"><?php echo ( $item->estado == 2 ) ? 'Finalizado': 'Radicado'; ?></td>
			<td class="center"><?php echo $item->id_user?></td>
			<td class="center"><?php echo $item->id_tecnico?></td>
			<td class="center"><?php echo $item->fecha_servicio?></td>
			<td class="center"><?php echo $item->numero_asistencia?></td>
			<td class="center"><?php echo $item->acciones_realizadas?></td>

		</tr>
	<?php 
		
	endforeach; 
	?>
	</tbody>
</table>