<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();
$document = JFactory::getDocument();
$app = JFactory::getApplication();

$document->addStyleSheet($host.'less/load-styles.php?load=solicitud');
jimport('joomla.application.module.helper');
$module = JModuleHelper::getModule('mod_vm_login');

if ( $this->group != '17' ) {
	$session = JFactory::getSession(); // starting the session
	$session->destroy(); //This will destroy the joomla session
	$app->redirect('index.php');	
}

// Load the tooltip behavior.
//JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
//JHtml::_('behavior.modal');
JHtml::_('behavior.framework');

$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));

$document->addScriptDeclaration( 
	'var inicio = "' . $this->state->get( 'filter.id_user' ) . '";'

	 );
?>

<div class="close-session">
	<?php echo JModuleHelper::renderModule($module); ?>
</div>

<?php echo $this->renderMenu(); ?>
<div class="content-solicitud">
	<div class="solicitud">
		<h2>Mis solicitudes de servicio</h2>
		<form action="<?php echo JRoute::_('?layout=missolicitudes');?>" method="post" name="adminForm" id="adminForm">
			<div class="form-mis-solicitud">
				<div class="top">
					<ul>
						<li><label>Fecha de radicado inicial</label><input readonly type="text" name="inicio" value="<?php echo $this->state->get( 'filter.inicio' );?>" id="inicio" class="datepicker-input" placeholder="AAAA/MM/DD" ><i class="calendar" data-calendar="#inicio"></i></li>
						<li><label>Fecha de radicado final</label><input readonly type="text" name="final" value="<?php echo $this->state->get( 'filter.final' );?>" id="final" class="datepicker-input" placeholder="AAAA/MM/DD" ><i class="calendar" data-calendar="#final"></i></li>
						
						<!-- <button type="submit"><?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?></button> -->
						<button type="button" class="limpiar-filtros" ><?php echo JText::_('Limpiar'); ?></button>
					</ul>
					
				</div>
				<div class="bottom"> 
					<div class="header">
						<table>
							<tr>
								<td><span><?php echo JHtml::_('grid.sort', 'No de radicado', 'no_radicado', $listDirn, $listOrder); ?></span></td>
								<td><span><?php echo JHtml::_('grid.sort', 'Fecha de radicación', 'fecha_radicado', $listDirn, $listOrder); ?></span></td>
								<td><span><?php echo JHtml::_('grid.sort', 'Placa', 'placa', $listDirn, $listOrder); ?></span></td>
								<td>Fallo</td>
							</tr>
						</table>
					</div>
					<div class="content">

						<?php if (count( $this->servicios) <= 0) {
						?>
						<table>
						<tr><td><h3>No hay resultados</h3></td></tr></table>	
						<?php
						}else{ ?>
						<table>
							<?php foreach ($this->servicios as $key => $servicio) {
							?>
							<tr>
								<td><?php echo $servicio->no_radicado ?></td>
								<td><?php echo $servicio->fecha_radicado ?></td>
								<td><?php echo $servicio->placa ?></td>
								<td><a href="#" class="vermas" data-id="<?php echo $servicio->id ?>" data-description="<?php echo $servicio->descripcion_fallo ?>">Ver más</a></td>
							</tr>
							<?php
							} ?>
							
						</table>
						<?php } ?>
					</div>	
				</div>
			</div>
			<div>
				<input type="hidden" name="task" value="" />
				<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
				<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
				<input type="hidden" name="boxchecked" value="0" />
				<?php echo JHtml::_('form.token'); ?>
			</div>	
		</form>
		
		</div>
		<div class="em-over-screen"></div>
	    <div class="em-modal-box">
            <div class="header">
                <i class="close-modal-button">x</i>
            </div>
            <div class="body">
                Hello modal box
            </div>
        </div>

        <div class="wrapper-paginator">
			<?php echo $this->paginations->getListFooter(); ?>
		</div>

</div>
