<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

//get the hosts name
jimport('joomla.environment.uri' );
$host = JURI::root();
$document = JFactory::getDocument();

$app = JFactory::getApplication();

$document->addStyleSheet($host.'less/load-styles.php?load=solicitud');

jimport('joomla.application.module.helper');
$module = JModuleHelper::getModule('mod_vm_login');

if (  $this->group != '17' && $this->group != '10' && $this->group != '11') {
	$session = JFactory::getSession(); // starting the session
	$session->destroy(); //This will destroy the joomla session
	$app->redirect('index.php');	
}
?>

<div class="close-session">
	<?php echo JModuleHelper::renderModule($module); ?>
</div>

<?php echo $this->renderMenu(); ?>

<div class="content-solicitud">
	
	<div class="solicitud">
		<h2>Solicitud de servicio</h2>
		<form id="solicitud-servicio-form">
			<div class="form-solicitud">
				<div class="left">
					<ul>
						<li><input type="text" name="placa" placeholder="Placa de vehículo"></li>
						<li><label>Fecha y hora de visita</label><input readonly type="text" name="fecha_visita" id="visita" placeholder="AAAA/MM/DD"><i class="calendar" data-calendar="#visita"></i></li>
						<li><input type="text" name="kilometraje" placeholder="Kilometraje en gas"></li>
						<li><label>Fecha de instalación</label><input readonly type="text" name="fecha_instalacion" id="instalacion" class="fecha" placeholder="AAAA/MM/DD"><i class="calendar" data-calendar="#instalacion"></i></li>
					</ul>
				</div>
				<div class="right"> 
					<ul>
						<li><input type="text" name="sistema" placeholder="Sistema instalado"></li>
						<li><textarea name="descripcion_fallo" placeholder="Descripción fallo"></textarea></li>
					</ul>
				</div>
				<div class="accion">
					<input type="submit" value="Enviar" class="button-app"/>
				</div>
			</div>	
		</form>
		
	</div>

</div>



