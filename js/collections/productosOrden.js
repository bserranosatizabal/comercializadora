/**
* Products Orden Collection
* version : 1.0
* package: Comercializadora.frontend
* package: Comercializadora.frontend.mvc
* author: Alejandro Suarez
* Creation date: Sep 2014
*
* Allows to manage the collections of funcion model
*
*/
( function( $, window, document, utilities ){


	// Extends my object from Backbone model
	ProductosOrden = Backbone.Collection.extend({

			model: ProductoOrdenModel

			/**
	        * Gets the event from id
	        *
	        */
	    

	});

})( jQuery, this, this.document, this.Misc, undefined );