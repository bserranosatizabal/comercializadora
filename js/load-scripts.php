<?php
/**
* Odontoline Load Scripts
*
* We've decided to concatenate most of the scripts in the site
* so that there are less server requests, on the other hand we're
* gZipping the scripts so that they're lighter.
*
* @package OdontoLine
* @subpackage Utilities
*/

// Set default timezone
ini_set( 'date.timezone', 'America/Bogota' );

if ( substr_count( $_SERVER[ 'HTTP_ACCEPT_ENCODING' ], 'gzip' ) )
	ob_start( 'ob_gzhandler' );
else
	ob_start();


/**
* Get File's contents
* Taken from WordPress
*
* @param { string } path of the file
* @return { string } contents of the file, muting errors
*/
function get_file( $path ) {

	if ( function_exists( 'realpath' ) )
		$path = realpath( $path );

	if ( ! $path || ! @is_file( $path ) )
		return '';

	return @file_get_contents( $path );
}


/*
|---------------------------------------------------------------------------
| Define Config Variables - Output - Time
|---------------------------------------------------------------------------
| Output string
|
*/
$out = '';
$GLOBALS[ 'gTime' ] = mktime( 0, 0, 0, 21, 5, 1980 );
$cache = 'cache.js';
$compress = false;



/*
|---------------------------------------------------------------------------
| Include config files
|---------------------------------------------------------------------------
|
| App struture script
|
*/
$config = array();

foreach ( $config as $handle ) {
	$path = $handle;
	$out .= get_file( $path ) . "\n";

	// Time
	$file_time = filemtime( $path );
	if ( $file_time > $GLOBALS[ 'gTime' ] ) {
		$GLOBALS[ 'gTime' ] = $file_time;
	}

}


/*
|---------------------------------------------------------------------------
| Include helpers
|---------------------------------------------------------------------------
| Utilities - Helper functions used thrughout the project
|
*/

$utilities = array( 'misc.js' );

foreach ( $utilities as $handle ) {
	$path = 'helpers/' . $handle;
	$out .= get_file( $path ) . "\n";

	// Time
	$file_time = filemtime( $path );
	if ( $file_time > $GLOBALS[ 'gTime' ] ) {
		$GLOBALS[ 'gTime' ] = $file_time;
	}

}


/*
|---------------------------------------------------------------------------
| Include Models
|---------------------------------------------------------------------------
|
| Javascript Models as part
*/
$models = array( 'servicio', 'gestion', 'producto', 'empresa', 'certificacion', 'serial', 'garantia', 'productoGarantia', 'vehiculo', 'productoOrden', 'orden', 'imageUpload' );

foreach ( $models as $handle ) {
	$path = 'models/' . $handle . '.js';
	$out .= get_file( $path ) . "\n";

	// Time
	$file_time = filemtime( $path );
	if ( $file_time > $GLOBALS[ 'gTime' ] ) {
		$GLOBALS[ 'gTime' ] = $file_time;
	}

}

/*
|---------------------------------------------------------------------------
| Include Collections
|---------------------------------------------------------------------------
|
| Javascript Colections as part
*/
$collections = array( 'productos', 'productosOrden' );

foreach ( $collections as $handle ) {
	$path = 'collections/' . $handle . '.js';
	$out .= get_file( $path ) . "\n";

	// Time
	$file_time = filemtime( $path );
	if ( $file_time > $GLOBALS[ 'gTime' ] ) {
		$GLOBALS[ 'gTime' ] = $file_time;
	}

}



/*
|---------------------------------------------------------------------------
| Include Views
|---------------------------------------------------------------------------
|
| Views as part of the HMVC model ( Handler, Model, Views, Controller ) are
| inteneded to move and affect markup elements
*/

$views = array( 'solicitudservicio', 'consulta', 'gestion', 'garantia', 'gestionGarantia', 'administracion', 'ordenProducto', 'imageUpload' );

foreach ( $views as $handle ) {
	$path = 'views/' . $handle . '.js';
	$out .= get_file( $path ) . "\n";

	// Time
	$file_time = filemtime( $path );
	if ( $file_time > $GLOBALS[ 'gTime' ] ) {
		$GLOBALS[ 'gTime' ] = $file_time;
	}

}



/*
|---------------------------------------------------------------------------
| Catching
|---------------------------------------------------------------------------
|
| This catching is based upon the Less Tutorials in 'net.tutplus.com' and
| the load-scripts.php file in wordpress.
|
| We're using a library called JSMinPlus which compresses javascript using a javascript parser.
| In this case, we're storing the time of the last modifyed js include, and based on that compare it to the
| cache.js file that is generated when not existent. If the time of the cache.js is greather that that of
| the last modified script, we just use the cache.js. On the other hand if the time of the last modifyed
| script is greather that that of the cache.js, we compress the files again and write that content
| to the cache.js again
|
|
*/

/**
* Check for the cache.js file
*/
if ( file_exists ( $cache ) ) {

	$cacheTime = filemtime( $cache );

	if ( $cacheTime < $GLOBALS[ 'gTime' ] ) {
		$GLOBALS[ 'gTime' ] = $cacheTime;
		$recache = true;
	}
	else {
		$recache = false;
	}
}

else {
	$recache = true;
}

if ( ! $recache && isset( $_SERVER[ 'IF-Modified-Since' ] )
	&& strtotime( $_SERVER[ 'If-Modified-Since' ] ) >= $GLOBALS[ 'gTime' ] ) {

	header( 'HTTP/1.0 304 Not Modified' );
}

else {
	header( 'Content-Type: application/x-javascript; charset=UTF-8' );
	header( 'Last-Modified: ' . gmdate( 'D, d M Y H:i:s', $GLOBALS[ 'gTime' ] ) . ' GMT' );

	// Compress the files if get here and write that
	// content to the cache.js
	if ( $recache ) {

		require_once 'libs/JSMinPlus.php';

		//$libs = get_file( $libs );

		if ( $compress ) {
			$js = JSMinPlus::minify( $out );
		}
		else {
			$js = $out;
		}

		// Change the permissions of the cache file to 666
		// and write to the file
		file_put_contents( $cache, $js );

		// NOTE: this does not usually work in the server
		//chmod( $cache, 0666 );

		//echo $libs;
		echo $js;

	}

	else {
		//readfile( $libs );
		readfile( $cache );
	}

}


?>