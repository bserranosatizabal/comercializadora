/**
* Empresa Model
* version : 1.0
* package: odontoline.frontend
* package: odontoline.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){


	// Extends my object from Backbone model
	EmpresaModel = Backbone.Model.extend({
	        defaults: {
		            id: 0
		        ,	nit: ""
		        ,	nombre: ""
	        }

	    ,   initialize: function(){
	            
	        }
    });

})( jQuery, this, this.document, this.Misc, undefined );