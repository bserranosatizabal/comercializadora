/**
* Garantia Model
* version : 1.0
* package: comercializadora.frontend
* package: comercializadora.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){


	// Extends my object from Backbone model
	GarantiaModel = Backbone.Model.extend({
	        defaults: {
		        	id: 0
		        ,	placa: ""
		        ,	kilometraje: ""
		        ,	sistema: ""
		        ,	fecha_instalacion: "0000-00-00"
		        ,	no_radicado: ""
		        ,	fecha_radicado: "0000-00-00"
		        ,	estado: ""
	        }

	    ,   initialize: function(){

	            
	        }

	        /**
	        * Gets the event from id
	        *
	        */
	    ,	save: function( productos ,callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_solicitud'
					,	task: 'garantia.saveGarantia'
					,	data: _this.attributes
					,	productos: productos

				}
				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}

	    ,	saveReplica: function( productos ,callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_solicitud'
					,	task: 'garantia.saveGarantia'
					,	data: _this.attributes
					,	productos: productos

				}
				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}

	    ,	generateCertificacion: function( codigo, garantia, productos, callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_solicitud'
					,	task: 'garantia.generatePdfGarantia'
					,	codigo: codigo
					,	garantia: garantia
					,	productos: productos

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}
    });

})( jQuery, this, this.document, this.Misc, undefined );