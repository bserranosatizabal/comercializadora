/**
* Gestion Model
* version : 1.0
* package: comercializadora.frontend
* package: comercializadora.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){


	// Extends my object from Backbone model
	GestionModel = Backbone.Model.extend({
	        defaults: {
		            id: 0
		        ,	id_orden: 0
		        ,	id_tecnico: 0
		        ,	fecha_servicio: "0000-00-00"
		        ,	numero_asistencia: ""   
	        }

	    ,   initialize: function(){
	            
	        }


	    	/**
	    	* Saves the event
	    	*
	    	* @param { function } the callback function
	    	* @param { function } the callback error function
	    	*
	    	*/
	    ,	save: function( callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_gestion'
					,	task: 'evento.ajaxSave'
					,	evento: _this.attributes

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );
	    	}
    });

})( jQuery, this, this.document, this.Misc, undefined );