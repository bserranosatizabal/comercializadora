/**
* Certificacion Model
* version : 1.0
* package: Comercializadora.frontend
* package: Comercializadora.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){


	// Extends my object from Backbone model
	OrdenModel = Backbone.Model.extend({
	        defaults: {
		            productos: {}
		        ,	vehiculo: {}
	        }

	    ,   initialize: function(){

	            
	        }

	        /**
	        * Gets the event from id
	        *
	        */
	    ,	save: function( callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_producto'
					,	task: 'producto.saveVehiculo'
					,	data: _this.attributes

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}

	    ,	generateCertificacion: function( codigo, callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_producto'
					,	task: 'producto.generatePdf'
					,	codigo: codigo
					,	data: _this.attributes

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}
    });

})( jQuery, this, this.document, this.Misc, undefined );