/**
* Productos Model
* version : 1.0
* package: odontoline.frontend
* package: odontoline.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){


	// Extends my object from Backbone model
	ProductoModel = Backbone.Model.extend({
	        defaults: {
		            id: 0
		        ,	nombre: ""
		        ,	codigo_certificado: ""
	        }

	    ,   initialize: function(){
	            
	        }

	       /**
	        * Gets the event from id
	        *
	        */
	   

    });

})( jQuery, this, this.document, this.Misc, undefined );