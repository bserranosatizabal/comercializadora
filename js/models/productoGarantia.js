/**
* Producto Garantia Model
* version : 1.0
* package: comercializadora.frontend
* package: comercializadora.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){


	// Extends my object from Backbone model
	ProductoGarantiaModel = Backbone.Model.extend({
	        defaults: {
		        	id_garantia: 0
		        ,	id_producto: 0
		        ,	nombre_producto: ''
		        ,	serial: ""
		        ,	descripcion_fallo: ""
		        ,	estado: ""
		        ,	diagnostico: ""
		        ,	observaciones: ""
		        ,	id_tipo: 0
		        ,	replica: ""
		        ,	dias_garantia: ""
		        ,	fecha_recepcion: "0000-00-00"
		        ,	fecha_radicado: "0000-00-00"
	        }

	    ,   initialize: function(){

	            
	        }

	    ,	saveReplica: function( data, callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_solicitud'
					,	task: 'garantia.saveReplica'
					,	data: data

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}

	    ,	save: function( callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_solicitud'
					,	task: 'garantia.saveGestion'
					,	data: _this.attributes

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}

	    ,	changeState: function( data, callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_solicitud'
					,	task: 'garantia.changeStateProducts'
					,	data: data

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}
    });

})( jQuery, this, this.document, this.Misc, undefined );