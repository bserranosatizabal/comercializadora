/**
* Certificacion Model
* version : 1.0
* package: Comercializadora.frontend
* package: Comercializadora.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){


	// Extends my object from Backbone model
	ProductoOrdenModel = Backbone.Model.extend({
	        defaults: {
		            referencia: ''
		        ,	serial: ''
		        ,	foto: ''
		        ,	id_orden: 0
		        ,	id_tipo: 0
	        }

	    ,   initialize: function(){

	            
	        }
    });

})( jQuery, this, this.document, this.Misc, undefined );