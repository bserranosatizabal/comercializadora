/**
* Sample Model
* version : 1.0
* package: odontoline.frontend
* package: odontoline.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){


	// Extends my object from Backbone model
	SampleModel = Backbone.Model.extend({
	        defaults: {
		            id: 0
	        }

	    ,   initialize: function(){
	            
	        }

	        /**
	        * Gets the event from id
	        *
	        */
	    ,	getBy: function( key, value, callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_gestion'
					,	task: 'evento.ajaxGet'
					,	key: key
					,	value: value

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}


	    	/**
	    	* Saves the event
	    	*
	    	* @param { function } the callback function
	    	* @param { function } the callback error function
	    	*
	    	*/
	    ,	save: function( callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_gestion'
					,	task: 'evento.ajaxSave'
					,	evento: _this.attributes

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );
	    	}
    });

})( jQuery, this, this.document, this.Misc, undefined );