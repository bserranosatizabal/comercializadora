/**
* Seriales Model
* version : 1.0
* package: odontoline.frontend
* package: odontoline.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){


	// Extends my object from Backbone model
	SerialModel = Backbone.Model.extend({
	        defaults: {
		            id: 0
		        ,	serial: ""
		        ,	producto: ""
		        ,	capacidad: ""
		        ,	certificado: ""
		        ,	fecha_inicio: "0000-00-00"
		        ,	fecha_fin: "0000-00-00"
	        }

	    ,   initialize: function(){
	            
	        }

	        /**
	        * Gets the event from id
	        *
	        */
	    ,	getBy: function( key, value, callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_consulta'
					,	task: 'consulta.searchCertificationBySerial'
					,	key: key
					,	value: value

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}

	    ,	generateSerial: function( serial, codigo, html, callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_consulta'
					,	task: 'consulta.generatePdfSerial'
					,	serial: serial
					,	codigo: codigo
					,	html: html

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}	

    });

})( jQuery, this, this.document, this.Misc, undefined );