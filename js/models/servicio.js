/**
* Servicio Model
* version : 1.0
* package: comercializadora.frontend
* package: comercializadora.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){


	// Extends my object from Backbone model
	ServicioModel = Backbone.Model.extend({
	        defaults: {
		            id: 0
		        ,	placa: ""
		        ,	fecha_visita: "0000-00-00"
		        ,	kilometraje: ""
		        ,	fecha_instalacion: "0000-00-00"
		        ,	sistema: ""
		        ,	descripcion_fallo: ""
		        ,	no_radicado: ""
		        ,	fecha_radicado: "0000-00-00"
		        ,	estado: ""
		        ,	id_tecnico: 0
		        ,	fecha_servicio: "0000-00-00"
		        ,	numero_asistencia: ""
		        ,	acciones_realizadas: ""
	        }

	    ,   initialize: function(){
	            
	        }

	        /**
	        * Gets the event from id
	        *
	        */
	    ,	save: function(  callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_solicitud'
					,	task: 'solicitud.saveSolicitud'
					,	data: _this.attributes
				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}

	    	/**
	        * Gets the event from id
	        *
	        */
	    ,	saveGestion: function(  callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_solicitud'
					,	task: 'solicitud.saveGestion'
					,	data: _this.attributes
				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}

	    ,	generatePdf: function( codigo, data, callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_solicitud'
					,	task: 'solicitud.generatePdfServicio'
					,	codigo: codigo
					,	data: data

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}

	    ,	rangeDate: function( data, callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_solicitud'
					,	task: 'solicitud.rangeDate'
					,	data: data

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}

	    ,	getNamesUser: function( data, callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_solicitud'
					,	task: 'solicitud.getNamesUser'
					,	data: data

				}

				

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );

	    	}			

    });

})( jQuery, this, this.document, this.Misc, undefined );