/**
* Certificacion Model
* version : 1.0
* package: Comercializadora.frontend
* package: Comercializadora.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){


	// Extends my object from Backbone model
	VehiculoModel = Backbone.Model.extend({
	        defaults: {
		            id: 0
		        ,	placa: ""
		        ,	fecha_inicio: "0000-00-00"
	        }

	    ,   initialize: function(){

	            
	        }
    });

})( jQuery, this, this.document, this.Misc, undefined );