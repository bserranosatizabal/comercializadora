/**
* Administracion Garantia View
* version : 1.0
* package: Comercializadora .frontend
* package: Comercializadora .frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var AdministracionView = {};

	// Extends my object from Backbone events
	AdministracionView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
				'click #producto': 'showAddProduct'
			}

		,	view: this

		,	initialize: function(){

				_.bindAll(
					this, 
					'saveAdmin',
					'onCompleteSaveAdmin'
				);

				this.garantia = new GarantiaModel();
				this.producto = new ProductoGarantiaModel();

			}

			/**
			* Save form garantia
			*
			*/
		,	saveAdmin: function( e ){

				e.preventDefault();

				var data = utilities.formToJson( '#solicitud-servicio-form' )
				,	required = [ 
						'placa',
						'fecha_visita',
						'kilometraje',
						'fecha_instalacion',
						'sistema',
						'descripcion_fallo'

					]
				,	errors = []
				,	_this = this;


				utilities.validateEmptyFields( required, data, errors );

				if( errors.length > 0)
					return utilities.showNotification( 'error', 'Diligencie totalmente el formulario.', 0 );

				this.onProgress( 'Generando solicitud...', function(){

					//instance the model of service 
					_this.servicio.set( data );

					// instance function to save service
					_this.servicio.save(  _this.onCompleteSaveGestion, _this.onError );
					
				});
			}

			/**
			* On complete Save form garantia
			*
			*/
		,	onCompleteSaveAdmin: function( data ){

				
			}

			/**
			* Displays div with a progress
			*
			*/
		,	onProgress: function( message, callback ){


				var data = { 
					message : message
				};

				var html = new EJS( { url: url + 'js/templates/progress.ejs' } ).render( data );

				var modal = {
					content: html
				};

				utilities.showModalWindow( modal );

				if( typeof callback == 'function' ){

					setTimeout( function(){ callback.call(); }, 2000 );
				}
					
				
			}

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.AdministracionView = new AdministracionView();

})( jQuery, this, this.document, this.Misc, undefined );