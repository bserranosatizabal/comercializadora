/**
* Consulta View
* version : 1.0
* package: odontoline.frontend
* package: odontoline.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var ConsultaView = {};

	// Extends my object from Backbone events
	ConsultaView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
				'submit #consulta-form': 'consultaCertificacion',
				'click #print-certificacion': 'generatePdfCertificacion',
				'click #print-serial': 'generatePdfSerial'

			}

		,	view: this

		,	initialize: function(){

				_.bindAll(
					this, 
					'consultaCertificacion',
					'onCompleteConsultaCertificacion',
					'onCompleteConsultaSerial',
					'renderCertificacion',
					'renderSerial'
				);

				this.producto = new ProductoModel();
				this.empresa = new EmpresaModel();
				this.certificacion = new CertificacionModel();
				this.serial = new SerialModel();
				this.certificaHtml = '';
				this.serialHtml = '';

			}

			/**
			* Validate form and redirect the request to the different model
			*
			*/
		,	consultaCertificacion: function( e ){

				e.preventDefault();
				
				var data = utilities.formToJson( '#consulta-form' )
				,	required = [ 
						'codigo',
						'serial'
					]
				,	errors = []
				,	_this = this;

				utilities.validateEmptyFields( required, data, errors );

				if( data.codigo != "" && data.serial != "")
					return utilities.showNotification( 'error', 'Seleccione solo una opción (Código o Serial).', 0 );

				if( data.codigo != "" && data.serial == ""){

					this.onProgress( 'Buscando certificación...', function(){


						//instance the model of certification
						_this.certificacion.set( { codigo: data.codigo } );

						// instance function to consult the certification by code
						_this.certificacion.getBy( 'codigo', _this.certificacion.get( 'codigo' ), _this.onCompleteConsultaCertificacion, _this.onError );

					});
					
				}

				if( data.codigo == "" && data.serial != ""){

					this.onProgress( 'Buscando serial...', function(){

						//instance the model of seriales
						_this.serial.set( { serial: data.serial } );

						// instance function to consult the certification by serial
						_this.serial.getBy( 'serial', _this.serial.get( 'serial' ), _this.onCompleteConsultaSerial, _this.onError );
						
					} );

				}

				if( data.codigo == "" && data.serial == "")
					return utilities.showNotification( 'error', 'Digite por favor un codigo o serial.', 0 );

				
			}

			/**
			* Displays div with a progress
			*
			*/
		,	onProgress: function( message, callback ){


				var data = { 
					message : message
				};

				var html = new EJS( { url: url + 'js/templates/progress.ejs' } ).render( data );

				var modal = {
					content: html
				};

				utilities.showModalWindow( modal );

				if( typeof callback == 'function' ){

					setTimeout( function(){ callback.call(); }, 2000 );
				}
					
				
			}	

			/**
			* Function when complete the process load certification by code
			*
			*/
		,	onCompleteConsultaCertificacion: function( data ){

				if( data.status == 404 ){
					var html = new EJS( { url: url + 'js/templates/notFound.ejs' } ).render( data );

					var modal = {
						content: html
					};

					return utilities.showModalWindow( modal );
				}

				data.certificacion[0].fecha_inicio = utilities.dateFormat( data.certificacion[0].fecha_inicio );

				data.certificacion[0].fecha_fin = utilities.dateFormat( data.certificacion[0].fecha_fin );

				this.empresa.set( data.empresa );
				this.producto.set( data.producto );
				this.certificacion.set( data.certificacion[0] );

				this.renderCertificacion();
				
			}	

			/**
			* Function when complete the process load certification by serial
			*
			*/
		,	onCompleteConsultaSerial: function( data ){

				if( data.status == 404 ){
					var html = new EJS( { url: url + 'js/templates/notFound.ejs' } ).render( data );

					var modal = {
						content: html
					};

					return utilities.showModalWindow( modal );
				}

				data.serial[0].fecha_inicio = utilities.dateFormat( data.serial[0].fecha_inicio );

				data.serial[0].fecha_fin = utilities.dateFormat( data.serial[0].fecha_fin );
 	
				this.serial.set( data.serial[0] );

				this.renderSerial();
				
			}

			/**
			* Display EJS when is a certification by code
			*
			*/
		,	renderCertificacion: function(){

				var html = new EJS( { url: url + 'js/templates/certificacion.ejs' } ).render( this );

				this.certificaHtml = html;

				var modal = {
					content: html
				};

				return utilities.showModalWindow( modal );
			}

			/**
			* Display EJS when is a certification by serial
			*
			*/
		,	renderSerial: function(){

				var html = new EJS( { url: url + 'js/templates/serial.ejs' } ).render( this );

				this.serialHtml = html;

				var modal = {
					content: html
				};

				return utilities.showModalWindow( modal );
				
			}

		,	generatePdfCertificacion: function( e ){

				e.preventDefault();

				this.certificacion.set();

				var codigoCertificacion = this.certificacion.get('codigo');

				this.certificacion.generateCertificacion( codigoCertificacion, this.certificaHtml, this.onCompleteGeneratePdfCertificacion, this.onError );

			}

		,	onCompleteGeneratePdfCertificacion: function( data ){

				if( data.path == '' ){

					data.message = 'El PDF no pudo ser generado.';

					var html = new EJS( { url: url + 'js/templates/notFound.ejs' } ).render( data );

					var modal = {
						content: html
					};

					return utilities.showModalWindow( modal );
				}

				window.open( url + data.path, '_blank' );
			}

		,	generatePdfSerial: function( e ){

				e.preventDefault();

				this.serial.set();


				var codigoSerial = this.serial.get('serial');
				var codigoCertificacionSerial = this.serial.get('certificado');

				this.serial.generateSerial( codigoSerial, codigoCertificacionSerial, this.serialHtml, this.onCompleteGeneratePdfSerial, this.onError );

			}

		,	onCompleteGeneratePdfSerial: function( data ){

				if( data.path == '' ){

					data.message = 'El PDF no pudo ser generado.';

					var html = new EJS( { url: url + 'js/templates/notFound.ejs' } ).render( data );

					var modal = {
						content: html
					};

					return utilities.showModalWindow( modal );
				}

				window.open( url + data.path, '_blank' );

			}						

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.ConsultaView = new ConsultaView();

})( jQuery, this, this.document, this.Misc, undefined );