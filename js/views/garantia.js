/**
* Garantia View
* version : 1.0
* package: Comercializadora .frontend
* package: Comercializadora .frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var GarantiaView = {};

	// Extends my object from Backbone events
	GarantiaView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
				'click #producto': 'showAddProduct',
				'submit #garantia-form': 'saveGarantia',
				'click #add-product': 'onAddProduct',
				'click .remove-product': 'onRemoveProduct',
				'click .ver-mas': 'showDescription',
				'click #print-garantia': 'generatePdfCertificacion',
				'click .button-diagnostico': 'showDiagnostico',
				'submit #replica-form': 'saveReplica'
			}

		,	view: this

		,	initialize: function(){

				_.bindAll(
					this, 
					'saveGarantia',
					'onCompleteSaveGarantia',
					'onAddProduct',
					'saveReplica',
					'onCompleteSaveReplica'
				);

				this.garantia = new GarantiaModel();
				this.productos = new Productos();

				this.productos.on({
					'add': this.onRenderProducts,
					'remove': this.onRenderProducts,
					'reset': this.onRenderProducts

				}, this);

				this.radicado = '';
				this.dataGarantia = '';
				this.dataProductos = '';

			}

			/**
			* Save form garantia
			*
			*/
		,	saveGarantia: function( e ){

				e.preventDefault();

				var data = utilities.formToJson( '#garantia-form' )
				,	required = [ 
						'placa',
						'sistema',
						'kilometraje',
						'fecha_instalacion',
						'sistema'

					]
				,	errors = []
				,	_this = this;


				utilities.validateEmptyFields( required, data, errors );

				if( errors.length > 0)
					return utilities.showNotification( 'error', 'Diligencie totalmente el formulario.', 0 );

				if( ! utilities.justNumbers( data.kilometraje ) )
					return utilities.showNotification( 'error', 'El campo kilometraje solo debe contener números.', 0 );

				if ( this.productos.length <= 0 )
					return utilities.showNotification( 'error', 'Debe agregar un producto.', 0 );

				this.dataGarantia = data;

				this.onProgress( 'Generando garantia...', function(){

					//instance the model of service 
					_this.garantia.set( data );

					_this.dataProductos = _this.productos.toJSON();

					// instance function to save service
					_this.garantia.save(  _this.dataProductos, _this.onCompleteSaveGarantia, _this.onError );
					
				});
			}

			/**
			* On complete Save form garantia
			*
			*/
		,	onCompleteSaveGarantia: function( data ){

				if( data.status == 500 )
					return utilities.showNotification( 'error', message, 0 );

				$('#garantia-form')[0].reset();

				this.productos.reset();

				this.radicado = data.radicado;

				this.onProgress( data.message );
				this.openNotification( data );


				
			}

			/**
			* Add product to the table
			*
			*/
		,	onAddProduct: function( e ){

				e.preventDefault();

				// Validate data
				var data = utilities.formToJson( '#producto-form' )
				,	required = [ 
						'serial',
						'descripcion_fallo'

					]
				,	errors = []
				,	_this = this;

				


				utilities.validateEmptyFields( required, data, errors );

				if( errors.length > 0)
					return utilities.showNotification( 'error', 'Diligencie totalmente el formulario.', 0 );

				if( data.id_producto == '')
					return utilities.showNotification( 'error', 'Seleccione un producto.', 0 );

				data.nombre_producto = $('#productos option[value="'+ data.id_producto +'"]').text();

				// Instance new model
				var producto = new ProductoGarantiaModel( data );

				// Add model to collection
				this.productos.add( producto );

				$('#producto-form')[0].reset(); 
				
			}

			/**
			* Removes a product from table
			*
			*/
		,	onRemoveProduct: function( e ){

				e.preventDefault();

				// Get the id
				var target = e.currentTarget
				,	id = $( target ).data( 'id' );

				// Add model to collection
				this.productos.remove( { cid: id } );
				
			}



			/**
			* Add product to the table
			*
			*/
		,	onRenderProducts: function( model ){

				model.url = url;

				var html = new EJS({ url: url + 'js/templates/producto.ejs' }).render({products: this.productos });
				
				$('.content-products').html(html);
				
			}

			/**
			* Show notification
			*
			*/

		,	openNotification: function( data ){

				data.url =  url;

				var html = new EJS({ url: url + 'js/templates/notificacionGarantia.ejs' }).render( data );

				var modal = {
					content: html,
					height: 340
				};

				utilities.showModalWindow( modal );

			}

		,	showAddProduct: function( e ){

				e.preventDefault();

				if( $('.medium').is( ':visible' ) )
					return $('.medium').slideUp();
				
				$('.medium').slideDown( 'slow', function(){

					$('.medium').css({'display':'inline-block'});

				});		

			}

		,	generatePdfCertificacion: function( e ){

				e.preventDefault();

				this.garantia.generateCertificacion( this.radicado ,this.dataGarantia, this.dataProductos, this.onCompleteGeneratePdfCertificacion, this.onError );

			}

		,	onCompleteGeneratePdfCertificacion: function( data ){

				if( data.path == '' ){

					data.message = 'El PDF no pudo ser generado.';

					var html = new EJS( { url: url + 'js/templates/notFound.ejs' } ).render( data );

					var modal = {
						content: html
					};

					return utilities.showModalWindow( modal );
				}

				window.open( url + data.path, '_blank' );
				window.open( url + data.cliente, '_blank' );

			}

			/**
			* Displays div with a progress
			*
			*/
		,	onProgress: function( message, callback ){


				var data = { 
					message : message
				};

				var html = new EJS( { url: url + 'js/templates/progress.ejs' } ).render( data );

				var modal = {
					content: html
				};

				utilities.showModalWindow( modal );

				if( typeof callback == 'function' ){

					setTimeout( function(){ callback.call(); }, 2000 );
				}
					
				
			}

		,	showDescription: function( e ){

				e.preventDefault();

				var data = {}
				,	target = e.currentTarget;

				data.descripcion = $( target ).data('descripcion');


				var html = new EJS({ url: url + 'js/templates/descripcion.ejs' }).render( data );

				var modal = {
					content: html,
					height: 340
				};

				utilities.showModalWindow( modal );

			}

		,	showDiagnostico: function( e ){

				e.preventDefault();

				var data = {}
				,	target = e.currentTarget;

				data.diagnostico = $( target ).data('diagnostico');
				data.replica = $( target ).data('replica');
				data.id_producto = $( target ).data('id');

				var html = new EJS({ url: url + 'js/templates/diagnostico.ejs' }).render( data );

				var modal = {
					content: html,
					height: 600
				};

				$('.em-modal-box .body').css({ 'max-height': 'none', 'overflow-y': 'hidden' });

				utilities.showModalWindow( modal );

			}

		,	saveReplica: function(e){

				e.preventDefault();

				// Validate data
				var data = utilities.formToJson( '#replica-form' )
				,	required = [ 
						'replica'
					]
				,	errors = []
				,	_this = this;

				
				utilities.validateEmptyFields( required, data, errors );

				if( errors.length > 0)
					return utilities.showNotification( 'error', 'Diligencie la replica por favor.', 0 );

				var producto = new ProductoGarantiaModel();

				producto.saveReplica( data, this.onCompleteSaveReplica, this.onError );

			}

		,	onCompleteSaveReplica: function( data ){

				if( data.status == 500 )
					return utilities.showNotification( 'error', data.message, 0 );

				
				utilities.showNotification( 'error', data.message , 1500, function(){
					return location.reload();
				} );
					

				
			}	


			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.GarantiaView = new GarantiaView();

})( jQuery, this, this.document, this.Misc, undefined );