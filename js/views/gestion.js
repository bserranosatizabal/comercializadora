/**
* Gestion View
* version : 1.0
* package: odontoline.frontend
* package: odontoline.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var GestionView = {};

	// Extends my object from Backbone events
	GestionView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
				'keyup #identificacion': 'getNamesUser',
				'change #estado': 'reloadFormSelect',
				'submit #gestion-servicio-form': 'saveGestion'
			}

		,	view: this

		,	initialize: function(){

				_.bindAll(
					this, 
					'saveGestion',
					'onCompleteSaveGestion',
					'getNamesUser',
					'onGetNamesUser'

				);

				this.servicio = new ServicioModel();

			}

			/**
			* Triggers the click when user saves the analytics configuration
			*
			*/
		,	saveGestion: function( e ){

				e.preventDefault();

				var data = utilities.formToJson( '#gestion-servicio-form' )
				,	required = [ 
						'placa',
						'fecha_visita',
						'kilometraje',
						'fecha_instalacion',
						'sistema',
						'descripcion_fallo',
						'acciones_realizadas',
						'fecha_radicado',
						'fecha_servicio',
						'numero_asistencia'
					]
				,	errors = []
				,	_this = this;


				utilities.validateEmptyFields( required, data, errors );

				if( errors.length > 0)
					return utilities.showNotification( 'error', 'Diligencie totalmente el formulario.', 0 );

				if( data.id_tecnico == '')
					return utilities.showNotification( 'error', 'Seleccione un técnico.', 0 );

				if( ! utilities.justNumbers( data.numero_asistencia ) )
					return utilities.showNotification( 'error', 'El campo número de asistencia solo debe contener números.', 0 );


				this.onProgress( 'Guardando gestion...', function(){

					//instance the model of service 
					_this.servicio.set( data );

					// instance function to save service
					_this.servicio.saveGestion(  _this.onCompleteSaveGestion, _this.onError );
					
				});

			}

			/**
			* Displays div with a progress
			*
			*/
		,	onProgress: function( message, callback ){


				var data = { 
					message : message
				};

				var html = new EJS( { url: url + 'js/templates/progress.ejs' } ).render( data );

				var modal = {
					content: html
				};

				utilities.showModalWindow( modal );

				if( typeof callback == 'function' ){

					setTimeout( function(){ callback.call(); }, 2000 );
				}
					
				
			}

		,	getNamesUser: function( e ){

			    var data = $( '#identificacion' ).val();

			    this.servicio.getNamesUser( data, this.onGetNamesUser, this.onError );
			}

		,	onGetNamesUser: function( data ){

				$( "#identificacion" ).autocomplete({
			    	source: data,
			    	select: this.reloadForm
			   	});
			}

		,	reloadForm: function( event, ui ){

				$( "#identificacion" ).val( ui.item.value );
				return $( '#adminForm' ).submit();

			}

		,	reloadFormSelect: function(){

				$( '#adminForm' ).submit();
			}


		,	onCompleteSaveGestion: function( data ){

				if (data.status == 500)
					return utilities.showNotification( 'error', data.message, 0 );


				utilities._closeModalWindow();

				utilities.showNotification( 'success', data.message, 0 );

				return window.location.href = 'solicitud?layout=gestionservicio'; 
				
			}

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.GestionView = new GestionView();

})( jQuery, this, this.document, this.Misc, undefined );