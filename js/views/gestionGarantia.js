/**
* Gestion Garantia View
* version : 1.0
* package: Comercializadora .frontend
* package: Comercializadora .frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var GestionGarantiaView = {};

	// Extends my object from Backbone events
	GestionGarantiaView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
				'click #producto': 'showAddProduct',
				'submit #gestion-garantia-form': 'changeStateProducts',
				'submit #gestion-producto-form': 'saveProducto'
			}

		,	view: this

		,	initialize: function(){

				_.bindAll(
					this, 
					'saveProducto',
					'onCompleteSaveProducto',
					'changeStateProducts',
					'onCompleteChangeStateProducts'
				);

				this.garantia = new GarantiaModel();
				this.producto = new ProductoGarantiaModel();

			}

			/**
			* Save form garantia
			*
			*/
		,	saveProducto: function( e ){

				e.preventDefault();

				var data = utilities.formToJson( '#gestion-producto-form' )
				
				,	errors = []
				,	_this = this;

				if ( data.curview == '' ){

					var required = [ 
						'nombre_producto',
						'serial',
						'descripcion_fallo',
						'observaciones'
					]
				}else{
					var required = [ 
						'nombre_producto',
						'serial',
						'descripcion_fallo',
						'diagnostico'

					]
				}




				utilities.validateEmptyFields( required, data, errors );

				if( errors.length > 0)
					return utilities.showNotification( 'error', 'Diligencie totalmente el formulario.', 0 );

				this.onProgress( 'Guardando producto...', function(){

					//instance the model of service 
					_this.producto.set( data );


					// instance function to save service
					_this.producto.save(  _this.onCompleteSaveProducto, _this.onError );
					
				});
			}

			/**
			* On complete Save form garantia
			*
			*/
		,	onCompleteSaveProducto: function( data ){

				if( data.status == 500 )
					return utilities.showNotification( 'error', data.message, 0 );

				utilities.showNotification( 'success', data.message, 1500, function(){

					if( data.view == 'admin' )
						return window.location.href = 'index.php/producto';
					
					return window.location.href = 'index.php/solicitud?task=garantia.getGarantiaById&id=' + data.garantia;
				});
				
			}

			/**
			* Change state products
			*
			*/
		,	changeStateProducts: function( e ){

				e.preventDefault();

				var ids = [],
					_this = this,
					data = {};

				if($(".bottom tbody input[type='checkbox']:checked").length > 0){

   					$('.bottom tbody input[type=checkbox]').each(function () {

			           	if (this.checked)
			           		ids.push($(this).val()); 
					});

   				}else{
   					return utilities.showNotification( 'error', 'Seleccione un producto.', 0 );
   				}

   				if ( $( '#estado' ).val() == '' )
   					return utilities.showNotification( 'error', 'Seleccione un estado.', 0 );
	
   				data.ids = ids;
   				data.estado = $( '#estado' ).val();

   				// Call confirm dialog
   				var confirm = {
   					message: "Desea cambiar el estado de los productos seleccionados?",
   					selectorYes: '.confirm-accept-button',
   					selectorNo: '.confirm-cancel-button',
   					callbackYes: function(){
   						_this.producto.changeState( data, _this.onCompleteChangeStateProducts, _this.onError );
   					}
   				};



   				return utilities.showConfirm( confirm );

   				
			
			}


			/**
			* Change state products
			*
			*/
		,	onCompleteChangeStateProducts: function( data ){

				utilities.showNotification( 'error', data.message, 1500, function() {
					
					return location.reload();
				});
			}


			/**
			* Displays div with a progress
			*
			*/
		,	onProgress: function( message, callback ){


				var data = { 
					message : message
				};

				var html = new EJS( { url: url + 'js/templates/progress.ejs' } ).render( data );

				var modal = {
					content: html
				};

				utilities.showModalWindow( modal );

				if( typeof callback == 'function' ){

					setTimeout( function(){ callback.call(); }, 2000 );
				}
					
				
			}

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.GestionGarantiaView = new GestionGarantiaView();

})( jQuery, this, this.document, this.Misc, undefined );