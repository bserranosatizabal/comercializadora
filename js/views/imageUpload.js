/**
* Image Uploader View
* version : 1.0
* package: casainmobiliaria.frontend
* package: casainmobiliaria.frontend.mvc
* author: 
* Creation date: August 2014
*
* Allows to upload images for ads in Virtuemart
*
*/
( function( $, window, document, Utilities ){	

	// Create a var to manage the events
	var imageUploadView = {};

	// Extends my object from Backbone events
	imageUploadView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
			}		

		,	_self: this

		,	allowToUpload: false

		,	uploader: {}

		,	imageQue: []

		,	imageCount: 0

		,	attributes : { }

		,	defaults : {

					element: $('#image-uploader')[0]
				,	uploadButtonText: 'Subir imagenes'	
				,   action: '../index.php'
				,   params: {

				    	option: 'com_producto',
				    	task: 'producto.uploadImages'
				    }

				,   allowedExtensions: [ 'jpg', 'png', 'jpeg' ]
				,   debug: false
			    
			}

		,	initialize: function(){

				_.bindAll(
					this,
					'onSubmit',
					'onUpload',
					'onComplete'
				);


				
				if( $.isEmptyObject( this.attributes ) )
					this.attributes = this.defaults;
				else
					$.extend( true, this.attributes, this.defaults );



				// Add callbacks
				$.extend(true, this.attributes, {
						onSubmit: this.onSubmit
					,	onUpload: this.onUpload
					,	onComplete: this.onComplete
					,   onError: this.onError
				});
				
					

				if( $('#'+this.attributes.element.id).length )
					this.uploader = new qq.FileUploader( this.attributes );




			}

			/**
			*
			*/
		,	onSubmit: function( id, fileName ){

				var queue = $( "input[name='file']" )[0].files;

				if( this.imageCount == 0 )
					this.imageCount = queue.length;

			}

		,	onUpload: function( id, filename, xhr ){

				
			}

			/**
			*
			*/
		,	onProgress: function( id, fileName, loaded, total ){

				var queue = $( "input[name='file']" )[0].files;

				if ( queue.length < 3 || queue.length > 10 ) {
					this.uploader._handler.cancelAll();
					return Utilities.showNotification('error', 'sumadre', 0);
				}

				var perc = Math.floor( loaded / total * 100 );
			}

			/**
			*
			*/
		,	onComplete: function(id, fileName, responseJSON){

				if( ! responseJSON.success ){
					$( '#'+this.attributes.element.id + ' .qq-upload-list' ).remove();
					return;
				}

				$( '#'+this.attributes.element.id  + ' .qq-upload-list').html('Imagen cargada.');
				$( '#'+this.attributes.element.id  + ' .qq-upload-list').css({'color':'green'});

				window.comercializadoraImageQueue.push( responseJSON.filename );
				
			}

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				Utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}


	});	

	$(document).ready(function($) {
		
		//window.imageUploadView = new imageUploadView();

		window.comercializadoraImageQueue = []; 

		$.each( $('.product-picture-uploader'), function( key, value ){

			var id = $( value ).attr( 'id' );

			new imageUploadView( { attributes:{ element: $( '#' + id )[0] } });
		});

	});		

})( jQuery, this, this.document, this.Misc, 'undefined' );