/**
* Orden de Producto View
* version : 1.0
* package: odontoline.frontend
* package: odontoline.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var OrdenProductoView = {};

	// Extends my object from Backbone events
	OrdenProductoView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
				'click .input-checkbox input[type="checkbox"]': 'showAddProduct',
				'click #add-product-orden': 'onAddProduct',
				'click .remove-product': 'onRemoveProduct',
				'click #button-add-orden': 'saveOrden',
				'click #print-product': 'generatePdfCertificacion',
				'keyup #placa': 'filterByPlaca',
				'click .button-foto': 'showPicture'
			}

		,	view: this

		,	productType: 'regulador'

		,	initialize: function(){

				_.bindAll(
					this, 
					'saveOrden',
					'onAddProduct',
					'onProgress',
					'onCompleteSaveOrden',
					'generatePdfCertificacion'

				);

				this.productos = new ProductosOrden();

				this.productos.on({
					'add': this.onRenderProducts,
					'remove': this.onRenderProducts,
					'reset': this.onRenderProducts

				}, this);

				this.form = '';
				
				this.vehiculo = new VehiculoModel();
				this.orden = new OrdenModel();
				this.radicado = '';
				this.products = '';
				this.dataVehiculo = '';

			}

			/**
			* Save orden
			*
			*/
		,	saveOrden: function( e ){

				e.preventDefault();

				var data = utilities.formToJson( '#vehiculo-form' )
				,	required = [ 
						'placa',
						'kilometraje',
						'fecha_instalacion'

					]
				,	errors = []
				,	_this = this;

				utilities.validateEmptyFields( required, data, errors );

				if( errors.length > 0)
					return utilities.showNotification( 'error', 'Diligencie totalmente el formulario.', 0 );

				if( ! utilities.justNumbers( data.kilometraje ) )
					return utilities.showNotification( 'error', 'El campo kilometraje solo debe contener números.', 0 );

				if ( this.productos.length <= 0 )
					return utilities.showNotification( 'error', 'Debe agregar un producto.', 0 );

				this.dataVehiculo = data;

				this.vehiculo.set( data );

				this.onProgress( 'Generando orden...', function(){

					_this.products = _this.productos.toJSON();
					//instance the model of service 
					_this.orden.set( { vehiculo: _this.vehiculo.attributes, productos: _this.products } );

					// instance function to save service
					_this.orden.save(  _this.onCompleteSaveOrden, _this.onError );
					
				});


			}

			/**
			* Displays div with a progress
			*
			*/
		,	onProgress: function( message, callback ){


				var data = { 
					message : message
				};

				var html = new EJS( { url: url + 'js/templates/progress.ejs' } ).render( data );

				var modal = {
					content: html
				};

				utilities.showModalWindow( modal );

				if( typeof callback == 'function' ){

					setTimeout( function(){ callback.call(); }, 2000 );
				}
					
				
			}

		,	showAddProduct: function( e ){

				e.preventDefault();

				if($('.input-checkbox input[type="checkbox"]:checked').length > 0){
					$('.input-checkbox input[type="checkbox"]').each(function () {


			           	if (this.checked){

			           		if( $( '.'+this.name + '> .medium').is( ':visible' ) )
								return $( '.'+this.name + '> .medium').slideUp();
							
							$( '.'+this.name + '> .medium').slideDown( 'slow', function(){

								$( '.'+this.name + '> .medium').css({'display':'inline-block'});


							});
			           	}
					});

				}	
			}

			/**
			* Add product to the table
			*
			*/
		,	onRenderProducts: function( model ){

				var html = new EJS({ url: url + 'js/templates/productosOrden.ejs' }).render({products: this.productos.where({ tipo: this.productType }) });
				
				$('.'+this.form+' .content-products tbody').html(html);
				
			}

			/**
			* Add product to the table
			*
			*/
		,	onAddProduct: function( e ){

				e.preventDefault();

				var target = e.currentTarget;

				this.form = $(target).data( 'form' );

				// Validate data
				var data = utilities.formToJson( '.'+this.form )
				,	required = [ 
						'referencia',
						'serial'

					]
				,	errors = []
				,	_this = this;

				utilities.validateEmptyFields( required, data, errors );

				if( errors.length > 0)
					return utilities.showNotification( 'error', 'Diligencie totalmente el formulario de productos.', 0 );

				// set global var to know the product type
				this.productType = data.tipo;

				// Instance new model
				var producto = new ProductoOrdenModel( data );

				var serial = this.productos.findWhere( { serial: data.serial } );

				if( serial ){
					return utilities.showNotification( 'error', 'Este serial ya se encuentra registrado previamente, verifique los productos agregados.', 0 );
				}

				// Add picture to the product model
				if( window.comercializadoraImageQueue.length <= 0 )
					return utilities.showNotification( 'error', 'Cargue la imagen del producto antes de añadirlo', 0 );

				var foto = _.last( window.comercializadoraImageQueue );
				window.comercializadoraImageQueue = [];

				producto.set( 'foto', foto );
				
				// Add model to collection
				this.productos.add( producto );

				$('.'+this.form)[0].reset(); 
				
			}




			/**
			* Removes a product from table
			*
			*/
		,	onRemoveProduct: function( e ){

				e.preventDefault();

				// Get the id
				var target = e.currentTarget
				,	id = $( target ).data( 'id' );

				// Add model to collection
				this.productos.remove( { cid: id } );
				
			}

		,	onCompleteSaveOrden: function( data ){

				if ( data.productStatus == 404 )
					return utilities.showNotification( 'error', data.productMessage, 0 );

				if ( data.status == 500 )
					return utilities.showNotification( 'error', data.message, 0 );

				$('#vehiculo-form')[0].reset();

				this.productos.reset();

				this.radicado = data.radicado;

				this.openNotification( data );
			}

			/**
			* Show notification
			*
			*/

		,	openNotification: function( data ){

				var html = new EJS({ url: url + 'js/templates/notificacionProducto.ejs' }).render( data );

				var modal = {
					content: html,
					height: 340
				};


				utilities.showModalWindow( modal );

			}

		,	generatePdfCertificacion: function( e ){

				e.preventDefault();

				this.orden.generateCertificacion( this.radicado , this.onCompleteGeneratePdfCertificacion, this.onError );

			}

		,	onCompleteGeneratePdfCertificacion: function( data ){

				if( data.path == '' ){

					data.message = 'El PDF no pudo ser generado.';

					var html = new EJS( { url: url + 'js/templates/notFound.ejs' } ).render( data );

					var modal = {
						content: html
					};

					return utilities.showModalWindow( modal );
				}

				window.open( url + data.path, '_blank' );

			}

		,	filterByPlaca: function( e ){

				if ( e.which == 13 ) {

					$('#adminForm').submit();
				}
			}

		,	showPicture: function( e ){

				e.preventDefault();

				var data = {};

				var target = e.currentTarget;

				data.imagen = $(target).data('foto');

				var html = new EJS( { url: url + 'js/templates/picture.ejs' } ).render( data );

				var modal = {
					content: html
				};

				return utilities.showModalWindow( modal );

			}	

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.OrdenProductoView = new OrdenProductoView();

})( jQuery, this, this.document, this.Misc, undefined );