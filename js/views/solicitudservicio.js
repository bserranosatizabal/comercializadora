/**
* Registro View
* version : 1.0
* package: odontoline.frontend
* package: odontoline.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Allows to manage the registration
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var SolicitudServicioView = {};

	// Extends my object from Backbone events
	SolicitudServicioView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
				'submit #solicitud-servicio-form': 'saveServicio',
				'click .calendar': 'triggerCalendar',
				'click #print-solicitud': 'generatePdfCertificacion',
				'click .limpiar-filtros': 'resetForm',
				'click .vermas': 'showDescription'

			}

		,	view: this

		,	initialize: function(){

				_.bindAll(
					this,
					'saveServicio',
					'onCompleteSaveServicio',
					'openNotification',
					'onSelectDate'
				);


				// Datepicker options
				utilities.bindPickerOptions( '.datepicker-input', { 
					dateFormat: "yy-mm-dd",
					firstDay: 1,
					dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
					dayNamesShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
					monthNames:["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio",
					"Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
					monthNamesShort:["Ene", "Feb", "Mar", "Abr", "May", "Jun",
					"Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
					changeMonth: true,
					changeYear: true,
					yearRange: "-99:+14",
					onSelect: this.onSelectDate 
				} );

				// Datepicker options
				utilities.bindPickerOptions( '#visita', { 
					dateFormat: "yy-mm-dd",
					firstDay: 1,
					dayNamesMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
					dayNamesShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
					monthNames:["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio",
					"Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
					monthNamesShort:["Ene", "Feb", "Mar", "Abr", "May", "Jun",
					"Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
					changeMonth: true,
					changeYear: true,
					yearRange: "-99:+14",
					minDate: 0
				} );

				// Models
				this.servicio = new ServicioModel();
				this.dataServicio = '';
				this.radicado = '';
				

			}

		,	saveServicio: function(e){

				e.preventDefault();
				
				var data = utilities.formToJson( '#solicitud-servicio-form' )
				,	required = [ 
						'placa',
						'fecha_visita',
						'kilometraje',
						'fecha_instalacion',
						'sistema',
						'descripcion_fallo'

					]
				,	errors = []
				,	_this = this;

				console.log( data );

				utilities.validateEmptyFields( required, data, errors );

				if( errors.length > 0)
					return utilities.showNotification( 'error', 'Diligencie totalmente el formulario.', 0 );

				this.dataServicio = data;

				this.onProgress( 'Generando solicitud...', function(){

					//instance the model of service 
					_this.servicio.set( data );

					// instance function to save service
					_this.servicio.save(  _this.onCompleteSaveServicio, _this.onError );
					
				});

			}

			/**
			* Displays div with a progress
			*
			*/
		,	onProgress: function( message, callback ){


				var data = { 
					message : message
				};

				var html = new EJS( { url: url + 'js/templates/progress.ejs' } ).render( data );

				var modal = {
					content: html
				};

				utilities.showModalWindow( modal );

				if( typeof callback == 'function' ){

					setTimeout( function(){ callback.call(); }, 2000 );
				}
					
				
			}


		,	onCompleteSaveServicio: function( data ){

				if( data.status == 500 ){
					this.onProgress( data.message );
				}

				$('#solicitud-servicio-form')[0].reset();

				this.radicado = data.radicado;

				this.onProgress( data.message );
				this.openNotification( data );

			}


		,	generatePdfCertificacion: function( e ){

				e.preventDefault();

				this.servicio.generatePdf( this.radicado ,this.dataServicio, this.onCompleteGeneratePdfCertificacion, this.onError );

			}

		,	onCompleteGeneratePdfCertificacion: function( data ){

				if( data.path == '' ){

					data.message = 'El PDF no pudo ser generado.';

					var html = new EJS( { url: url + 'js/templates/notFound.ejs' } ).render( data );

					var modal = {
						content: html
					};

					return utilities.showModalWindow( modal );
				}

				window.open( url + data.path, '_blank' );
			}

			/**
			* Show notification
			*
			*/

		,	openNotification: function( data ){

				data.url =  url;

				var html = new EJS({ url: url + 'js/templates/notificacionSolicitud.ejs' }).render( data );

				var modal = {
					content: html,
					height: 340
				};

				utilities.showModalWindow( modal );


			}

			/**
			* Show calendar to each input
			*
			*/

		,	triggerCalendar	: function( e ){

				var target = e.currentTarget;

				var calendar = $(target).data('calendar');

				utilities.triggerCalendar( calendar );

			}


			/**
			* Filter by dates
			*/
		,	onSelectDate: function( date, object ){

				var data = {};

				data.initialDate = $('#inicio').val();
				data.finalDate =  $('#final').val();

				if( data.initialDate == '')
					return utilities.showNotification( 'error', 'Seleccione la fecha inicial.', 0 );

				$('#adminForm').submit();
				
			}


		,	resetForm: function( e ){

				e.preventDefault();
        		$( '#adminForm' ).find( ':input' ).val( '' );
        		$( '#adminForm' ).find('[name="filter_order_Dir"]').val('DESC');
        		$( '#adminForm' ).submit();

			}

		,	showDescription: function( e ){

				e.preventDefault();

				var data = {}
				, 	target = e.currentTarget;

				data.descripcion = $(target).data('description');

				console.log( data );

				var html = new EJS({ url: url + 'js/templates/descripcion.ejs' }).render( data );

				var modal = {
					content: html,
					height: 340
				};

				utilities.showModalWindow( modal );

			}

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.SolicitudServicioView = new SolicitudServicioView();

})( jQuery, this, this.document, this.Misc, undefined );