<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$url = JURI::root();

$document = JFactory::getDocument();
$document->addStyleSheet($host.'less/load-styles.php?load=consulta');


?>

<div class="certificacion">

	<form id="consulta-form">
		<div class="header">
			<i class="icon-certificacion"></i>
			<h4>Consulte aquí su certificación</h4>
		</div>
		<div class="body">
			<ul>
				<li><input type="text" name="codigo" placeholder="Código del producto"><span>Ó</span></li>
				<li><input type="text" name="serial" placeholder="Serial del producto"></li>
			</ul>
		</div>
		<div class="footer">
			<input type="submit" value="Consultar" class="button-app">
		</div>
	</form>
</div>