<?php
/*------------------------------------------------------------------------
* Module VM Login
* author    Netbase Team
* copyright Copyright (C) 2012 www.cms-extensions.net All Rights Reserved.
* @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
* Websites: www.cms-extensions.net
* Technical Support:  Forum - www.cms-extensions.net
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die;
JHtml::_('behavior.keepalive');
?>
<?php if ($type == 'logout') : ?>
<form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" id="login-form">
<?php if ($params->get('greeting')) :

//echo $user->get('name');
 ?>
 	
	<span class="logout">
	<div class="greeting">
	<?php 
    echo '<b>' . JText::_('HI_TITLE') . '&nbsp' . $user->get('name') . '</b>';
    ?>
	</div>
<?php endif; ?>
	
	<span class="logout-button">
		<input type="submit" name="Submit" value="<?php echo JText::_('BUTTON_LOGOUT'); ?>" title="<?php echo JText::_('BUTTON_LOGOUT'); ?>"/>
		<input type="hidden" name="option" value="com_users" />
		<input type="hidden" name="task" value="user.logout" />
		<input type="hidden" name="return" value="<?php echo $return; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</span>
</form>
<?php else : ?>
<form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" id="login-form" >
	<?php if ($params->get('pretext')): ?>
		<div class="pretext">
		<p><?php echo $params->get('pretext'); ?></p>
		</div>
	<?php endif; ?>
    
    <span class="<?php echo $style ?>" style="display: block;">
	<span class="yoo-login">

		<span class="login">        
        <span class="username">
				<input id="modlgn-username" type="text" name="username" size="18" value="<?php echo JText::_('NAME_USERNAME'); ?>" onblur="if(this.value=='') this.value='<?php echo JText::_('NAME_USERNAME'); ?>';" onfocus="if(this.value=='<?php echo JText::_('NAME_USERNAME'); ?>') this.value='';" />
   	
			</span>
			
			<span class="password">
			
				<input id="modlgn-passwd" type="password" name="password" size="10" value="<?php echo JText::_( 'PASSWORD' ); ?>" onblur="if(this.value=='') this.value='<?php echo JText::_( 'PASSWORD' ); ?>';" onfocus="if(this.value=='<?php echo JText::_( 'PASSWORD' ); ?>') this.value='';" />
				
			</span>
        
    	<?php if(JPluginHelper::isEnabled('system', 'remember')) : ?>
			<?php endif; ?>
            
		<span class="login-button">
				<button value="<?php echo JText::_( 'BUTTON_LOGIN'); ?>" name="Submit" type="submit" title="<?php echo JText::_('BUTTON_LOGIN'); ?>"><?php echo JText::_('BUTTON_LOGIN'); ?></button>
			</span>

        	<?php if ( $lost_password ) { ?>
			<span class="lostpassword">
				<a href="<?php echo JRoute::_( 'index.php?option=com_users&view=reset' ); ?>" title="<?php echo JText::_('FORGOT_YOUR_PASSWORD'); ?>"></a>
			</span>
			<?php } ?>
			
			<?php if ( $lost_username ) { ?>
			<span class="lostusername">
				<a href="<?php echo JRoute::_( 'index.php?option=com_users&view=remind' ); ?>" title="<?php echo JText::_('FORGOT_YOUR_USERNAME'); ?>"></a>
			</span>
			<?php } ?>
			
			<?php
			$usersConfig = JComponentHelper::getParams('com_users');
			if ($usersConfig->get('allowUserRegistration')) { ?>
			<span class="registration">
            <?php 
            if($style_login == 'joomla_style') { ?>
				<a href="<?php echo JRoute::_( 'index.php?option=com_users&view=registration' ); ?>" title="<?php echo JText::_( 'REGISTER'); ?>"></a>
            <?php
            }
                else {
            ?>
            	<a href="<?php echo JRoute::_( 'index.php?option=com_virtuemart&view=user&task=editaddresscart' ); ?>" title="<?php echo JText::_( 'REGISTER'); ?>"></a>
            <?php } ?>
			</span>
			<?php } ?>
            
           	<?php 
               if ($cart_vms) { ?>
			<span class="cart_vms">
				<a href="<?php echo JRoute::_( 'index.php?option=com_virtuemart&view=cart' ); ?>" title="<?php echo JText::_('VIEW_VM_CART'); ?>"></a>
			</span>
			<?php } ?>
            
			<?php echo $params->get('posttext'); ?>
	<input type="hidden" name="option" value="com_users" />
	<input type="hidden" name="task" value="user.login" />
	<input type="hidden" name="return" value="<?php echo $return; ?>" />
	<?php echo JHtml::_('form.token'); ?>
	</span>
	
	</span>
</span>
</form>
<?php endif; ?>
