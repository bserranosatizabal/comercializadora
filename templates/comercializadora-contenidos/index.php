<!DOCTYPE html>
<html lang="es" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9">
        <?php //JHTML::_('behavior.mootools'); ?>
        <jdoc:include type="head" /> 
        <link href='http://fonts.googleapis.com/css?family=Arimo:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Muli:400,400italic,300italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Archivo+Narrow' rel='stylesheet' type='text/css'>   
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,900,700italic,700,500italic,400italic,500,300italic,300,100,100italic,900italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
        <link rel="stylesheet"	href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/template.css" type="text/css" />
        <link rel="stylesheet"  href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/jquery-ui-1.10.4.custom.min.css" type="text/css" />
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
        <script type="text/javascript" src="js/libs/jquery-1.10.2.min.js"></script>
        <link rel="stylesheet" type="text/css" href="less/load-styles.php?load=register">

        <script type="text/javascript">
        var url = "<?php echo JURI::root(); ?>"
        </script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        
        <script src="js/libs/jquery-ui-1.10.4.custom.min.js"></script>
        <script src="js/libs/ejs.js"></script>
        <script src="js/libs/underscore-min.js"></script>
        <script src="js/libs/backbone-min.js"></script>
        <script src="js/libs/fileuploader/fileuploader.js"></script>
    </head>

    <body>
        <div id="content_cab">
            <div id="content_top">
                <a class="enlace-home" href="index.php"></a>
                <div id="logo-animate"> 
                    <iframe scrolling="no" frameborder="0" height="105" width="275" src="./html5/logo.html" style="border:none;"></iframe>
                </div>

                <div id="mods-top"> 
                    <div id="acceso">
                      <jdoc:include type="modules" name="acceso" style="xhtml" />
                    </div>     
                    <div id="buscar">
                      <jdoc:include type="modules" name="buscar" style="xhtml" />
                    </div> 
                </div>   
            </div>     
        </div>      

        <div id="content-menu">
            <div id="menu">
                <jdoc:include type="modules" name="menu" style="xhtml" />
            </div>  
        </div> 

        <main id="all-content">
            <section id="content-content">
                <jdoc:include type="message" />
                <jdoc:include type="component" />
            </section>
        </main>
        <div id="content_pie">
            <div id="content_piedos">
                <div id="pie">
                    <jdoc:include type="modules" name="pie" style="xhtml" />
                </div>

                <div id="redes"> 
                    <h4 class="inferior">Síganos</h4>
                    <p class="tinferior">Síganos en las redes sociales</p><iframe scrolling="no" frameborder="0" height="60" width="125" src="./html5/redes.html" style="border:none; float:right;"></iframe>
                </div>

                <div id="copy">
                    <?php echo date('Y'); ?>| Sitio Web Desarrollado Por <a href="http://www.creandopaginasweb.com/" target="_blank">
                    <div id="logo"></div></a>
                </div>
            </div>
        </div>

        <div class="global-notification">Hola mensaje</div>


        <div class="confirm-notification">
            <span class="message">Hola mundo</span>
            <a href="#" class="confirm-accept-button">Aceptar</a>
            <a href="#" class="confirm-cancel-button">Cancelar</a>
        </div>
        <!-- Overlay and modal box -->
        <div class="em-over-screen"></div>
        <div class="em-modal-box">
            <div class="header">
                <i class="close-modal-button">x</i>
            </div>
            <div class="body">
                Hello modal box
            </div>
        </div>
        
        <script src="js/load-scripts.php"></script>
    </body>
</html>
