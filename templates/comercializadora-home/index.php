<?php 
    $uri = &JURI::getInstance();
    $url = $uri->root();
?>

<!DOCTYPE html>
<html lang="es" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9">

        <jdoc:include type="head" /> 
        <link href='http://fonts.googleapis.com/css?family=Arimo:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Muli:400,400italic,300italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Archivo+Narrow' rel='stylesheet' type='text/css'>   
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,900,700italic,700,500italic,400italic,500,300italic,300,100,100italic,900italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
        <link rel="stylesheet" href="css/template.css" type="text/css" /><link rel="stylesheet"	href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/template.css" type="text/css" />
        
        <script type="text/javascript">
            var x;
            x=jQuery(document);
            x.ready(inicio);
            function inicio(){
            var ancho= window.innerWidth;
            var margen= (1900-ancho)/2;
            var x;
            x=jQuery("#banner");
            x.css("margin-left","-"+margen+"px");
            }
        </script>

        <!-- include libs backbone -->

        <script src="js/libs/jquery-ui-1.10.4.custom.min.js"></script>
        <script src="js/libs/ejs.js"></script>
        <script src="js/libs/underscore-min.js"></script>
        <script src="js/libs/backbone-min.js"></script>

        <script type="text/javascript">

            var url = '<?php echo $url; ?>';

        </script>

    </head>

    <body onresize="inicio()">

        <div id="content_cab">
            <div id="content_top">
                <a class="enlace-home" href="index.php"></a> 
                <div id="logo-animate"> 
                    <iframe scrolling="no" frameborder="0" height="105" width="275" src="./html5/logo.html" style="border:none;"></iframe>
                </div>

                <div id="mods-top"> 
                    <div id="acceso">
                        <jdoc:include type="modules" name="acceso" style="xhtml" />
                    </div>     
                    <div id="buscar">
                        <jdoc:include type="modules" name="buscar" style="xhtml" />
                    </div> 
                </div>   
            </div>     
        </div>      

        <div id="content-menu">
            <div id="menu">
                <jdoc:include type="modules" name="menu" style="xhtml" />
            </div>  
        </div> 
        <div id="content-banner">
            <div id="banner">
                <jdoc:include type="modules" name="banner" style="xhtml" />
            </div>  
        </div> 

        <div id="sombra-banner"></div> 

        <div id="all-content">
            <div id="content-content">
                <div id="left-content">
                    <div id="destacados">
                        <jdoc:include type="modules" name="destacados" style="xhtml" />
                    </div>

                    <div id="noticias">
                        <jdoc:include type="modules" name="noticias" style="xhtml" />
                    </div>

                    <div id="content-mods">
                        <div id="banco-pruebas">
                            <jdoc:include type="modules" name="banco-pruebas" style="xhtml" />
                        </div>

                        <div id="calendario">
                        	<jdoc:include type="modules" name="calendario" style="xhtml" />
                        </div>

                        <div id="certificacion">
                        	<jdoc:include type="modules" name="certificacion" style="xhtml" />
                        </div>
                    </div>
                </div>
                <div id="right-content">
                    <div id="orden-aqui">
                        <jdoc:include type="modules" name="orden-aqui" style="xhtml" />
                    </div>
                </div>
            </div>
        </div>
        <div id="content_pie">
            <div id="content_piedos">
                <div id="pie">
                    <jdoc:include type="modules" name="pie" style="xhtml" />
                </div>

                <div id="redes"> 
                    <h4 class="inferior">Síganos</h4>
                    <p class="tinferior">Síganos en las redes sociales</p><iframe scrolling="no" frameborder="0" height="60" width="125" src="./html5/redes.html" style="border:none; float:right;"></iframe>
                </div>

                <div id="copy">
                    <?php echo date('Y'); ?>| Sitio Web Desarrollado Por <a href="http://www.creandopaginasweb.com/" target="_blank">
                    <div id="logo"></div></a>
                </div>
            </div>
        </div>
        <div class="global-notification">Hola mensaje</div>
        <!-- Overlay and modal box -->
        <div class="em-over-screen"></div>
        <div class="em-modal-box">
            <div class="header">
                <i class="close-modal-button">x</i>
            </div>
            <div class="body">
                Hello modal box
            </div>
        </div>
        <script src="js/load-scripts.php"></script>
    </body>
</html>
